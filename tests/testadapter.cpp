//=================================================================================================
//
//   Compiler independent safe bit-fields for C++98
//
//   File:          /test/testadapter.cpp
//
//   Description:   This file implements a mocked storage backend for testing the declaration and
//                  wrapper mechanisms.
//
//   License:       Copyright (c) 2018 Julian Dessecker
//
//                  Permission is hereby granted, free of charge, to any person obtaining
//                  a copy of this software and associated documentation files
//                  (the "Software"), to deal in the Software without restriction,
//                  including without limitation the rights to use, copy, modify, merge,
//                  publish, distribute, sublicense, and/or sell copies of the Software,
//                  and to permit persons to whom the Software is furnished to do so,
//                  subject to the following conditions:
//
//                  The above copyright notice and this permission notice shall be
//                  included in all copies or substantial portions of the Software.
//
//                  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
//                  EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
//                  OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
//                  NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS
//                  BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN
//                  ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
//                  CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
//                  SOFTWARE.
//
//=================================================================================================


#define __BITFIELDS__INCLUDE__GUARD__
#include "../include/bitfield/testadapter.h"
#undef __BITFIELDS__INCLUDE__GUARD__


#ifdef __BITFIELDS_TEST_MODE


#include <stdio.h>
#include <stdarg.h>
#include <string>


//-------------------------------------------------------------------------------------------------

static ::__bitfields::test::IMockedStorage* __test__mock = 0;
static bool __test__useDummyRead = false;
static bool __test__useDummyWrite = false;


//-------------------------------------------------------------------------------------------------

void BitfieldTests::setStorageBackendMock (::__bitfields::test::IMockedStorage* mock)
{
    __test__mock = mock;
}


//-------------------------------------------------------------------------------------------------

void BitfieldTests::setDummyBackendRead (bool useDummy)
{
    __test__useDummyRead = useDummy;
}


//-------------------------------------------------------------------------------------------------

void BitfieldTests::setDummyBackendWrite (bool useDummy)
{
    __test__useDummyWrite = useDummy;
}


//-------------------------------------------------------------------------------------------------

void BitfieldTests::reset ()
{
    __test__mock = 0;
    __test__useDummyRead = false;
    __test__useDummyWrite = false;
}


//-------------------------------------------------------------------------------------------------

::__bitfields::test::IMockedStorage* BitfieldTests::getStorageBackendMock ()
{
    return __test__mock;
}


//-------------------------------------------------------------------------------------------------

bool BitfieldTests::useDummyBackendRead ()
{
    return __test__useDummyRead;
}


//-------------------------------------------------------------------------------------------------

bool BitfieldTests::useDummyBackendWrite ()
{
    return __test__useDummyWrite;
}


//-------------------------------------------------------------------------------------------------
// Dump mocked layout
//-------------------------------------------------------------------------------------------------
void ::__bitfields::test::MockedLayout::dump (const char* name, bool compact, int indent)
{
    // write name
    bool nameSet =  ! (!name || name[0] == 0);
    bool hasDeclName = ! (!innerName || innerName[0] == 0);
    std::string sname =  (nameSet || hasDeclName) ? " " : "";
    if (hasDeclName)
    {
        sname += innerName;
    }
    else if (nameSet)
    {
        sname += name;
    }
    indentWrite (indent,            "bit-field member%s:\n", sname.c_str ());

    if (compact)
    {
        // TODO: write compact

        // write verbose info
        indentWrite (indent + 1,    "backend and attributes:\n");
        indentWrite (indent + 2,    "backend:                      %s\n", BackendType);
        indentWrite (indent + 2,    "attributes:                   %s\n", AttributesType);
        indentWrite (indent + 2,    "isArrayBackend:               %s\n", isArrayBackend ? "true" : "false");

        // write member info
        indentWrite (indent + 1,    "member type:\n");
        indentWrite (indent + 2,    "member type:                  %s\n", MemberType);
        indentWrite (indent + 2,    "member type no qual:          %s\n", MemberType);
        indentWrite (indent + 2,    "member is const:              %s\n", memberIsConst ? "true" : "false");
        indentWrite (indent + 2,    "localOffset:                  %d\n", (int) localOffset);

        // nesting information
        indentWrite (indent + 1,    "nesting information:\n");
        indentWrite (indent + 2,    "is nested:                    %s\n", isNested ? "true" : "false");
        indentWrite (indent + 2,    "hosting nested:               %s\n", hostingNested ? "true" : "false");
        indentWrite (indent + 2,    "nesting offset:               %d\n", (int) nestedOffset);
        if (isNested && NestingInformation)
        {
            indentWrite (indent + 2,    "nesting parent: {\n", (int) nestedOffset);
            NestingInformation->dump ("", compact, indent + 2);
            indentWrite (indent + 2,    "}\n", (int) nestedOffset);
        }
        else
        {
            indentWrite (indent + 2,    "nesting parent:               none\n", (int) nestedOffset);
        }

        // storage backend
        indentWrite (indent + 1,    "storage backend:\n");
        indentWrite (indent + 2,    "storage type:                 %s\n", StorageType);
        indentWrite (indent + 2,    "storage is const:             %s\n", storageIsConst ? "true" : "false");
        indentWrite (indent + 2,    "storage is volatile:          %s\n", storageIsVolatile ? "true" : "false");
        indentWrite (indent + 2,    "storage is relaxed:           %s\n", storageRelaxed ? "true" : "false");
        indentWrite (indent + 2,    "storage alignment:            %d\n", (int) storageAlignment);
        indentWrite (indent + 2,    "take own storage:             %s\n", takeOwnStorage ? "true" : "false");
        indentWrite (indent + 2,    "take outer storage:           %s\n", takeOuterStorage ? "true" : "false");
        indentWrite (indent + 2,    "bit-field bits:               %d\n", (int) bitfieldBits);

        // array defintions
        indentWrite (indent + 1,    "array infos:\n");
        indentWrite (indent + 2,    "is array:                     %s\n", isArray ? "true" : "false");
        indentWrite (indent + 2,    "index reversed:               %s\n", arrayIndexReversed ? "true" : "false");
        indentWrite (indent + 2,    "array size raw:               %d\n", (int) arraySizeRaw);
        indentWrite (indent + 2,    "array element count:          %d\n", (int) arraySize);
        indentWrite (indent + 2,    "array address conversion:     %s\n", ArrayAddressConversion);

        // view implementation
        indentWrite (indent + 1,    "view wrapper:\n");
        indentWrite (indent + 2,    "has view:                     %s\n", hasView ? "true" : "false");
        if (hasView)
        {
            indentWrite (indent + 2,    "view type:                    %s\n", ViewType);
        }
        else
        {
            indentWrite (indent + 2,    "view type:                    none\n");
        }

        // resolved layout
        indentWrite (indent + 1,    "resolved layout:\n");
        indentWrite (indent + 2,    "representation type:          %s\n", RepresentationType);
        indentWrite (indent + 2,    "representation type no qual:  %s\n", RepresentationType);
        indentWrite (indent + 2,    "bits:                         %d\n", (int) bits);
        indentWrite (indent + 2,    "offset:                       %d\n", (int) offset);
        indentWrite (indent + 2,    "is const:                     %s\n", isConst ? "true" : "false");
    }
    else
    {
        // write verbose info
        indentWrite (indent + 1,    "backend and attributes:\n");
        indentWrite (indent + 2,    "backend:                      %s\n", BackendType);
        indentWrite (indent + 2,    "attributes:                   %s\n", AttributesType);
        indentWrite (indent + 2,    "isArrayBackend:               %s\n", isArrayBackend ? "true" : "false");

        // write member info
        indentWrite (indent + 1,    "member type:\n");
        indentWrite (indent + 2,    "member type:                  %s\n", MemberType);
        indentWrite (indent + 2,    "member type no qual:          %s\n", MemberType);
        indentWrite (indent + 2,    "member is const:              %s\n", memberIsConst ? "true" : "false");
        indentWrite (indent + 2,    "localOffset:                  %d\n", (int) localOffset);

        // nesting information
        indentWrite (indent + 1,    "nesting information:\n");
        indentWrite (indent + 2,    "is nested:                    %s\n", isNested ? "true" : "false");
        indentWrite (indent + 2,    "hosting nested:               %s\n", hostingNested ? "true" : "false");
        indentWrite (indent + 2,    "nesting offset:               %d\n", (int) nestedOffset);
        if (isNested && NestingInformation)
        {
            indentWrite (indent + 2,    "nesting parent: {\n", (int) nestedOffset);
            NestingInformation->dump ("", compact, indent + 2);
            indentWrite (indent + 2,    "}\n", (int) nestedOffset);
        }
        else
        {
            indentWrite (indent + 2,    "nesting parent:               none\n", (int) nestedOffset);
        }

        // storage backend
        indentWrite (indent + 1,    "storage backend:\n");
        indentWrite (indent + 2,    "storage type:                 %s\n", StorageType);
        indentWrite (indent + 2,    "storage is const:             %s\n", storageIsConst ? "true" : "false");
        indentWrite (indent + 2,    "storage is volatile:          %s\n", storageIsVolatile ? "true" : "false");
        indentWrite (indent + 2,    "storage is relaxed:           %s\n", storageRelaxed ? "true" : "false");
        indentWrite (indent + 2,    "storage alignment:            %d\n", (int) storageAlignment);
        indentWrite (indent + 2,    "take own storage:             %s\n", takeOwnStorage ? "true" : "false");
        indentWrite (indent + 2,    "take outer storage:           %s\n", takeOuterStorage ? "true" : "false");
        indentWrite (indent + 2,    "bit-field bits:               %d\n", (int) bitfieldBits);

        // array defintions
        indentWrite (indent + 1,    "array infos:\n");
        indentWrite (indent + 2,    "is array:                     %s\n", isArray ? "true" : "false");
        indentWrite (indent + 2,    "index reversed:               %s\n", arrayIndexReversed ? "true" : "false");
        indentWrite (indent + 2,    "array size raw:               %d\n", (int) arraySizeRaw);
        indentWrite (indent + 2,    "array element count:          %d\n", (int) arraySize);

        // view implementation
        indentWrite (indent + 1,    "view wrapper:\n");
        indentWrite (indent + 2,    "has view:                     %s\n", hasView ? "true" : "false");
        if (hasView)
        {
            indentWrite (indent + 2,    "view type:                    %s\n", ViewType);
        }
        else
        {
            indentWrite (indent + 2,    "view type:                    none\n");
        }

        // resolved layout
        indentWrite (indent + 1,    "resolved layout:\n");
        indentWrite (indent + 2,    "representation type:          %s\n", RepresentationType);
        indentWrite (indent + 2,    "representation type no qual:  %s\n", RepresentationType);
        indentWrite (indent + 2,    "bits:                         %d\n", (int) bits);
        indentWrite (indent + 2,    "offset:                       %d\n", (int) offset);
        indentWrite (indent + 2,    "is const:                     %s\n", isConst ? "true" : "false");
    }
}


//-------------------------------------------------------------------------------------------------
// Write formated string with indent
//-------------------------------------------------------------------------------------------------
void ::__bitfields::test::MockedLayout::indentWrite (int indent, const char* fmt, ...)
{
    // create string from printf style call
    char buffer[32768];
    va_list args;
    va_start (args, fmt);
    vsprintf(buffer, fmt, args);
    va_end (args);

    // write indented string
    for (int i=0; i<indent; i++)
        printf ("  ");
    printf ("%s", buffer);
}


#endif
