//=================================================================================================
//
//   Compiler independent safe bit-fields for C++98
//
//   File:          /test/test_basics.cpp
//
//   Description:   Basic layout tests for bit-field members.
//
//   License:       Copyright (c) 2018 Julian Dessecker
//
//                  Permission is hereby granted, free of charge, to any person obtaining
//                  a copy of this software and associated documentation files
//                  (the "Software"), to deal in the Software without restriction,
//                  including without limitation the rights to use, copy, modify, merge,
//                  publish, distribute, sublicense, and/or sell copies of the Software,
//                  and to permit persons to whom the Software is furnished to do so,
//                  subject to the following conditions:
//
//                  The above copyright notice and this permission notice shall be
//                  included in all copies or substantial portions of the Software.
//
//                  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
//                  EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
//                  OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
//                  NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS
//                  BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN
//                  ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
//                  CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
//                  SOFTWARE.
//
//=================================================================================================


#include <gtest/gtest.h>
#include "api/bitfield.h"
#include "include/bitfield/testadapter.h"
#include "mockedstorage.h"
#include "testutil.h"


//-------------------------------------------------------------------------------------------------
// Check basic storage type declarations
//-------------------------------------------------------------------------------------------------


// --- 8 bit storage ---
//                          | data type |   name |  arraysize | offset |    bitcount |
DECLARE_BITFIELD (Storage8T, uint8_t)
    BITFIELD_MEMBER         ( uint8_t,      xx,                 4,          2        );
    BITFIELD_MEMBER         ( bool,         b,                  0                    );
END_BITFIELD

TEST (BasicTest, StorageLayout8T)
{
    // instantiate bit-field and get layout
    Storage8T val;
    ::__bitfields::test::MockedLayout* layout = ::BitfieldTests::getLayout (val.xx);

    // assert we got a layout information
    ASSERT_NE (layout, nullptr);

    // match storage type name
    ASSERT_EQ (Util::matchesType<uint8_t> (layout->StorageType), true);

    // assert type size matches
    ASSERT_EQ (layout->storageElemetSize, 8);

    // assert default alignment is correct
    ASSERT_EQ (layout->storageAlignment, 8);

    // assert storage qualifiers
    ASSERT_EQ (layout->storageIsConst, false);
    ASSERT_EQ (layout->storageIsVolatile, false);

    // assert final const modifier is not set
    ASSERT_EQ (layout->isConst, false);

    // clean up
    delete layout;
};


// --- 16 bit storage ---
//                          | data type |   name |  arraysize | offset |    bitcount |
DECLARE_BITFIELD (Storage16T, uint16_t)
    BITFIELD_MEMBER         ( uint8_t,      xx,                 4,          2        );
    BITFIELD_MEMBER         ( bool,         b,                  0                    );
END_BITFIELD

TEST (BasicTest, StorageLayout16T)
{
    // instantiate bit-field and get layout
    Storage16T val;
    ::__bitfields::test::MockedLayout* layout = ::BitfieldTests::getLayout (val.xx);

    // assert we got a layout information
    ASSERT_NE (layout, nullptr);

    // match storage type name
    ASSERT_EQ (Util::matchesType<uint16_t> (layout->StorageType), true);

    // assert type size matches
    ASSERT_EQ (layout->storageElemetSize, 16);

    // assert default alignment is correct
    ASSERT_EQ (layout->storageAlignment, 16);

    // assert storage qualifiers
    ASSERT_EQ (layout->storageIsConst, false);
    ASSERT_EQ (layout->storageIsVolatile, false);

    // assert final const modifier is not set
    ASSERT_EQ (layout->isConst, false);

    // clean up
    delete layout;
};


// --- 32 bit storage ---
//                          | data type |   name |  arraysize | offset |    bitcount |
DECLARE_BITFIELD (Storage32T, uint32_t)
    BITFIELD_MEMBER         ( uint8_t,      xx,                 4,          2        );
    BITFIELD_MEMBER         ( bool,         b,                  0                    );
END_BITFIELD

TEST (BasicTest, StorageLayout32T)
{
    // instantiate bit-field and get layout
    Storage32T val;
    ::__bitfields::test::MockedLayout* layout = ::BitfieldTests::getLayout (val.xx);

    // assert we got a layout information
    ASSERT_NE (layout, nullptr);

    // match storage type name
    ASSERT_EQ (Util::matchesType<uint32_t> (layout->StorageType), true);

    // assert type size matches
    ASSERT_EQ (layout->storageElemetSize, 32);

    // assert default alignment is correct
    ASSERT_EQ (layout->storageAlignment, 32);

    // assert storage qualifiers
    ASSERT_EQ (layout->storageIsConst, false);
    ASSERT_EQ (layout->storageIsVolatile, false);

    // assert final const modifier is not set
    ASSERT_EQ (layout->isConst, false);

    // clean up
    delete layout;
};


// --- 64 bit storage ---
//                          | data type |   name |  arraysize | offset |    bitcount |
DECLARE_BITFIELD (Storage64T, uint64_t)
    BITFIELD_MEMBER         ( uint8_t,      xx,                 4,          2        );
    BITFIELD_MEMBER         ( bool,         b,                  0                    );
END_BITFIELD

TEST (BasicTest, StorageLayout64T)
{
    // instantiate bit-field and get layout
    Storage64T val;
    ::__bitfields::test::MockedLayout* layout = ::BitfieldTests::getLayout (val.xx);

    // assert we got a layout information
    ASSERT_NE (layout, nullptr);

    // match storage type name
    ASSERT_EQ (Util::matchesType<uint64_t> (layout->StorageType), true);

    // assert type size matches
    ASSERT_EQ (layout->storageElemetSize, 64);

    // assert default alignment is correct
    ASSERT_EQ (layout->storageAlignment, 64);

    // assert storage qualifiers
    ASSERT_EQ (layout->storageIsConst, false);
    ASSERT_EQ (layout->storageIsVolatile, false);

    // assert final const modifier is not set
    ASSERT_EQ (layout->isConst, false);

    // clean up
    delete layout;
};


// --- 8 bit const storage ---
//                          | data type |   name |  arraysize | offset |    bitcount |
DECLARE_BITFIELD (Storage8CT, const uint8_t)
    BITFIELD_MEMBER         ( uint8_t,      xx,                 4,          2        );
    BITFIELD_MEMBER         ( bool,         b,                  0                    );
END_BITFIELD

TEST (BasicTest, StorageLayout8CT)
{
    // instantiate bit-field and get layout
    Storage8CT& val = *((Storage8CT*) 1234);
    ::__bitfields::test::MockedLayout* layout = ::BitfieldTests::getLayout (val.xx);

    // assert we got a layout information
    ASSERT_NE (layout, nullptr);

    // match storage type name
    ASSERT_EQ (Util::matchesType<const uint8_t> (layout->StorageType), true);

    // assert type size matches
    ASSERT_EQ (layout->storageElemetSize, 8);

    // assert default alignment is correct
    ASSERT_EQ (layout->storageAlignment, 8);

    // assert storage qualifiers
    ASSERT_EQ (layout->storageIsConst, true);
    ASSERT_EQ (layout->storageIsVolatile, false);

    // assert final const modifier is set
    ASSERT_EQ (layout->isConst, true);

    // clean up
    delete layout;
};


// --- 16 bit const storage ---
//                          | data type |   name |  arraysize | offset |    bitcount |
DECLARE_BITFIELD (Storage16CT, const uint16_t)
    BITFIELD_MEMBER         ( uint8_t,      xx,                 4,          2        );
    BITFIELD_MEMBER         ( bool,         b,                  0                    );
END_BITFIELD

TEST (BasicTest, StorageLayout16CT)
{
    // instantiate bit-field and get layout
    Storage16CT& val = *((Storage16CT*) 1234);
    ::__bitfields::test::MockedLayout* layout = ::BitfieldTests::getLayout (val.xx);

    // assert we got a layout information
    ASSERT_NE (layout, nullptr);

    // match storage type name
    ASSERT_EQ (Util::matchesType<const uint16_t> (layout->StorageType), true);

    // assert type size matches
    ASSERT_EQ (layout->storageElemetSize, 16);

    // assert default alignment is correct
    ASSERT_EQ (layout->storageAlignment, 16);

    // assert storage qualifiers
    ASSERT_EQ (layout->storageIsConst, true);
    ASSERT_EQ (layout->storageIsVolatile, false);

    // assert final const modifier is set
    ASSERT_EQ (layout->isConst, true);

    // clean up
    delete layout;
};


// --- 32 bit const storage ---
//                          | data type |   name |  arraysize | offset |    bitcount |
DECLARE_BITFIELD (Storage32CT, const uint32_t)
    BITFIELD_MEMBER         ( uint8_t,      xx,                 4,          2        );
    BITFIELD_MEMBER         ( bool,         b,                  0                    );
END_BITFIELD

TEST (BasicTest, StorageLayout32CT)
{
    // instantiate bit-field and get layout
    Storage32CT& val = *((Storage32CT*) 1234);
    ::__bitfields::test::MockedLayout* layout = ::BitfieldTests::getLayout (val.xx);

    // assert we got a layout information
    ASSERT_NE (layout, nullptr);

    // match storage type name
    ASSERT_EQ (Util::matchesType<const uint32_t> (layout->StorageType), true);

    // assert type size matches
    ASSERT_EQ (layout->storageElemetSize, 32);

    // assert default alignment is correct
    ASSERT_EQ (layout->storageAlignment, 32);

    // assert storage qualifiers
    ASSERT_EQ (layout->storageIsConst, true);
    ASSERT_EQ (layout->storageIsVolatile, false);

    // assert final const modifier is set
    ASSERT_EQ (layout->isConst, true);

    // clean up
    delete layout;
};


// --- 64 bit const storage ---
//                          | data type |   name |  arraysize | offset |    bitcount |
DECLARE_BITFIELD (Storage64CT, const uint64_t)
    BITFIELD_MEMBER         ( uint8_t,      xx,                 4,          2        );
    BITFIELD_MEMBER         ( bool,         b,                  0                    );
END_BITFIELD

TEST (BasicTest, StorageLayout64CT)
{
    // instantiate bit-field and get layout
    Storage64CT& val = *((Storage64CT*) 1234);
    ::__bitfields::test::MockedLayout* layout = ::BitfieldTests::getLayout (val.xx);

    // assert we got a layout information
    ASSERT_NE (layout, nullptr);

    // match storage type name
    ASSERT_EQ (Util::matchesType<const uint64_t> (layout->StorageType), true);

    // assert type size matches
    ASSERT_EQ (layout->storageElemetSize, 64);

    // assert default alignment is correct
    ASSERT_EQ (layout->storageAlignment, 64);

    // assert storage qualifiers
    ASSERT_EQ (layout->storageIsConst, true);
    ASSERT_EQ (layout->storageIsVolatile, false);

    // assert final const modifier is set
    ASSERT_EQ (layout->isConst, true);

    // clean up
    delete layout;
};


// --- 8 bit volatile storage ---
//                          | data type |   name |  arraysize | offset |    bitcount |
DECLARE_BITFIELD (Storage8VT, volatile uint8_t)
    BITFIELD_MEMBER         ( uint8_t,      xx,                 4,          2        );
    BITFIELD_MEMBER         ( bool,         b,                  0                    );
END_BITFIELD

TEST (BasicTest, StorageLayout8VT)
{
    // instantiate bit-field and get layout
    Storage8VT& val = *((Storage8VT*) 1234);
    ::__bitfields::test::MockedLayout* layout = ::BitfieldTests::getLayout (val.xx);

    // assert we got a layout information
    ASSERT_NE (layout, nullptr);

    // match storage type name
    ASSERT_EQ (Util::matchesType<volatile uint8_t> (layout->StorageType), true);

    // assert type size matches
    ASSERT_EQ (layout->storageElemetSize, 8);

    // assert default alignment is correct
    ASSERT_EQ (layout->storageAlignment, 8);

    // assert storage qualifiers
    ASSERT_EQ (layout->storageIsConst, false);
    ASSERT_EQ (layout->storageIsVolatile, true);

    // assert final const modifier is not set
    ASSERT_EQ (layout->isConst, false);

    // clean up
    delete layout;
};


// --- 16 bit volatile storage ---
//                          | data type |   name |  arraysize | offset |    bitcount |
DECLARE_BITFIELD (Storage16VT, volatile uint16_t)
    BITFIELD_MEMBER         ( uint8_t,      xx,                 4,          2        );
    BITFIELD_MEMBER         ( bool,         b,                  0                    );
END_BITFIELD

TEST (BasicTest, StorageLayout16VT)
{
    // instantiate bit-field and get layout
    Storage16VT& val = *((Storage16VT*) 1234);
    ::__bitfields::test::MockedLayout* layout = ::BitfieldTests::getLayout (val.xx);

    // assert we got a layout information
    ASSERT_NE (layout, nullptr);

    // match storage type name
    ASSERT_EQ (Util::matchesType<volatile uint16_t> (layout->StorageType), true);

    // assert type size matches
    ASSERT_EQ (layout->storageElemetSize, 16);

    // assert default alignment is correct
    ASSERT_EQ (layout->storageAlignment, 16);

    // assert storage qualifiers
    ASSERT_EQ (layout->storageIsConst, false);
    ASSERT_EQ (layout->storageIsVolatile, true);

    // assert final const modifier is not set
    ASSERT_EQ (layout->isConst, false);

    // clean up
    delete layout;
};


// --- 32 bit volatile storage ---
//                          | data type |   name |  arraysize | offset |    bitcount |
DECLARE_BITFIELD (Storage32VT, volatile uint32_t)
    BITFIELD_MEMBER         ( uint8_t,      xx,                 4,          2        );
    BITFIELD_MEMBER         ( bool,         b,                  0                    );
END_BITFIELD

TEST (BasicTest, StorageLayout32VT)
{
    // instantiate bit-field and get layout
    Storage32VT& val = *((Storage32VT*) 1234);
    ::__bitfields::test::MockedLayout* layout = ::BitfieldTests::getLayout (val.xx);

    // assert we got a layout information
    ASSERT_NE (layout, nullptr);

    // match storage type name
    ASSERT_EQ (Util::matchesType<volatile uint32_t> (layout->StorageType), true);

    // assert type size matches
    ASSERT_EQ (layout->storageElemetSize, 32);

    // assert default alignment is correct
    ASSERT_EQ (layout->storageAlignment, 32);

    // assert storage qualifiers
    ASSERT_EQ (layout->storageIsConst, false);
    ASSERT_EQ (layout->storageIsVolatile, true);

    // assert final const modifier is not set
    ASSERT_EQ (layout->isConst, false);

    // clean up
    delete layout;
};


// --- 64 bit volatile storage ---
//                          | data type |   name |  arraysize | offset |    bitcount |
DECLARE_BITFIELD (Storage64VT, volatile uint64_t)
    BITFIELD_MEMBER         ( uint8_t,      xx,                 4,          2        );
    BITFIELD_MEMBER         ( bool,         b,                  0                    );
END_BITFIELD

TEST (BasicTest, StorageLayout64VT)
{
    // instantiate bit-field and get layout
    Storage64VT& val = *((Storage64VT*) 1234);
    ::__bitfields::test::MockedLayout* layout = ::BitfieldTests::getLayout (val.xx);

    // assert we got a layout information
    ASSERT_NE (layout, nullptr);

    // match storage type name
    ASSERT_EQ (Util::matchesType<volatile uint64_t> (layout->StorageType), true);

    // assert type size matches
    ASSERT_EQ (layout->storageElemetSize, 64);

    // assert default alignment is correct
    ASSERT_EQ (layout->storageAlignment, 64);

    // assert storage qualifiers
    ASSERT_EQ (layout->storageIsConst, false);
    ASSERT_EQ (layout->storageIsVolatile, true);

    // assert final const modifier is not set
    ASSERT_EQ (layout->isConst, false);

    // clean up
    delete layout;
};


// --- 8 bit const volatile storage ---
//                          | data type |   name |  arraysize | offset |    bitcount |
DECLARE_BITFIELD (Storage8CVT, const volatile uint8_t)
    BITFIELD_MEMBER         ( uint8_t,      xx,                 4,          2        );
    BITFIELD_MEMBER         ( bool,         b,                  0                    );
END_BITFIELD

TEST (BasicTest, StorageLayout8CVT)
{
    // instantiate bit-field and get layout
    Storage8CVT& val = *((Storage8CVT*) 1234);
    ::__bitfields::test::MockedLayout* layout = ::BitfieldTests::getLayout (val.xx);

    // assert we got a layout information
    ASSERT_NE (layout, nullptr);

    // match storage type name
    ASSERT_EQ (Util::matchesType<const volatile uint8_t> (layout->StorageType), true);

    // assert type size matches
    ASSERT_EQ (layout->storageElemetSize, 8);

    // assert default alignment is correct
    ASSERT_EQ (layout->storageAlignment, 8);

    // assert storage qualifiers
    ASSERT_EQ (layout->storageIsConst, true);
    ASSERT_EQ (layout->storageIsVolatile, true);

    // assert final const modifier is set
    ASSERT_EQ (layout->isConst, true);

    // clean up
    delete layout;
};


// --- 16 bit const volatile storage ---
//                          | data type |   name |  arraysize | offset |    bitcount |
DECLARE_BITFIELD (Storage16CVT, const volatile uint16_t)
    BITFIELD_MEMBER         ( uint8_t,      xx,                 4,          2        );
    BITFIELD_MEMBER         ( bool,         b,                  0                    );
END_BITFIELD

TEST (BasicTest, StorageLayout16CVT)
{
    // instantiate bit-field and get layout
    Storage16CVT& val = *((Storage16CVT*) 1234);
    ::__bitfields::test::MockedLayout* layout = ::BitfieldTests::getLayout (val.xx);

    // assert we got a layout information
    ASSERT_NE (layout, nullptr);

    // match storage type name
    ASSERT_EQ (Util::matchesType<const volatile uint16_t> (layout->StorageType), true);

    // assert type size matches
    ASSERT_EQ (layout->storageElemetSize, 16);

    // assert default alignment is correct
    ASSERT_EQ (layout->storageAlignment, 16);

    // assert storage qualifiers
    ASSERT_EQ (layout->storageIsConst, true);
    ASSERT_EQ (layout->storageIsVolatile, true);

    // assert final const modifier is set
    ASSERT_EQ (layout->isConst, true);

    // clean up
    delete layout;
};


// --- 32 bit const volatile storage ---
//                          | data type |   name |  arraysize | offset |    bitcount |
DECLARE_BITFIELD (Storage32CVT, const volatile uint32_t)
    BITFIELD_MEMBER         ( uint8_t,      xx,                 4,          2        );
    BITFIELD_MEMBER         ( bool,         b,                  0                    );
END_BITFIELD

TEST (BasicTest, StorageLayout32CVT)
{
    // instantiate bit-field and get layout
    Storage32CVT& val = *((Storage32CVT*) 1234);
    ::__bitfields::test::MockedLayout* layout = ::BitfieldTests::getLayout (val.xx);

    // assert we got a layout information
    ASSERT_NE (layout, nullptr);

    // match storage type name
    ASSERT_EQ (Util::matchesType<const volatile uint32_t> (layout->StorageType), true);

    // assert type size matches
    ASSERT_EQ (layout->storageElemetSize, 32);

    // assert default alignment is correct
    ASSERT_EQ (layout->storageAlignment, 32);

    // assert storage qualifiers
    ASSERT_EQ (layout->storageIsConst, true);
    ASSERT_EQ (layout->storageIsVolatile, true);

    // assert final const modifier is set
    ASSERT_EQ (layout->isConst, true);

    // clean up
    delete layout;
};


// --- 64 bit const volatile storage ---
//                          | data type |   name |  arraysize | offset |    bitcount |
DECLARE_BITFIELD (Storage64CVT, const volatile uint64_t)
    BITFIELD_MEMBER         ( uint8_t,      xx,                 4,          2        );
    BITFIELD_MEMBER         ( bool,         b,                  0                    );
END_BITFIELD

TEST (BasicTest, StorageLayout64CVT)
{
    // instantiate bit-field and get layout
    Storage64CVT& val = *((Storage64CVT*) 1234);
    ::__bitfields::test::MockedLayout* layout = ::BitfieldTests::getLayout (val.xx);

    // assert we got a layout information
    ASSERT_NE (layout, nullptr);

    // match storage type name
    ASSERT_EQ (Util::matchesType<const volatile uint64_t> (layout->StorageType), true);

    // assert type size matches
    ASSERT_EQ (layout->storageElemetSize, 64);

    // assert default alignment is correct
    ASSERT_EQ (layout->storageAlignment, 64);

    // assert storage qualifiers
    ASSERT_EQ (layout->storageIsConst, true);
    ASSERT_EQ (layout->storageIsVolatile, true);

    // assert final const modifier is set
    ASSERT_EQ (layout->isConst, true);

    // clean up
    delete layout;
};


//-------------------------------------------------------------------------------------------------
// Check basic storage definitions
//-------------------------------------------------------------------------------------------------
using BasicTestTypes = testing::Types <
        Storage8T, Storage16T, Storage32T, Storage64T,
        Storage8CT, Storage16CT, Storage32CT, Storage64CT,
        Storage8VT, Storage16VT, Storage32VT, Storage64VT,
        Storage8CVT, Storage16CVT, Storage32CVT, Storage64CVT
    >;

// typed test
template<typename T>
struct BasicTest : public testing::Test
{
    using MyParamType = T;
};

// implementation of test for dependent type
TYPED_TEST_CASE (BasicTest, BasicTestTypes);
TYPED_TEST(BasicTest, BasicTestTypes)
{
    // use test type
    using T  = typename TestFixture::MyParamType;

    // instanciate value
    T& val = *((T*) 1234);
    ::__bitfields::test::MockedLayout* layout = ::BitfieldTests::getLayout (val.xx);

    // assert we got a layout information
    ASSERT_NE (layout, nullptr);

    // assert attributes for basic bit-field member (not nested, no array, no view)
    ASSERT_EQ (layout->storageRelaxed, false);
    ASSERT_EQ (layout->takeOwnStorage, false);
    ASSERT_EQ (layout->takeOuterStorage, false);
    ASSERT_EQ (layout->isArrayBackend, false);
    ASSERT_EQ (layout->memberIsConst, false);
    ASSERT_EQ (layout->memberIsVolatile, false);
    ASSERT_EQ (layout->isNested, false);
    ASSERT_EQ (layout->hostingNested, false);
    ASSERT_EQ (layout->nestedOffset, 0);
    ASSERT_EQ (layout->NestingInformation, nullptr);
    ASSERT_EQ (layout->hasView, false);

    // check array layout
    ASSERT_EQ (layout->isArray, false);
    ASSERT_EQ (layout->arrayIndexReversed, false);
    ASSERT_EQ (layout->arraySizeRaw, 0);
    ASSERT_EQ (layout->arraySize, 1);

    // check number of required bits
    ASSERT_EQ (layout->bitfieldBits, 6);

    // check layout of member .xx
    ASSERT_EQ (layout->localOffset, 4);
    ASSERT_EQ (layout->offset, 4);
    ASSERT_EQ (layout->bits, 2);

    // check representation type and qualifiers
    ASSERT_EQ (layout->representationIsConst, false);
    ASSERT_EQ (layout->representationIsVolatile, false);
    ASSERT_EQ (Util::matchesType<uint8_t> (layout->RepresentationType), true);

    // setup mocked storage backend
    MockedStorageBackend ms;
    BitfieldTests::reset ();
    BitfieldTests::setStorageBackendMock (&ms);
    BitfieldTests::setDummyBackendRead (false);
    BitfieldTests::setDummyBackendWrite (false);

    // clean up
    delete layout;
};


//-------------------------------------------------------------------------------------------------
// Check TakeOwnStorage directive
//-------------------------------------------------------------------------------------------------
//                          | data type |   name |  arraysize | offset |    bitcount |
DECLARE_BITFIELD (TakeOwnStorageDirectiveT, uint16_t, TakeOwnStorage)
    BITFIELD_MEMBER         ( uint8_t,      xx,                 4,          2        );
    BITFIELD_MEMBER         ( bool,         b,                  0                    );
END_BITFIELD

TEST (BasicTest, TakeOwnStorageDirective)
{
    // instantiate bit-field and get layout
    TakeOwnStorageDirectiveT& val = *((TakeOwnStorageDirectiveT*) 1234);
    ::__bitfields::test::MockedLayout* layout = ::BitfieldTests::getLayout (val.xx);

    ASSERT_EQ (layout->takeOuterStorage, false);
    ASSERT_EQ (layout->takeOwnStorage, true);

    // clean up
    delete layout;
}


//-------------------------------------------------------------------------------------------------
// Check TakeOuterStorage directive
//-------------------------------------------------------------------------------------------------
//                          | data type |   name |  arraysize | offset |    bitcount |
DECLARE_BITFIELD (TakeOuterStorageDirectiveT, uint16_t, TakeOuterStorage)
    BITFIELD_MEMBER         ( uint8_t,      xx,                 4,          2        );
    BITFIELD_MEMBER         ( bool,         b,                  0                    );
END_BITFIELD

TEST (BasicTest, TakeOuterStorageDirective)
{
    // instantiate bit-field and get layout
    TakeOuterStorageDirectiveT& val = *((TakeOuterStorageDirectiveT*) 1234);
    ::__bitfields::test::MockedLayout* layout = ::BitfieldTests::getLayout (val.xx);

    ASSERT_EQ (layout->takeOuterStorage, true);
    ASSERT_EQ (layout->takeOwnStorage, false);

    // clean up
    delete layout;
}


//-------------------------------------------------------------------------------------------------
// Check RelaxedStorage directive
//-------------------------------------------------------------------------------------------------
//                          | data type |   name |  arraysize | offset |    bitcount |
DECLARE_BITFIELD (RelaxedStorageDirectiveT, uint16_t, RelaxedStorage)
    BITFIELD_MEMBER         ( uint8_t,      xx,                 4,          2        );
    BITFIELD_MEMBER         ( bool,         b,                  0                    );
END_BITFIELD

TEST (BasicTest, RelaxedStorageDirective)
{
    // instantiate bit-field and get layout
    RelaxedStorageDirectiveT& val = *((RelaxedStorageDirectiveT*) 1234);
    ::__bitfields::test::MockedLayout* layout = ::BitfieldTests::getLayout (val.xx);

    ASSERT_EQ (layout->storageRelaxed, true);
    ASSERT_EQ (layout->takeOuterStorage, false);
    ASSERT_EQ (layout->takeOwnStorage, false);

    // clean up
    delete layout;
}


//-------------------------------------------------------------------------------------------------
// Check Alignment directive
//-------------------------------------------------------------------------------------------------

// explicit, 8 bits
//                          | data type |   name |  arraysize | offset |    bitcount |
DECLARE_BITFIELD (Alignemt8DirectiveT, uint16_t, Alignment<8>)
    BITFIELD_MEMBER         ( uint8_t,      xx,                 4,          2        );
    BITFIELD_MEMBER         ( bool,         b,                  0                    );
END_BITFIELD

TEST (BasicTest, Alignemt8Directive)
{
    // instantiate bit-field and get layout
    Alignemt8DirectiveT& val = *((Alignemt8DirectiveT*) 1234);
    ::__bitfields::test::MockedLayout* layout = ::BitfieldTests::getLayout (val.xx);

    ASSERT_EQ (layout->storageAlignment, 8);

    ASSERT_EQ (layout->storageRelaxed, false);
    ASSERT_EQ (layout->takeOuterStorage, false);
    ASSERT_EQ (layout->takeOwnStorage, false);

    // clean up
    delete layout;
}

// explicit, 32 bits
//                          | data type |   name |  arraysize | offset |    bitcount |
DECLARE_BITFIELD (Alignemt32DirectiveT, uint16_t, Alignment<32>)
    BITFIELD_MEMBER         ( uint8_t,      xx,                 4,          2        );
    BITFIELD_MEMBER         ( bool,         b,                  0                    );
END_BITFIELD

TEST (BasicTest, Alignemt32Directive)
{
    // instantiate bit-field and get layout
    Alignemt32DirectiveT& val = *((Alignemt32DirectiveT*) 1234);
    ::__bitfields::test::MockedLayout* layout = ::BitfieldTests::getLayout (val.xx);

    ASSERT_EQ (layout->storageAlignment, 32);

    ASSERT_EQ (layout->storageRelaxed, false);
    ASSERT_EQ (layout->takeOuterStorage, false);
    ASSERT_EQ (layout->takeOwnStorage, false);

    // clean up
    delete layout;
}


//-------------------------------------------------------------------------------------------------
// Check that directive order doesn't matter
//-------------------------------------------------------------------------------------------------

// #error start at index 0 && introduce addressing style

// directives order #0
//                          | data type |   name |  arraysize | offset |    bitcount |
DECLARE_BITFIELD (DirectiveOrder0T, uint16_t, Alignment<8>, TakeOuterStorage, RelaxedStorage)
    BITFIELD_MEMBER         ( uint8_t,      xx,                 4,          2        );
    BITFIELD_MEMBER         ( bool,         b,                  0                    );
END_BITFIELD

TEST (BasicTest, DirectiveOrder0)
{
    // instantiate bit-field and get layout
    DirectiveOrder0T& val = *((DirectiveOrder0T*) 1234);
    ::__bitfields::test::MockedLayout* layout = ::BitfieldTests::getLayout (val.xx);

    ASSERT_EQ (layout->storageAlignment, 8);

    ASSERT_EQ (layout->storageRelaxed, true);
    ASSERT_EQ (layout->takeOuterStorage, true);
    ASSERT_EQ (layout->takeOwnStorage, false);

    // clean up
    delete layout;
}

// directives order #1
//                          | data type |   name |  arraysize | offset |    bitcount |
DECLARE_BITFIELD (DirectiveOrder1T, uint16_t, TakeOuterStorage, RelaxedStorage, Alignment<8>)
    BITFIELD_MEMBER         ( uint8_t,      xx,                 4,          2        );
    BITFIELD_MEMBER         ( bool,         b,                  0                    );
END_BITFIELD

TEST (BasicTest, DirectiveOrder1)
{
    // instantiate bit-field and get layout
    DirectiveOrder1T& val = *((DirectiveOrder1T*) 1234);
    ::__bitfields::test::MockedLayout* layout = ::BitfieldTests::getLayout (val.xx);

    ASSERT_EQ (layout->storageAlignment, 8);

    ASSERT_EQ (layout->storageRelaxed, true);
    ASSERT_EQ (layout->takeOuterStorage, true);
    ASSERT_EQ (layout->takeOwnStorage, false);

    // clean up
    delete layout;
}

// directives order #2
//                          | data type |   name |  arraysize | offset |    bitcount |
DECLARE_BITFIELD (DirectiveOrder2T, uint16_t, RelaxedStorage, Alignment<8>, TakeOuterStorage)
    BITFIELD_MEMBER         ( uint8_t,      xx,                 4,          2        );
    BITFIELD_MEMBER         ( bool,         b,                  0                    );
END_BITFIELD

TEST (BasicTest, DirectiveOrder2)
{
    // instantiate bit-field and get layout
    DirectiveOrder2T& val = *((DirectiveOrder2T*) 1234);
    ::__bitfields::test::MockedLayout* layout = ::BitfieldTests::getLayout (val.xx);

    ASSERT_EQ (layout->storageAlignment, 8);

    ASSERT_EQ (layout->storageRelaxed, true);
    ASSERT_EQ (layout->takeOuterStorage, true);
    ASSERT_EQ (layout->takeOwnStorage, false);

    // clean up
    delete layout;
}


// directives order #3
//                          | data type |   name |  arraysize | offset |    bitcount |
DECLARE_BITFIELD (DirectiveOrder3T, uint16_t, Alignment<8>, TakeOuterStorage, RelaxedStorage)
//  BITFIELD_MEMBER         ( uint8_t,      xx,                 offset(4),  2        );
    BITFIELD_MEMBER         ( uint8_t,      xx,                 4,          2        );
    BITFIELD_MEMBER         ( bool,         b,                  0                    );
END_BITFIELD

TEST (BasicTest, DirectiveOrder3)
{
    // instantiate bit-field and get layout
    DirectiveOrder3T& val = *((DirectiveOrder3T*) 1234);
    ::__bitfields::test::MockedLayout* layout = ::BitfieldTests::getLayout (val.xx);

    ASSERT_EQ (layout->storageAlignment, 8);

    ASSERT_EQ (layout->storageRelaxed, true);
    ASSERT_EQ (layout->takeOuterStorage, true);
    ASSERT_EQ (layout->takeOwnStorage, false);

    // clean up
    delete layout;
}
