//=================================================================================================
//
//   Compiler independent safe bit-fields for C++98
//
//   File:          /test/test_arrays.cpp
//
//   Description:   Test case implementation for bit-field array members.
//
//   License:       Copyright (c) 2018 Julian Dessecker
//
//                  Permission is hereby granted, free of charge, to any person obtaining
//                  a copy of this software and associated documentation files
//                  (the "Software"), to deal in the Software without restriction,
//                  including without limitation the rights to use, copy, modify, merge,
//                  publish, distribute, sublicense, and/or sell copies of the Software,
//                  and to permit persons to whom the Software is furnished to do so,
//                  subject to the following conditions:
//
//                  The above copyright notice and this permission notice shall be
//                  included in all copies or substantial portions of the Software.
//
//                  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
//                  EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
//                  OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
//                  NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS
//                  BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN
//                  ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
//                  CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
//                  SOFTWARE.
//
//=================================================================================================


#include <gtest/gtest.h>
#include "api/bitfield.h"
#include "include/bitfield/testadapter.h"
#include "mockedstorage.h"
#include "testutil.h"


//-------------------------------------------------------------------------------------------------
// Basic array declaration tests
//-------------------------------------------------------------------------------------------------

//                          | data type |   name |  arraysize | offset |    bitcount |
DECLARE_BITFIELD (StorageArraySimpleT, uint8_t)
    BITFIELD_ARRAY_MEMBER   ( uint8_t,      xx,     12,         4,          4        );
    BITFIELD_MEMBER         ( bool,         b,                  0                    );
END_BITFIELD

TEST (ArrayTest, StorageArraySimple)
{
    // instantiate bit-field and get layout
    StorageArraySimpleT val;
    ::__bitfields::test::MockedLayout* layout = ::BitfieldTests::getLayout (val.xx);

    // assert we got a layout information
    ASSERT_NE (layout, nullptr);

    // match storage type name
    ASSERT_EQ (Util::matchesType<uint8_t> (layout->StorageType), true);

    // assert array member type is default
    ASSERT_EQ (Util::matchesType<__bitfields::DefaultArrayAddressConversion>
        (layout->ArrayAddressConversion), true);

    // assert bit-field size (4 offset + 12 * 4 bit)
    ASSERT_EQ (layout->bitfieldBits, 4 + 12 * 4);

    // assert type size matches
    ASSERT_EQ (layout->storageElemetSize, 8);

    // assert default alignment is correct
    ASSERT_EQ (layout->storageAlignment, 8);

    // assert storage qualifiers
    ASSERT_EQ (layout->storageIsConst, false);
    ASSERT_EQ (layout->storageIsVolatile, false);

    // assert final const modifier is not set
    ASSERT_EQ (layout->isConst, false);

    // assert bit count matches one element
    ASSERT_EQ (layout->bits, 4);

    // assert member is array type
    ASSERT_EQ (layout->isArray, true);

    // assert member is array type
    ASSERT_EQ (layout->arraySize, 12);

    // assert array index is not reported as reversed
    ASSERT_EQ (layout->arrayIndexReversed, false);

    // assert local offset matches
    ASSERT_EQ (layout->localOffset, 4);

    // clean up
    delete layout;
};


//-------------------------------------------------------------------------------------------------
// Test array with reverse index
//-------------------------------------------------------------------------------------------------

//                          | data type |   name |  arraysize | offset |    bitcount |
DECLARE_BITFIELD (StorageArrayReverseT, uint16_t)
    BITFIELD_ARRAY_MEMBER   ( uint8_t,      xx,     -47,        96,         4        );
    BITFIELD_MEMBER         ( bool,         b,                  0                    );
END_BITFIELD

TEST (ArrayTest, StorageArraySimpleRev)
{
    // instantiate bit-field and get layout
    StorageArrayReverseT val;
    ::__bitfields::test::MockedLayout* layout = ::BitfieldTests::getLayout (val.xx);

    // assert we got a layout information
    ASSERT_NE (layout, nullptr);

    // match storage type name
    ASSERT_EQ (Util::matchesType<uint16_t> (layout->StorageType), true);

    // assert bit-field size (95 offset + 12 * 4 bit)
    ASSERT_EQ (layout->bitfieldBits, 96 + 47 * 4);

    // assert type size matches
    ASSERT_EQ (layout->storageElemetSize, 16);

    // assert default alignment is correct
    ASSERT_EQ (layout->storageAlignment, 16);

    // assert storage qualifiers
    ASSERT_EQ (layout->storageIsConst, false);
    ASSERT_EQ (layout->storageIsVolatile, false);

    // assert final const modifier is not set
    ASSERT_EQ (layout->isConst, false);

    // assert bit count matches one element
    ASSERT_EQ (layout->bits, 4);

    // assert member is array type
    ASSERT_EQ (layout->isArray, true);

    // assert member is array type
    ASSERT_EQ (layout->arraySize, 47);

    // assert array index is not reported as reversed
    ASSERT_EQ (layout->arrayIndexReversed, true);

    // assert local offset matches
    ASSERT_EQ (layout->localOffset, 96);

    // clean up
    delete layout;
};


//-------------------------------------------------------------------------------------------------
// Test default bit-count works for array declarations
//-------------------------------------------------------------------------------------------------

//                          | data type |   name |  arraysize | offset |    bitcount |
DECLARE_BITFIELD (StorageArrayDefaultBitCountT, uint16_t)
    BITFIELD_ARRAY_MEMBER   ( uint8_t,      xx,     -47,        96                   );
    BITFIELD_MEMBER         ( bool,         b,                  0                    );
END_BITFIELD

TEST (ArrayTest, StorageArrayDefaultBitCount)
{
    // instantiate bit-field and get layout
    StorageArrayDefaultBitCountT val;
    ::__bitfields::test::MockedLayout* layout = ::BitfieldTests::getLayout (val.xx);

    // assert we got a layout information
    ASSERT_NE (layout, nullptr);

    // assert bit-field size (95 offset + 12 * 8 bit)
    ASSERT_EQ (layout->bitfieldBits, 96 + 47 * 8);

    // assert type size matches
    ASSERT_EQ (layout->storageElemetSize, 16);

    // assert bit count matches one element
    ASSERT_EQ (layout->bits, 8);

    // assert member is array type
    ASSERT_EQ (layout->isArray, true);

    // clean up
    delete layout;
};


//-------------------------------------------------------------------------------------------------
// Overload array address convertion
//-------------------------------------------------------------------------------------------------

struct TestArrayAddressConversion: public __bitfields::IsArrayAddressConversionFlag
{
    static inline int convertArrayAddr (int inbound)
    {
        return inbound - 1;
    }
};

//                          | data type |   name |  arraysize | offset |    bitcount |
DECLARE_BITFIELD (StorageArrayAddrConvertT, uint16_t)
    BITFIELD_ARRAY_MEMBER   ( uint8_t,      xx,     -47,        96,         8 /* AUTO */, TestArrayAddressConversion);
    BITFIELD_MEMBER         ( bool,         b,                  0                    );
END_BITFIELD

TEST (ArrayTest, StorageArrayAddrConvert)
{
    // instantiate bit-field and get layout
    StorageArrayAddrConvertT val;
    ::__bitfields::test::MockedLayout* layout = ::BitfieldTests::getLayout (val.xx);

    // assert we got a layout information
    ASSERT_NE (layout, nullptr);

    // assert array member type is overwritten type
    ASSERT_EQ (Util::matchesType<TestArrayAddressConversion>
        (layout->ArrayAddressConversion), true);

    // assert member is array type
    ASSERT_EQ (layout->isArray, true);

    // clean up
    delete layout;
};
