//=================================================================================================
//
//   Compiler independent safe bit-fields for C++98
//
//   File:          /test/mockedstorage.h
//
//   Description:   A mocked storage backend for tracing storage access for unit-tests
//
//   License:       Copyright (c) 2018 Julian Dessecker
//
//                  Permission is hereby granted, free of charge, to any person obtaining
//                  a copy of this software and associated documentation files
//                  (the "Software"), to deal in the Software without restriction,
//                  including without limitation the rights to use, copy, modify, merge,
//                  publish, distribute, sublicense, and/or sell copies of the Software,
//                  and to permit persons to whom the Software is furnished to do so,
//                  subject to the following conditions:
//
//                  The above copyright notice and this permission notice shall be
//                  included in all copies or substantial portions of the Software.
//
//                  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
//                  EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
//                  OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
//                  NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS
//                  BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN
//                  ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
//                  CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
//                  SOFTWARE.
//
//=================================================================================================


#pragma once


#include "include/bitfield/testadapter.h"


struct MockedStorageBackend: public ::__bitfields::test::IMockedStorage
{
    // callbacks to track storage backend invocations
    virtual void onGetArray (::__bitfields::test::MockedLayout& layout, const void* basePtr, int offset)
    {
        printf ("get array\n");
        layout.dump ("");
    };

    virtual void onGetScalar (::__bitfields::test::MockedLayout& layout, const void* basePtr)
    {
        printf ("get scalar\n");
        layout.dump ("");
    };

    virtual void onSetArray (::__bitfields::test::MockedLayout& layout, void* basePtr, int offset)
    {
        printf ("set array\n");
        layout.dump ("");
    };

    virtual void onSetScalar (::__bitfields::test::MockedLayout& layout, void* basePtr)
    {
        printf ("set scalar\n");
        layout.dump ("");
    };
};
