//=================================================================================================
//
//   Compiler independent safe bit-fields for C++98
//
//   File:          /include/bitfield/testadapter.h
//
//   Description:   This file implements a mocked storage backend for testing the declaration and
//                  wrapper mechanisms.
//
//   License:       Copyright (c) 2018 Julian Dessecker
//
//                  Permission is hereby granted, free of charge, to any person obtaining
//                  a copy of this software and associated documentation files
//                  (the "Software"), to deal in the Software without restriction,
//                  including without limitation the rights to use, copy, modify, merge,
//                  publish, distribute, sublicense, and/or sell copies of the Software,
//                  and to permit persons to whom the Software is furnished to do so,
//                  subject to the following conditions:
//
//                  The above copyright notice and this permission notice shall be
//                  included in all copies or substantial portions of the Software.
//
//                  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
//                  EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
//                  OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
//                  NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS
//                  BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN
//                  ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
//                  CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
//                  SOFTWARE.
//
//=================================================================================================


#pragma once


// check if we are included from top-level header
#ifndef __BITFIELDS__INCLUDE__GUARD__
#   error Do not include this file directly, inlude bitfield.h instead
#endif


#include "stubs.h"
#include "backend.h"


#ifdef __BITFIELDS_TEST_MODE


#include <typeinfo>


//-------------------------------------------------------------------------------------------------
// forward declarations
//-------------------------------------------------------------------------------------------------
struct BitfieldTests;
namespace __bitfields {
    namespace test {
        struct MockedLayout;
        struct IMockedStorage;
    }
}


//-------------------------------------------------------------------------------------------------
// get layout if we have an underlying bit-field member (false condition)
//-------------------------------------------------------------------------------------------------
namespace __bitfields {
    template<typename T, bool cond>
    struct SelectTestLayoutGetter
    {
        static inline ::__bitfields::test::MockedLayout* getLayout ()
        {
            return 0;
        }
    };
} // namespace __bitfields


//-------------------------------------------------------------------------------------------------
// interface for bit-field tests
//-------------------------------------------------------------------------------------------------
struct BitfieldTests
{
    // setters for test mock
    static void setStorageBackendMock (::__bitfields::test::IMockedStorage* mock);
    static void setDummyBackendRead (bool useDummy);
    static void setDummyBackendWrite (bool useDummy);
    static void reset ();

    // getters for test mock
    static ::__bitfields::test::IMockedStorage* getStorageBackendMock ();
    static bool useDummyBackendRead ();
    static bool useDummyBackendWrite ();

    // get mocked layout for bit-field member
    template<typename T>
    inline static __bitfields::test::MockedLayout* getLayout ()
    {
        return ::__bitfields::SelectTestLayoutGetter
            <T, ::dstd::meta::IsBaseClassOf<
                ::__bitfields::ContainsBitfieldLayout,
                typename ::dstd::meta::StripQualifiers<T>::T>
            ::value>::getLayout ();
    }

    // get mocked layout for bit-field member variable
    template<typename T>
    inline static __bitfields::test::MockedLayout* getLayout (const T& var)
    {
        return getLayout<T> ();
    }
};


namespace __bitfields {
namespace test {


//-------------------------------------------------------------------------------------------------
// Conditionally get mocked layout (false case)
//-------------------------------------------------------------------------------------------------
template<typename L, bool isNested>
struct MockedLayoutCond
{
    static MockedLayout* create () {return 0;}
};


//-------------------------------------------------------------------------------------------------
// Runtime layout representation for testing of bit-fields
//-------------------------------------------------------------------------------------------------
struct MockedLayout
{
    //                                               | Basic decl   |

    // declaration names
    const char* ArrayAddressConversion;     // Test:
    const char* outerName;                  // Test: |  ignored     |
    const char* innerName;                  // Test: |  ignored     |

    // view / array conversions
    const char* BackendType;                // Test: |  ignored     |
    const char* AttributesType;             // Test: |  ignored     |
    bool isArrayBackendLocal;               // Test: |  checked     |
    bool hasArrayBackend;                   // Test: |  checked     |

    // member type
    const char* MemberType;                 // Test: |  checked     |
    const char* MemberTypeNoQual;           // Test: |  ignored     |
    bool memberIsConst;                     // Test: |  checked     |
    bool memberIsVolatile;                  // Test: |  checked     |
    int localOffset;                        // Test: |  checked     |

    // nesting
    bool isNested;                          // Test: |  checked     |
    bool hostingNested;                     // Test: |  checked     |
    int nestedOffset;                       // Test: |  checked     |
    MockedLayout* NestingInformation;       // Test: |  checked     |

    // storage backend
    const char* StorageType;                // Test: |  checked     |
    int storageElemetSize;                  // Test: |  checked     |
    bool storageIsConst;                    // Test: |  checked     |
    bool storageIsVolatile;                 // Test: |  checked     |
    int storageAlignment;                   // Test: |  checked     |
    bool storageRelaxed;                    // Test: |  checked     |
    bool takeOwnStorage;                    // Test: |  checked     |
    bool takeOuterStorage;                  // Test: |  checked     |
    int bitfieldBits;                       // Test: |  checked     |

    // view conversion
    bool hasView;                           // Test: |  checked     |
    const char* ViewType;                   // Test: |  ignored     |

    // array
    bool isArray;                           // Test: |  checked     |
    int arraySizeRaw;                       // Test: |  checked     |
    int arraySize;                          // Test: |  checked     |
    bool arrayIndexReversed;                // Test: |  checked     |

    // resolved layout
    const char* RepresentationType;         // Test: |  checked     |
    const char* RepresentationTypeNoQual;   // Test: |  ignored     |
    bool representationIsConst;             // Test: |  checked     |
    bool representationIsVolatile;          // Test: |  checked     |
    int bits;                               // Test: |  checked     |
    bool isConst;                           // Test: |  checked     |
    int offset;                             // Test: |  checked     |

    // delete mocked layout
    ~MockedLayout ()
    {
        delete NestingInformation;
    }

    // create from meta layout information
    template<typename L>
    static MockedLayout* create ()
    {
        MockedLayout* l = new MockedLayout ();

        // declaration names
        l->ArrayAddressConversion = typeid (typename L::ArrayAddressConversion).name ();
        l->innerName = L::Names::getInner ();
        l->outerName = L::Names::getOuter ();

        // view / array converstions
        l->BackendType = typeid (typename L::Backend).name ();
        l->AttributesType = typeid (typename L::Attributes).name ();
        l->ViewType = typeid (typename L::ViewType).name ();
        l->isArrayBackendLocal = L::isArrayBackendLocal;
        l->hasArrayBackend = L::hasArrayBackend;

        // member type
        l->MemberType = typeid (typename L::MemberType).name ();
        l->MemberTypeNoQual = typeid (typename L::MemberTypeNoQual).name ();
        l->memberIsConst = L::memberIsConst;
        l->memberIsVolatile = TypeInfo<typename L::MemberType>::isVolatile;
        l->localOffset = L::localOffset;

        // nesting
        l->isNested = L::isNested;
        l->hostingNested = L::hostingNested;
        l->nestedOffset = L::nestedOffset;
        if (L::isNested)
        {
            l->NestingInformation =
                MockedLayoutCond<typename L::NestingParent, L::isNested>::create ();
        }
        else
        {
            l->NestingInformation = 0;
        }

        // storage backend
        l->StorageType = typeid (typename L::StorageType).name ();
        l->storageElemetSize = sizeof (typename L::StorageType) * 8;
        l->storageIsConst = L::storageIsConst;
        l->storageIsVolatile = L::storageIsVolatile;
        l->storageAlignment = L::storageAlignment;
        l->storageRelaxed = L::storageRelaxed;
        l->takeOwnStorage = L::takeOwnStorage;
        l->takeOuterStorage = L::takeOuterStorage;
        l->bitfieldBits = L::bitfieldBits;

        // view conversion
        l->hasView = L::hasView;
        l->ViewType = typeid (typename L::ViewT).name ();

        // array
        l->isArray = L::isArray;
        l->arraySizeRaw = L::arraySizeRaw;
        l->arraySize = L::arraySize;
        l->arrayIndexReversed = L::arrayIndexReversed;

        // resolved layout
        l->RepresentationType = typeid (typename L::Representation).name ();
        l->RepresentationTypeNoQual = typeid (typename L::RepresentationNoQual).name ();
        l->representationIsConst = TypeInfo<typename L::Representation>::isConst;
        l->representationIsVolatile = TypeInfo<typename L::Representation>::isVolatile;
        l->bits = L::bits;
        l->isConst = L::isConst;
        l->offset = L::offset;

        return l;
    }

    // dump to console
    void indentWrite (int ident, const char* fmt, ...);
    void dump (const char* name, bool compact = true, int indent = 0);
};


//-------------------------------------------------------------------------------------------------
// Conditionally get mocked layout (true case)
//-------------------------------------------------------------------------------------------------
template<typename L>
struct MockedLayoutCond<L, true>
{
    static MockedLayout* create () {return MockedLayout::create<L> ();}
};


//-------------------------------------------------------------------------------------------------
// Listener implementation for mocked backend
//-------------------------------------------------------------------------------------------------
struct IMockedStorage
{
    virtual ~IMockedStorage () {}

    // callbacks to track storage backend invocations
    virtual void onGetArray (MockedLayout& layout, const void* basePtr, int offset) {};
    virtual void onGetScalar (MockedLayout& layout, const void* basePtr) {};
    virtual void onSetArray (MockedLayout& layout, void* basePtr, int offset) {};
    virtual void onSetScalar (MockedLayout& layout, void* basePtr) {};
};


//-------------------------------------------------------------------------------------------------
// Mocked storage backend
//-------------------------------------------------------------------------------------------------
template<typename layout>
struct MockedStorageBackend
{
    typename layout::RepresentationNoQual __get () const
    {
        IMockedStorage* mock = BitfieldTests::getStorageBackendMock ();

        // storage backend mock set?
        if (mock)
        {
            MockedLayout* ml = MockedLayout::create<layout> ();
            if (layout::isArrayBackendLocal)
            {
                const ArrayReferenceBase* arrayRef
                    = reinterpret_cast<const ArrayReferenceBase*> (this);
                mock->onGetArray (*ml, arrayRef->basePointer, arrayRef->offsetBits);
            }
            else
            {
                mock->onGetScalar (*ml, (const void*) this);
            }
            delete ml;
            if (BitfieldTests::useDummyBackendRead ())
            {
                return (typename layout::RepresentationNoQual) 0;
            }
        }

        // get from standard storage backend
        typedef typename StorageBackendSelector<layout>::T BE;
        return reinterpret_cast<const BE*> (this)->__get ();
    }

    // set member -> inform global mock
    void __set (const typename layout::RepresentationNoQual& v)
    {
        IMockedStorage* mock = BitfieldTests::getStorageBackendMock ();

        // storage backend mock set?
        if (mock)
        {
            MockedLayout* ml = MockedLayout::create<layout> ();
            if (layout::isArrayBackendLocal)
            {
                const ArrayReferenceBase* arrayRef
                    = reinterpret_cast<const ArrayReferenceBase*> (this);
                mock->onSetArray (*ml, arrayRef->basePointer, arrayRef->offsetBits);
            }
            else
            {
                mock->onSetScalar (*ml, (void*) this);
            }
            delete ml;
            if (BitfieldTests::useDummyBackendWrite ())
            {
                return;
            }
        }

        // set on standard storage backend
        typedef typename StorageBackendSelector<layout>::T BE;
        reinterpret_cast<BE*> (this)->__set (v);
    }
};


} // namespace test
} // namespace __bitfields


//-------------------------------------------------------------------------------------------------
// testing activated, use mocked backend
//-------------------------------------------------------------------------------------------------
namespace __bitfields {
    template<typename layout>
    struct StorageSelectorWithTests
    {
        typedef test::MockedStorageBackend<layout> T;
    };
} // namespace __bitfields


#else


//-------------------------------------------------------------------------------------------------
// testing not activated, delegate to standard storage backend
//-------------------------------------------------------------------------------------------------
namespace __bitfields {
    template<typename layout>
    struct StorageSelectorWithTests
    {
        typedef typename StorageBackendSelector<layout>::T T;
    };
} // namespace __bitfields


#endif
