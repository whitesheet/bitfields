//=================================================================================================
//
//   Compiler independent safe bit-fields for C++98
//
//   File:          /include/bitfield/initializer.h
//
//   Description:   This file implements compile-time optimized initializers for bit-fields.
//
//   License:       Copyright (c) 2018 Julian Dessecker
//
//                  Permission is hereby granted, free of charge, to any person obtaining
//                  a copy of this software and associated documentation files
//                  (the "Software"), to deal in the Software without restriction,
//                  including without limitation the rights to use, copy, modify, merge,
//                  publish, distribute, sublicense, and/or sell copies of the Software,
//                  and to permit persons to whom the Software is furnished to do so,
//                  subject to the following conditions:
//
//                  The above copyright notice and this permission notice shall be
//                  included in all copies or substantial portions of the Software.
//
//                  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
//                  EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
//                  OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
//                  NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS
//                  BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN
//                  ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
//                  CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
//                  SOFTWARE.
//
//=================================================================================================


#pragma once


// check if we are included from top-level header
#ifndef __BITFIELDS__INCLUDE__GUARD__
#   error Do not include this file directly, inlude bitfield.h instead
#endif


#include "../util/typeinfo.h"


namespace __bitfields {


//-------------------------------------------------------------------------------------------------
// convert T& to T
//-------------------------------------------------------------------------------------------------
template<typename TP>
struct InitializerStripReference
{
    typedef TP T;
};

template<typename TP>
struct InitializerStripReference<TP&>
{
    typedef TP T;
};


//-------------------------------------------------------------------------------------------------
// Cast bit-field to initializer type (i.e. relax storage order)
//-------------------------------------------------------------------------------------------------
template<typename T> typename T::template __ForceNonVolatile<1>::T& initializerCast
    (char* noArray, T& ref)
{
    (void) noArray;
    return *(reinterpret_cast<typename T::template __ForceNonVolatile<1>::T*> (&ref));
}

// array reference needs to be copied since it's a stack value
template<typename T> typename T::template __ForceNonVolatile<1>::T initializerCast
    (void* isArray, T ref)
{
    (void) isArray;
    return *(reinterpret_cast<typename T::template __ForceNonVolatile<1>::T*> (&ref));
}


//-------------------------------------------------------------------------------------------------
// Cast bit-field to relaxed reader type (i.e. relax storage order)
//-------------------------------------------------------------------------------------------------
template<typename T> const typename T::template __ForceNonVolatile<2>::T& combinedReaderCast
    (char* noArray, const T& ref)
{
    (void) noArray;
    return *(reinterpret_cast<const typename T::template __ForceNonVolatile<2>::T*> (&ref));
}

// array reference needs to be copied since it's a stack value
template<typename T> const typename T::template __ForceNonVolatile<2>::T combinedReaderCast
    (void* isArray, T ref)
{
    (void) isArray;
    return *(reinterpret_cast<typename T::template __ForceNonVolatile<2>::T*> (&ref));
}


//-------------------------------------------------------------------------------------------------
// issue a memory barrier and return true
//-------------------------------------------------------------------------------------------------
inline bool MemoryBarrierReturnTrue ()
{
    __BITFIELDS__PORT_MEMBARRIER;
    return true;
}


//-------------------------------------------------------------------------------------------------
// issue a memory barrier and return false
//-------------------------------------------------------------------------------------------------
inline bool MemoryBarrierReturnFalse ()
{
    __BITFIELDS__PORT_MEMBARRIER;
    return false;
}


} // namespace __bitfields


//-------------------------------------------------------------------------------------------------
// iterate variadic macro arguments and invoke initializer statements
//-------------------------------------------------------------------------------------------------

// invoke assignment
#define __BITFIELDS_ASSIGN(var, expression) var.expression
#define __BITFIELDS_CONCAT_SEMICOLON ;
#define __BITFIELDS_CONCAT_ANDAND &&
#define __BITFIELDS_CONCAT_OROR ||

// __BITFIELDS_CONCAT macro names
#define __BITFIELDS_CONCAT(arg1, arg2) __BITFIELDS_CONCAT1(arg1, arg2)
#define __BITFIELDS_CONCAT1(arg1, arg2) __BITFIELDS_CONCAT2(arg1, arg2)
#define __BITFIELDS_CONCAT2(arg1, arg2) arg1##arg2

// variadic arguments iterator (calls __BITFIELDS_ASSIGN for each argument)
#define __BITFIELDS_INIT_DECL_1(cop, var, xpression, ...) \
    __BITFIELDS_ASSIGN(var, xpression)

#define __BITFIELDS_INIT_DECL_2(cop, var, xpression, ...) \
    __BITFIELDS_ASSIGN(var, xpression) cop \
    __BITFIELDS_INIT_DECL_1(cop, var, __VA_ARGS__)

#define __BITFIELDS_INIT_DECL_3(cop, var, xpression, ...) \
    __BITFIELDS_ASSIGN(var, xpression) cop \
    __BITFIELDS_INIT_DECL_2(cop, var, __VA_ARGS__)

#define __BITFIELDS_INIT_DECL_4(cop, var, xpression, ...) \
    __BITFIELDS_ASSIGN(var, xpression) cop \
    __BITFIELDS_INIT_DECL_3(cop, var, __VA_ARGS__)

#define __BITFIELDS_INIT_DECL_5(cop, var, xpression, ...) \
    __BITFIELDS_ASSIGN(var, xpression) cop \
    __BITFIELDS_INIT_DECL_4(cop, var, __VA_ARGS__)

#define __BITFIELDS_INIT_DECL_6(cop, var, xpression, ...) \
    __BITFIELDS_ASSIGN(var, xpression) cop \
    __BITFIELDS_INIT_DECL_5(cop, var, __VA_ARGS__)

#define __BITFIELDS_INIT_DECL_7(cop, var, xpression, ...) \
    __BITFIELDS_ASSIGN(var, xpression) cop \
    __BITFIELDS_INIT_DECL_6(cop, var, __VA_ARGS__)

#define __BITFIELDS_INIT_DECL_8(cop, var, xpression, ...) \
    __BITFIELDS_ASSIGN(var, xpression) cop \
    __BITFIELDS_INIT_DECL_7(cop, var, __VA_ARGS__)

#define __BITFIELDS_INIT_DECL_9(cop, var, xpression, ...) \
    __BITFIELDS_ASSIGN(var, xpression) cop \
    __BITFIELDS_INIT_DECL_8(cop, var, __VA_ARGS__)

#define __BITFIELDS_INIT_DECL_10(cop, var, xpression, ...) \
    __BITFIELDS_ASSIGN(var, xpression) cop \
    __BITFIELDS_INIT_DECL_9(cop, var, __VA_ARGS__)

#define __BITFIELDS_INIT_DECL_11(cop, var, xpression, ...) \
    __BITFIELDS_ASSIGN(var, xpression) cop \
    __BITFIELDS_INIT_DECL_10(cop, var, __VA_ARGS__)

#define __BITFIELDS_INIT_DECL_12(cop, var, xpression, ...) \
    __BITFIELDS_ASSIGN(var, xpression) cop \
    __BITFIELDS_INIT_DECL_11(cop, var, __VA_ARGS__)

#define __BITFIELDS_INIT_DECL_13(cop, var, xpression, ...) \
    __BITFIELDS_ASSIGN(var, xpression) cop \
    __BITFIELDS_INIT_DECL_12(cop, var, __VA_ARGS__)

#define __BITFIELDS_INIT_DECL_14(cop, var, xpression, ...) \
    __BITFIELDS_ASSIGN(var, xpression) cop \
    __BITFIELDS_INIT_DECL_13(cop, var, __VA_ARGS__)

#define __BITFIELDS_INIT_DECL_15(cop, var, xpression, ...) \
    __BITFIELDS_ASSIGN(var, xpression) cop \
    __BITFIELDS_INIT_DECL_14(cop, var, __VA_ARGS__)

#define __BITFIELDS_INIT_DECL_16(cop, var, xpression, ...) \
    __BITFIELDS_ASSIGN(var, xpression) cop \
    __BITFIELDS_INIT_DECL_15(cop, var, __VA_ARGS__)

#define __BITFIELDS_INIT_DECL_17(cop, var, xpression, ...) \
    __BITFIELDS_ASSIGN(var, xpression) cop \
    __BITFIELDS_INIT_DECL_16(cop, var, __VA_ARGS__)

#define __BITFIELDS_INIT_DECL_18(cop, var, xpression, ...) \
    __BITFIELDS_ASSIGN(var, xpression) cop \
    __BITFIELDS_INIT_DECL_17(cop, var, __VA_ARGS__)

#define __BITFIELDS_INIT_DECL_19(cop, var, xpression, ...) \
    __BITFIELDS_ASSIGN(var, xpression) cop \
    __BITFIELDS_INIT_DECL_18(cop, var, __VA_ARGS__)

#define __BITFIELDS_INIT_DECL_20(cop, var, xpression, ...) \
    __BITFIELDS_ASSIGN(var, xpression) cop \
    __BITFIELDS_INIT_DECL_19(cop, var, __VA_ARGS__)

#define __BITFIELDS_INIT_DECL_21(cop, var, xpression, ...) \
    __BITFIELDS_ASSIGN(var, xpression) cop \
    __BITFIELDS_INIT_DECL_20(cop, var, __VA_ARGS__)

#define __BITFIELDS_INIT_DECL_22(cop, var, xpression, ...) \
    __BITFIELDS_ASSIGN(var, xpression) cop \
    __BITFIELDS_INIT_DECL_21(cop, var, __VA_ARGS__)

#define __BITFIELDS_INIT_DECL_23(cop, var, xpression, ...) \
    __BITFIELDS_ASSIGN(var, xpression) cop \
    __BITFIELDS_INIT_DECL_22(cop, var, __VA_ARGS__)

#define __BITFIELDS_INIT_DECL_24(cop, var, xpression, ...) \
    __BITFIELDS_ASSIGN(var, xpression) cop \
    __BITFIELDS_INIT_DECL_23(cop, var, __VA_ARGS__)

#define __BITFIELDS_INIT_DECL_25(cop, var, xpression, ...) \
    __BITFIELDS_ASSIGN(var, xpression) cop \
    __BITFIELDS_INIT_DECL_24(cop, var, __VA_ARGS__)

#define __BITFIELDS_INIT_DECL_26(cop, var, xpression, ...) \
    __BITFIELDS_ASSIGN(var, xpression) cop \
    __BITFIELDS_INIT_DECL_25(cop, var, __VA_ARGS__)

#define __BITFIELDS_INIT_DECL_27(cop, var, xpression, ...) \
    __BITFIELDS_ASSIGN(var, xpression) cop \
    __BITFIELDS_INIT_DECL_26(cop, var, __VA_ARGS__)

#define __BITFIELDS_INIT_DECL_28(cop, var, xpression, ...) \
    __BITFIELDS_ASSIGN(var, xpression) cop \
    __BITFIELDS_INIT_DECL_27(cop, var, __VA_ARGS__)

#define __BITFIELDS_INIT_DECL_29(cop, var, xpression, ...) \
    __BITFIELDS_ASSIGN(var, xpression) cop \
    __BITFIELDS_INIT_DECL_28(cop, var, __VA_ARGS__)

#define __BITFIELDS_INIT_DECL_30(cop, var, xpression, ...) \
    __BITFIELDS_ASSIGN(var, xpression) cop \
    __BITFIELDS_INIT_DECL_29(cop, var, __VA_ARGS__)

#define __BITFIELDS_INIT_DECL_31(cop, var, xpression, ...) \
    __BITFIELDS_ASSIGN(var, xpression) cop \
    __BITFIELDS_INIT_DECL_30(cop, var, __VA_ARGS__)

#define __BITFIELDS_INIT_DECL_32(cop, var, xpression, ...) \
    __BITFIELDS_ASSIGN(var, xpression) cop \
    __BITFIELDS_INIT_DECL_31(cop, var, __VA_ARGS__)

#define __BITFIELDS_INIT_DECL_33(cop, var, xpression, ...) \
    __BITFIELDS_ASSIGN(var, xpression) cop \
    __BITFIELDS_INIT_DECL_32(cop, var, __VA_ARGS__)

#define __BITFIELDS_INIT_DECL_34(cop, var, xpression, ...) \
    __BITFIELDS_ASSIGN(var, xpression) cop \
    __BITFIELDS_INIT_DECL_33(cop, var, __VA_ARGS__)

#define __BITFIELDS_INIT_DECL_35(cop, var, xpression, ...) \
    __BITFIELDS_ASSIGN(var, xpression) cop \
    __BITFIELDS_INIT_DECL_34(cop, var, __VA_ARGS__)

#define __BITFIELDS_INIT_DECL_36(cop, var, xpression, ...) \
    __BITFIELDS_ASSIGN(var, xpression) cop \
    __BITFIELDS_INIT_DECL_35(cop, var, __VA_ARGS__)

#define __BITFIELDS_INIT_DECL_37(cop, var, xpression, ...) \
    __BITFIELDS_ASSIGN(var, xpression) cop \
    __BITFIELDS_INIT_DECL_36(cop, var, __VA_ARGS__)

#define __BITFIELDS_INIT_DECL_38(cop, var, xpression, ...) \
    __BITFIELDS_ASSIGN(var, xpression) cop \
    __BITFIELDS_INIT_DECL_37(cop, var, __VA_ARGS__)

#define __BITFIELDS_INIT_DECL_39(cop, var, xpression, ...) \
    __BITFIELDS_ASSIGN(var, xpression) cop \
    __BITFIELDS_INIT_DECL_38(cop, var, __VA_ARGS__)

#define __BITFIELDS_INIT_DECL_40(cop, var, xpression, ...) \
    __BITFIELDS_ASSIGN(var, xpression) cop \
    __BITFIELDS_INIT_DECL_39(cop, var, __VA_ARGS__)

#define __BITFIELDS_INIT_DECL_41(cop, var, xpression, ...) \
    __BITFIELDS_ASSIGN(var, xpression) cop \
    __BITFIELDS_INIT_DECL_40(cop, var, __VA_ARGS__)

#define __BITFIELDS_INIT_DECL_42(cop, var, xpression, ...) \
    __BITFIELDS_ASSIGN(var, xpression) cop \
    __BITFIELDS_INIT_DECL_41(cop, var, __VA_ARGS__)

#define __BITFIELDS_INIT_DECL_43(cop, var, xpression, ...) \
    __BITFIELDS_ASSIGN(var, xpression) cop \
    __BITFIELDS_INIT_DECL_42(cop, var, __VA_ARGS__)

#define __BITFIELDS_INIT_DECL_44(cop, var, xpression, ...) \
    __BITFIELDS_ASSIGN(var, xpression) cop \
    __BITFIELDS_INIT_DECL_43(cop, var, __VA_ARGS__)

#define __BITFIELDS_INIT_DECL_45(cop, var, xpression, ...) \
    __BITFIELDS_ASSIGN(var, xpression) cop \
    __BITFIELDS_INIT_DECL_44(cop, var, __VA_ARGS__)

#define __BITFIELDS_INIT_DECL_46(cop, var, xpression, ...) \
    __BITFIELDS_ASSIGN(var, xpression) cop \
    __BITFIELDS_INIT_DECL_45(cop, var, __VA_ARGS__)

#define __BITFIELDS_INIT_DECL_47(cop, var, xpression, ...) \
    __BITFIELDS_ASSIGN(var, xpression) cop \
    __BITFIELDS_INIT_DECL_46(cop, var, __VA_ARGS__)

#define __BITFIELDS_INIT_DECL_48(cop, var, xpression, ...) \
    __BITFIELDS_ASSIGN(var, xpression) cop \
    __BITFIELDS_INIT_DECL_47(cop, var, __VA_ARGS__)

#define __BITFIELDS_INIT_DECL_49(cop, var, xpression, ...) \
    __BITFIELDS_ASSIGN(var, xpression) cop \
    __BITFIELDS_INIT_DECL_48(cop, var, __VA_ARGS__)

#define __BITFIELDS_INIT_DECL_50(cop, var, xpression, ...) \
    __BITFIELDS_ASSIGN(var, xpression) cop \
    __BITFIELDS_INIT_DECL_49(cop, var, __VA_ARGS__)

#define __BITFIELDS_INIT_DECL_51(cop, var, xpression, ...) \
    __BITFIELDS_ASSIGN(var, xpression) cop \
    __BITFIELDS_INIT_DECL_50(cop, var, __VA_ARGS__)

#define __BITFIELDS_INIT_DECL_52(cop, var, xpression, ...) \
    __BITFIELDS_ASSIGN(var, xpression) cop \
    __BITFIELDS_INIT_DECL_51(cop, var, __VA_ARGS__)

#define __BITFIELDS_INIT_DECL_53(cop, var, xpression, ...) \
    __BITFIELDS_ASSIGN(var, xpression) cop \
    __BITFIELDS_INIT_DECL_52(cop, var, __VA_ARGS__)

#define __BITFIELDS_INIT_DECL_54(cop, var, xpression, ...) \
    __BITFIELDS_ASSIGN(var, xpression) cop \
    __BITFIELDS_INIT_DECL_53(cop, var, __VA_ARGS__)

#define __BITFIELDS_INIT_DECL_55(cop, var, xpression, ...) \
    __BITFIELDS_ASSIGN(var, xpression) cop \
    __BITFIELDS_INIT_DECL_54(cop, var, __VA_ARGS__)

#define __BITFIELDS_INIT_DECL_56(cop, var, xpression, ...) \
    __BITFIELDS_ASSIGN(var, xpression) cop \
    __BITFIELDS_INIT_DECL_55(cop, var, __VA_ARGS__)

#define __BITFIELDS_INIT_DECL_57(cop, var, xpression, ...) \
    __BITFIELDS_ASSIGN(var, xpression) cop \
    __BITFIELDS_INIT_DECL_56(cop, var, __VA_ARGS__)

#define __BITFIELDS_INIT_DECL_58(cop, var, xpression, ...) \
    __BITFIELDS_ASSIGN(var, xpression) cop \
    __BITFIELDS_INIT_DECL_57(cop, var, __VA_ARGS__)

#define __BITFIELDS_INIT_DECL_59(cop, var, xpression, ...) \
    __BITFIELDS_ASSIGN(var, xpression) cop \
    __BITFIELDS_INIT_DECL_58(cop, var, __VA_ARGS__)

#define __BITFIELDS_INIT_DECL_60(cop, var, xpression, ...) \
    __BITFIELDS_ASSIGN(var, xpression) cop \
    __BITFIELDS_INIT_DECL_59(cop, var, __VA_ARGS__)

#define __BITFIELDS_INIT_DECL_61(cop, var, xpression, ...) \
    __BITFIELDS_ASSIGN(var, xpression) cop \
    __BITFIELDS_INIT_DECL_60(cop, var, __VA_ARGS__)

#define __BITFIELDS_INIT_DECL_62(cop, var, xpression, ...) \
    __BITFIELDS_ASSIGN(var, xpression) cop \
    __BITFIELDS_INIT_DECL_61(cop, var, __VA_ARGS__)

#define __BITFIELDS_INIT_DECL_63(cop, var, xpression, ...) \
    __BITFIELDS_ASSIGN(var, xpression) cop \
    __BITFIELDS_INIT_DECL_62(cop, var, __VA_ARGS__)

#define __BITFIELDS_INIT_DECL_64(cop, var, xpression, ...) \
    __BITFIELDS_ASSIGN(var, xpression) cop \
    __BITFIELDS_INIT_DECL_63(cop, var, __VA_ARGS__)

// determine argument count and invoke variadic iterator
#define __BITFIELDS_INIT_DECL_NARG(...) __BITFIELDS_INIT_DECL_NARG_ \
    (__VA_ARGS__, __BITFIELDS_INIT_DECL_RSEQ_N())
#define __BITFIELDS_INIT_DECL_NARG_(...) __BITFIELDS_INIT_DECL_ARG_N(__VA_ARGS__)
#define __BITFIELDS_INIT_DECL_ARG_N(_1, _2, _3, _4, _5, _6, _7, _8, \
    _9, _10, _11, _12, _13, _14, _15, _16, _17, _18, _19, _20, _21, _22, _23, _24, \
    _25, _26, _27, _28, _29, _30, _31, _32, _33, _34, _35, _36, _37, _38, _39, _40, \
    _41, _42, _43, _44, _45, _46, _47, _48, _49, _50, _51, _52, _53, _54, _55, _56, \
    _57, _58, _59, _60, _61, _62, _63, _64, N, ...) N
#define __BITFIELDS_INIT_DECL_RSEQ_N() 64, 63, 62, 61, 60, 59, 58, 57, 56, \
    55, 54, 53, 52, 51, 50, 49, 48, 47, 46, 45, 44, 43, 42, 41, 40, \
    39, 38, 37, 36, 35, 34, 33, 32, 31, 30, 29, 28, 27, 26, 25, 24, \
    23, 22, 21, 20, 19, 18, 17, 16, 15, 14, 13, 12, 11, 10, 9, 8, 7, 6, 5, 4, 3, 2, 1, 0
#define __BITFIELDS_INIT_DECL_(N, cop, var, xpression, ...) \
    __BITFIELDS_CONCAT(__BITFIELDS_INIT_DECL_, N) (cop, var, xpression, __VA_ARGS__)

// unroll initializer arguments
#define __BITFIELDS_INIT_DECL(cop, var, xpression, ...) __BITFIELDS_INIT_DECL_ \
    (__BITFIELDS_INIT_DECL_NARG(xpression, __VA_ARGS__), cop, var, xpression, __VA_ARGS__)


//-------------------------------------------------------------------------------------------------
// bit-field initializers
//-------------------------------------------------------------------------------------------------
#define initializeBitfield(var, ...) \
    { \
        __BITFIELDS__PORT_MEMBARRIER; \
        __BITFIELDS_INIT_DECL (__BITFIELDS_CONCAT_SEMICOLON, \
            ::__bitfields::initializerCast(var.__isArrayStorageType (), var), __VA_ARGS__); \
        __BITFIELDS__PORT_MEMBARRIER; \
    }


//-------------------------------------------------------------------------------------------------
// Test conditions on a bit-field. Returns true if all expressions are true.
//-------------------------------------------------------------------------------------------------
#define testBitfieldAll(var, ...) \
    ( \
        (::__bitfields::MemoryBarrierReturnTrue () && \
        __BITFIELDS_INIT_DECL (__BITFIELDS_CONCAT_ANDAND, \
            ::__bitfields::combinedReaderCast(var.__isArrayStorageType (), var), __VA_ARGS__)) \
    )


//-------------------------------------------------------------------------------------------------
// Test conditions on a bit-field. Returns true if all expressions are true.
//-------------------------------------------------------------------------------------------------
#define testBitfield(var, ...) testBitfieldAll(var, __VA_ARGS__)


//-------------------------------------------------------------------------------------------------
// Test conditions on a bit-field. Returns true if any expression is true.
//-------------------------------------------------------------------------------------------------
#define testBitfieldAny(var, ...) \
    ( \
        (::__bitfields::MemoryBarrierReturnFalse () || \
        __BITFIELDS_INIT_DECL (__BITFIELDS_CONCAT_OROR, \
            ::__bitfields::combinedReaderCast(var.__isArrayStorageType (), var), __VA_ARGS__)) \
    )
