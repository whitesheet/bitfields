//=================================================================================================
//
//   Compiler independent safe bit-fields for C++98
//
//   File:          /include/bitfield/stubs.h
//
//   Description:   This file implements stub templates and classes for declaring bit-fields.
//
//   License:       Copyright (c) 2018 Julian Dessecker
//
//                  Permission is hereby granted, free of charge, to any person obtaining
//                  a copy of this software and associated documentation files
//                  (the "Software"), to deal in the Software without restriction,
//                  including without limitation the rights to use, copy, modify, merge,
//                  publish, distribute, sublicense, and/or sell copies of the Software,
//                  and to permit persons to whom the Software is furnished to do so,
//                  subject to the following conditions:
//
//                  The above copyright notice and this permission notice shall be
//                  included in all copies or substantial portions of the Software.
//
//                  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
//                  EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
//                  OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
//                  NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS
//                  BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN
//                  ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
//                  CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
//                  SOFTWARE.
//
//=================================================================================================


#pragma once


// check if we are included from top-level header
#ifndef __BITFIELDS__INCLUDE__GUARD__
#   error Do not include this file directly, inlude bitfield.h instead
#endif


#include "../util/typeinfo.h"


//-------------------------------------------------------------------------------------------------
// Endianess conversion type
//-------------------------------------------------------------------------------------------------
namespace __bitfields {
enum __EndianessConversion
{
    __kEC_Undefined = -1,
    __kEC_Native = 0,
    __kEC_BigEndian = 1,
    __kEC_LittleEndian = 2
};
} // namespace __bitfields


//-------------------------------------------------------------------------------------------------
// Bit-field attributes
//-------------------------------------------------------------------------------------------------
namespace __bitfields {
    struct RelaxedStorage {};               //< use relaxed memory access
    struct TakeOuterStorage {};             //< inherit storage type from outer nesting bit-field
    struct TakeOwnStorage {};               //< force to use own storage type
    struct PreventRead {};                  //< prevent read on register - if a read would be
                                            //    required simply use zero instead of reading the
                                            //    value
    struct __ArrayAddressConvFlag {};       //< type flag: this class implements an array address
                                            //   conversion
    struct __EndianessConvTypeFlag {};      //< endianess conversion flag
    template<int type>                      //< endianess conversion with type
    struct EndianessConversionType
    : public __EndianessConvTypeFlag
    {
        static const int endianessConversionType = type;
    };
} // namespace __bitfields


//-------------------------------------------------------------------------------------------------
// Check if type registered is bit-field
//-------------------------------------------------------------------------------------------------
namespace __bitfields {
    struct IsBitfieldTypeRequest {};
    struct IsBitfieldType {};
}

inline void* operator ->* (const void*, ::__bitfields::IsBitfieldTypeRequest&)
{
    return 0;
}


namespace __bitfields {


using namespace dstd::meta;


//-------------------------------------------------------------------------------------------------
// base class for own view implementations
//-------------------------------------------------------------------------------------------------
struct BasicViewImplementation {};


//-------------------------------------------------------------------------------------------------
// base class for view implementations
//-------------------------------------------------------------------------------------------------
struct ViewBase {};


//-------------------------------------------------------------------------------------------------
// Type Tag to indentify bit-field member wrappers
//-------------------------------------------------------------------------------------------------
struct ContainsBitfieldLayout {};


//-------------------------------------------------------------------------------------------------
// Compile time constant check, if a typename refers to a bit-field member declaration
//-------------------------------------------------------------------------------------------------
template <typename _T>
struct IsBitFieldMemberType
{
    typedef char yes[1];
    typedef char no[2];
    static yes& test (::__bitfields::IsBitfieldType);
    static no& test (...);
    typedef typename StripQualifiers <_T>::T ST;
    static const bool value = sizeof (test (operator ->* ((const ST*) 0x1234,
        *(::__bitfields::IsBitfieldTypeRequest*) 0x1234)
    )) == sizeof (yes);
};


//-------------------------------------------------------------------------------------------------
// Array reference type
//-------------------------------------------------------------------------------------------------
struct ArrayReferenceBase
{
    void* basePointer;
    int offsetBits;
};


//-------------------------------------------------------------------------------------------------
// Bit-field array address conversions
//-------------------------------------------------------------------------------------------------
struct DefaultArrayAddressConversion: public __ArrayAddressConvFlag
{
    static inline int convertArrayAddr (int inbound, int arraySize)
    {
        // reverse index ...
        if (arraySize < 0)
        {
            return (-arraySize - inbound - 1);
        }
        return inbound;
    }
};


//-------------------------------------------------------------------------------------------------
// empty nesting information (has no parent bit-field)
//-------------------------------------------------------------------------------------------------
struct NoNestingFlag
{
    static const bool storageIsConst = false;
    static const bool memberIsConst = false;
};


//-------------------------------------------------------------------------------------------------
// Compile-time assertion helpers
//-------------------------------------------------------------------------------------------------
template<bool succeeded> struct CheckAssertion {};
template<> struct CheckAssertion<true> {static const bool succeeded = true; struct Succeeded {};};


//-------------------------------------------------------------------------------------------------
// flags for aggregated initializers
//-------------------------------------------------------------------------------------------------
struct AggregatedInitFlag {};
struct AggregatedInitAndOpFlag: public AggregatedInitFlag {};
struct AggregatedInitOrOpFlag: public AggregatedInitFlag {};


} // namespace __bitfields
