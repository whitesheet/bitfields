//=================================================================================================
//
//   Compiler independent safe bit-fields for C++98
//
//   File:          /include/bitfield/declaration.h
//
//   Description:   This file implements the declaration mechanism for bit-fields. Basically,
//                  each bit-field declaration will be transformed into a union with wrapper
//                  for each member. The declaration mechanism collects all necessary layout
//                  information for the wrapper implementation and the storage backend. After
//                  collecting layout informations a wrapper will be instantiated for each
//                  member.
//
//   License:       Copyright (c) 2018 Julian Dessecker
//
//                  Permission is hereby granted, free of charge, to any person obtaining
//                  a copy of this software and associated documentation files
//                  (the "Software"), to deal in the Software without restriction,
//                  including without limitation the rights to use, copy, modify, merge,
//                  publish, distribute, sublicense, and/or sell copies of the Software,
//                  and to permit persons to whom the Software is furnished to do so,
//                  subject to the following conditions:
//
//                  The above copyright notice and this permission notice shall be
//                  included in all copies or substantial portions of the Software.
//
//                  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
//                  EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
//                  OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
//                  NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS
//                  BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN
//                  ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
//                  CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
//                  SOFTWARE.
//
//=================================================================================================


#pragma once


// check if we are included from top-level header
#ifndef __BITFIELDS__INCLUDE__GUARD__
#   error Do not include this file directly, inlude bitfield.h instead
#endif


#include "../util/typeinfo.h"
#include "wrapper.h"


namespace __bitfields {


using namespace dstd::meta;


//-------------------------------------------------------------------------------------------------
// Inject nesting info into type name (condition = false)
//-------------------------------------------------------------------------------------------------
template<typename memberType, typename nestingInfo, bool condition>
struct InjectNestingInfoIntoType
{
    typedef memberType MT;
};


//-------------------------------------------------------------------------------------------------
// Extract nesting offset from parent bit-field member declaration
//-------------------------------------------------------------------------------------------------
template<typename NT, bool isNested>
struct ExtractNestingOffset
{
    static const int nOffset = 0;
};

template<typename NT>
struct ExtractNestingOffset<NT, true>
{
    static const int nOffset = NT::offset;
};


//-------------------------------------------------------------------------------------------------
// parse optional bit-field directives
//-------------------------------------------------------------------------------------------------

// end of parameter list
struct _eop_ {};

// default attributes
struct DefaultAttributs
{
    static const bool takeOuter = false;
    static const bool takeOwn = false;
    static const bool relaxed = false;
    static const bool preventRead = false;
    static const int endianessConversionType = -1;
};

// default parsing rule, try to catch endianess conversion type
template <typename Parent, typename P1 = _eop_, typename P2 = _eop_, typename P3 = _eop_,
    typename P4 = _eop_, typename P5 = _eop_>
struct BitFieldAttributesInner
{
    // copy alignment attribute
    typedef typename BitFieldAttributesInner<DefaultAttributs, _eop_, P1, P2, P3, P4>::Attrs PT;
    struct Attrs
    {
        static const bool takeOuter = PT::takeOuter;
        static const bool takeOwn = PT::takeOwn;
        static const bool relaxed = PT::relaxed;
        static const bool preventRead = PT::preventRead;
        static const int endianessConversionType = P5::endianessConversionType;
    };
};

// shift right empty attribute list members
template <typename Parent, typename P1, typename P2, typename P3, typename P4>
struct BitFieldAttributesInner<Parent, P1, P2, P3, P4, _eop_>
{
    typedef typename BitFieldAttributesInner<DefaultAttributs, _eop_, P1, P2, P3, P4>::Attrs Attrs;
};

// terminate template recursion at end of variable attribute list
template <typename Parent>
struct BitFieldAttributesInner<Parent, _eop_, _eop_, _eop_, _eop_, _eop_>
{
    typedef Parent Attrs;
};

// catch PreventRead declaration
template <typename Parent, typename P1, typename P2, typename P3, typename P4>
struct BitFieldAttributesInner<Parent, P1, P2, P3, P4, ::__bitfields::PreventRead>
{
    typedef typename BitFieldAttributesInner<DefaultAttributs, _eop_, P1, P2, P3, P4>::Attrs PT;
    struct Attrs
    {
        static const bool takeOuter = PT::takeOuter;
        static const bool takeOwn = PT::takeOwn;
        static const bool relaxed = PT::relaxed;
        static const bool preventRead = true;
        static const int endianessConversionType = PT::endianessConversionType;
    };
};

// catch TakeOuterStorage declaration
template <typename Parent, typename P1, typename P2, typename P3, typename P4>
struct BitFieldAttributesInner<Parent, P1, P2, P3, P4, ::__bitfields::TakeOuterStorage>
{
    typedef typename BitFieldAttributesInner<DefaultAttributs, _eop_, P1, P2, P3, P4>::Attrs PT;
    struct Attrs
    {
        static const bool takeOuter = true;
        static const bool takeOwn = PT::takeOwn;
        static const bool relaxed = PT::relaxed;
        static const bool preventRead = PT::preventRead;
        static const int endianessConversionType = PT::endianessConversionType;
    };
};

// catch TakeOwnStorage declaration
template <typename Parent, typename P1, typename P2, typename P3, typename P4>
struct BitFieldAttributesInner<Parent, P1, P2, P3, P4, ::__bitfields::TakeOwnStorage>
{
    typedef typename BitFieldAttributesInner<DefaultAttributs, _eop_, P1, P2, P3, P4>::Attrs PT;
    struct Attrs
    {
        static const bool takeOuter = PT::takeOuter;
        static const bool takeOwn = true;
        static const bool relaxed = PT::relaxed;
        static const bool preventRead = PT::preventRead;
        static const int endianessConversionType = PT::endianessConversionType;
    };
};

// catch RelaxedStorage declaration
template <typename Parent, typename P1, typename P2, typename P3, typename P4>
struct BitFieldAttributesInner<Parent, P1, P2, P3, P4, ::__bitfields::RelaxedStorage>
{
    typedef typename BitFieldAttributesInner<DefaultAttributs, _eop_, P1, P2, P3, P4>::Attrs PT;
    struct Attrs
    {
        static const bool takeOuter = PT::takeOuter;
        static const bool takeOwn = PT::takeOwn;
        static const bool relaxed = true;
        static const bool preventRead = PT::preventRead;
        static const int endianessConversionType = PT::endianessConversionType;
    };
};

// top level template for parsing attributes
template <typename P1 = _eop_, typename P2 = _eop_, typename P3 = _eop_, typename P4 = _eop_
        , typename P5 = _eop_>
struct BitFieldAttributes
{
    typedef typename BitFieldAttributesInner<DefaultAttributs, P1, P2, P3, P4, P5>::Attrs Attrs;
};


//-------------------------------------------------------------------------------------------------
// compile time constant flag if we are inside an aggregated initializer
//-------------------------------------------------------------------------------------------------
template<int at, typename NestedParent, bool isNested>
struct IsAggregatedInitializerStore
{
    static const bool value = (at == 1);
};

template<int at, typename NestedParent>
struct IsAggregatedInitializerStore<at, NestedParent, true>
{
    static const bool value = ((at ? at : (NestedParent::aggregateType)) == 1);
};

template<int at, typename NestedParent, bool isNested>
struct IsAggregatedInitializerRead
{
    static const bool value = (at == 2);
};

template<int at, typename NestedParent>
struct IsAggregatedInitializerRead<at, NestedParent, true>
{
    static const bool value = ((at ? at : (NestedParent::aggregateType)) == 2);
};


//-------------------------------------------------------------------------------------------------
// strip volatile qualifier from storage type
//-------------------------------------------------------------------------------------------------
template<typename I, int stripVolatileCondition>
struct StripVolatileCond
{
    typedef typename StripVQualifier<I>::T T;
};

template<typename I>
struct StripVolatileCond<I, 0>
{
    typedef I T;
};


//-------------------------------------------------------------------------------------------------
// check if a parent type declaration is an array backend
//-------------------------------------------------------------------------------------------------
template<typename I, bool isDefined> struct GetNestedArrayBackendInner;

template<typename I>
struct GetNestedArrayBackend
{
    static const bool value =
        GetNestedArrayBackendInner<I, !IsBaseClassOf<NoNestingFlag,I>::value>::value;
};

template<typename I, bool isDefined>
struct GetNestedArrayBackendInner
{
    static const bool value = false;
};

template<typename I>
struct GetNestedArrayBackendInner<I, true>
{
    static const bool value = I::isArrayBackendLocal ||
        GetNestedArrayBackend<typename I::NestingParent>::value;
};


//-------------------------------------------------------------------------------------------------
// invoke non volatile parent cast
//-------------------------------------------------------------------------------------------------
template<typename _T, bool forceNonVolatile>
struct InvokeNonVolatileOnParent
{
    typedef _T T;
};

template<typename _T>
struct InvokeNonVolatileOnParent<_T, true>
{
    typedef typename _T::removeVolatileModifier T;
};


//-------------------------------------------------------------------------------------------------
// check if parent layout has relaxed storage flag (recursive)
//-------------------------------------------------------------------------------------------------
template<typename NestingType, bool isNested>
struct ParentLayoutHasRelaxedStorage
{
    static const bool val = false;
};

template<typename NestingType>
struct ParentLayoutHasRelaxedStorage<NestingType, true>
{
    static const bool val = NestingType::storageRelaxed;
};


//-------------------------------------------------------------------------------------------------
// check if parent layout has relaxed storage flag (recursive)
//-------------------------------------------------------------------------------------------------
template<typename NestingType, bool isNested>
struct ParentLayoutPreventsRead
{
    static const bool val = false;
};

template<typename NestingType>
struct ParentLayoutPreventsRead<NestingType, true>
{
    static const bool val = NestingType::preventRead;
};


//-------------------------------------------------------------------------------------------------
// select storage type (inner / outer)
//-------------------------------------------------------------------------------------------------
template<typename ST, typename NT, bool takeOuter>
struct SelectStorageType
{
    typedef typename NT::StorageType T;
};

template<typename ST, typename NT>
struct SelectStorageType<ST, NT, false>
{
    typedef ST T;
};


//-------------------------------------------------------------------------------------------------
// check if inner and outer types are different types
//-------------------------------------------------------------------------------------------------
template <typename ST, typename NT, bool isNestesd>
struct InnerOuterTypeDifferent
{
    // assert type is equal
    static const bool val = !(IsBaseClassOf<ST, typename NT::StorageTypeNoQual>::value &&
        IsBaseClassOf<typename NT::StorageType, ST>::value);
};

template <typename ST, typename NT>
struct InnerOuterTypeDifferent<ST, NT, false>
{
    static const bool val = false;
};


//-------------------------------------------------------------------------------------------------
// get parent endianess convertion type
//-------------------------------------------------------------------------------------------------
template<typename NestingParent, bool isNested>
struct GetParentEndianessConversionType
{
    static const int endianessConversionType = -1; // default
};

template<typename NestingParent>
struct GetParentEndianessConversionType<NestingParent, true>
{
    static const int endianessConversionType = NestingParent::endianessConversionType;
};


//-------------------------------------------------------------------------------------------------
// compile time constant bit-field member memory layout
//-------------------------------------------------------------------------------------------------
template<typename _Names, typename BE, typename ST, typename _Attributes, typename MT,
    int mOffset, int mBits, bool asConst, typename NT, typename VT, int bfBits, int aSize,
    typename _ArrayAddressConversion, int _aggregateType, bool __derefArray,
    bool mRelaxStorage, bool mPreventRead, int _endianessConversionType, bool isSecondPass = true>
struct BitFieldMemberLayout
{
    // declaration names
    typedef _Names Names;

    // storage backend
    typedef BE Backend;
    typedef _Attributes Attributes;
    typedef VT ViewType;

    // force storage to be non-volatile
    static const bool forceNonVolatile = _aggregateType != 0;

    // is array storage backend?
    static const bool isArrayBackendLocal = IsBaseClassOf <ArrayBackend, BE>::value;

    // simplified array?
    static const bool isSimplifiedArray = IsBaseClassOf <ArrayBackendSimplified, BE>::value;

    // access fits into a single storage cell?
    static const bool isSingleCellAccess = IsBaseClassOf <ArrayBackendSingleCell, BE>::value;

    // has array backend in nesting type (recursive) ?
    static const bool hasArrayBackend = GetNestedArrayBackend<NT>::value || isArrayBackendLocal;

    // strip qualifiers from memory layout
    typedef typename StripQualifiers<MT, true>::T SMT;

    // nesting informations
    static const bool isNested = ! IsBaseClassOf<NoNestingFlag, NT>::value;
    static const bool hostingNested = IsBitFieldMemberType<SMT>::value;
    static const int nestedOffset = ExtractNestingOffset<NT, isNested && (!__derefArray)>::nOffset;
    typedef typename InvokeNonVolatileOnParent<NT, isNested && forceNonVolatile>::T
        NestingParent;

    // need to take outer storage type?
    typedef typename SelectStorageType<ST, NT, isNested && Attributes::takeOuter>::T
        TempStorageType;

    // memory behind bit-field
    typedef typename StripVolatileCond<TempStorageType, forceNonVolatile>::T StorageType;
    typedef typename StripQualifiers<StorageType, true>::T StorageTypeNoQual;
    static const int storageElemetSize = sizeof (StorageTypeNoQual);
    static const bool storageIsConst = TypeInfo<StorageType>::isConst;
    static const bool storageIsVolatile = TypeInfo<StorageType>::isVolatile;
    static const bool storageRelaxed = Attributes::relaxed || mRelaxStorage
        || ParentLayoutHasRelaxedStorage<NT, isNested>::val;
    static const bool takeOwnStorage = Attributes::takeOwn;
    static const bool takeOuterStorage = Attributes::takeOuter;
    static const bool preventRead = Attributes::preventRead || mPreventRead
        || ParentLayoutPreventsRead<NT, isNested>::val;
    static const int bitfieldBits = bfBits;

    // conflicting inner/outer storage types?
    static const bool innerOuterTypeMismatch = InnerOuterTypeDifferent<StorageTypeNoQual, NT,
        isNested>::val && (!takeOwnStorage);

    // inherit endianess conversion flag
    static const int unionEndianessConversionType = Attributes::endianessConversionType;
    static const int parentEndianessConversionType =
            GetParentEndianessConversionType<NestingParent, isNested>::endianessConversionType;
    static const int endianessConversionType = (_endianessConversionType >= 0)
        ? _endianessConversionType : (takeOuterStorage
        ? (parentEndianessConversionType >= 0 ? parentEndianessConversionType :
            unionEndianessConversionType) : unionEndianessConversionType);

    // member type
    // inject own type as nesting parent into member layout if member type is a bit-field
    typedef BitFieldMemberLayout<Names, BE, ST, Attributes, MT, mOffset, mBits, asConst,
        NestingParent, VT, bfBits, aSize, _ArrayAddressConversion, _aggregateType,
        __derefArray, mRelaxStorage, mPreventRead, _endianessConversionType, false>
        OwnTypeFirstPass;
    typedef typename InjectNestingInfoIntoType<MT, OwnTypeFirstPass,
        IsBitFieldMemberType<SMT>::value>::MT MemberType;   //< with qualifiers
    typedef typename StripQualifiers<MemberType, true>::T MemberTypeNoQual;
    static const bool memberIsConst = TypeInfo<MT>::isConst;

    // with stripped volatile modifier
    typedef BitFieldMemberLayout<Names, BE, ST, Attributes, MT, mOffset, mBits, asConst,
        NestingParent, VT, bfBits, aSize, _ArrayAddressConversion, true, __derefArray,
        mRelaxStorage, mPreventRead, _endianessConversionType, false> removeVolatileModifier;

    // own layout type
    typedef BitFieldMemberLayout<Names, BE, ST, Attributes, MT, mOffset, mBits, asConst,
        NestingParent, VT, bfBits, aSize, _ArrayAddressConversion, _aggregateType, __derefArray
        , mRelaxStorage, mPreventRead, _endianessConversionType, true> __layout;

    // non-nested memory layout
    static const int localOffset = mOffset;

    // array layout
    static const bool isArray = aSize != 0 ? true : false;
    static const int arraySizeRaw = aSize;
    static const int arraySize = isArray ? (aSize < 0 ? -aSize : aSize) : 1;
    static const bool arrayIndexReversed = isArray && (aSize < 0);

    // array address conversion
    typedef _ArrayAddressConversion ArrayAddressConversion;

    // view conversion
    static const bool hasView = IsBaseClassOf<BasicViewImplementation, VT>::value;
    typedef typename VT::VT ViewT;

    // final memory layout (with nested offset, and view type conversion)
    typedef typename VT::RT Representation;
    typedef typename StripQualifiers<Representation, true>::T RepresentationNoQual;
    static const int bits = mBits;
    static const bool isConst =
        storageIsConst || memberIsConst || NT::storageIsConst || NT::memberIsConst || VT::isConst
        || asConst;

    // final bit offset, an array reference will break nested relations since it is referenced
    // already according to the nested parents offset
    static const int offset = ((__derefArray || isArrayBackendLocal) ? 0 : nestedOffset)
        + localOffset;

    // change storage backend to array, force const type if requested
    template<bool _asConst, bool isSingleCellAccess, bool simplified> struct AsArrayStorage
    {
        typedef typename MakeConstTypeCond<MemberType, _asConst>::T NMemberType;
        typedef BitFieldMemberLayout<Names, ArrayBackend, StorageType,  Attributes,
            NMemberType, 0, bits, _asConst, NestingParent, ViewType, bitfieldBits,
            arraySizeRaw, _ArrayAddressConversion, _aggregateType, true,
            mRelaxStorage, mPreventRead, _endianessConversionType> T;
    };

    // change storage backend to single cell array, force const type if requested
    template<bool _asConst> struct AsArrayStorage<_asConst,
        true, false>
    {
        typedef typename MakeConstTypeCond<MemberType, _asConst>::T NMemberType;
        typedef BitFieldMemberLayout<Names, ArrayBackendSingleCell, StorageType,  Attributes,
            NMemberType, 0, bits, _asConst, NestingParent, ViewType, bitfieldBits,
            arraySizeRaw, _ArrayAddressConversion, _aggregateType, true,
            mRelaxStorage, mPreventRead, _endianessConversionType> T;
    };

    // change storage backend to simple array, force const type if requested
    template<bool _asConst, bool isSingleCellAccess> struct AsArrayStorage<_asConst,
        isSingleCellAccess, true>
    {
        typedef typename MakeConstTypeCond<MemberType, _asConst>::T NMemberType;
        typedef BitFieldMemberLayout<Names, ArrayBackendSimplified, StorageType,  Attributes,
            NMemberType, 0, bits, _asConst, NestingParent, ViewType, bitfieldBits,
            arraySizeRaw, _ArrayAddressConversion, _aggregateType, true,
            mRelaxStorage, mPreventRead, _endianessConversionType> T;
    };

    // only when member type is a bit-field: replace storage backend
    template<typename backend> struct ReplaceMemberStorageBackend {
        typedef typename MT::template __WithStorageBackend<backend>::T T;
    };

    // publish aggregation type
    static const int aggregateType = _aggregateType;

    // is aggregated initializer?
    static const bool isAggregatedInitializer =
        IsAggregatedInitializerStore<_aggregateType, NT, isNested>::value;

    // is aggregated getter?
    static const bool isAggregatedGetter =
        IsAggregatedInitializerRead<_aggregateType, NT, isNested>::value;

    // dereference to inner array type (must be a bit-field member) and strip bit-offsets
    struct getInnerArrayTypeWithoutNesting
    {
        typedef BitFieldMemberLayout<_Names, Backend, StorageType, _Attributes, MemberType,
            0, mBits, asConst, NoNestingFlag, VT, mBits, 0, DefaultArrayAddressConversion,
            aggregateType, __derefArray, mRelaxStorage, mPreventRead, _endianessConversionType> T;
    };

    // for SFINAE tests: declare that we are a bit-field layout
    inline void __is_bitfield_layout_decl () {}
};


//-------------------------------------------------------------------------------------------------
// Conditionally inject nesting information into child type (if condition == true)
//-------------------------------------------------------------------------------------------------
template<typename memberType, typename nestingInfo>
struct InjectNestingInfoIntoType<memberType, nestingInfo, true>
{
    // strip away qualifiers from type
    static const bool wasConst = TypeInfo<memberType>::isConst;
    static const bool wasVolatile = TypeInfo<memberType>::isVolatile;
    typedef typename StripQualifiers<memberType, true>::T SMT;

    // create nested type
    typedef typename SMT::template __SetNesting<nestingInfo>::T N0;

    // restore previously removed qualifiers and export as member type
    typedef typename MakeConstTypeCond<N0, wasConst>::T N1;
    typedef typename MakeVolatileTypeCond<N1, wasVolatile>::T MT;
};


//-------------------------------------------------------------------------------------------------
// helper to determine union size in bits
//-------------------------------------------------------------------------------------------------
template<typename T, bool isPreflight>
struct GetUnionSize
{
    static const int size = 0;
};

template<typename T>
struct GetUnionSize<T, false>
{
    static const int size = sizeof (T);
};


//-------------------------------------------------------------------------------------------------
// Variable template argument extractor for optional parameters (member)
//-------------------------------------------------------------------------------------------------
// default recursion template, need to have a endianess conversion declaration
template<int _size, typename P1, typename P2, typename P3>
struct VarArgExtractorInner
{
    typedef VarArgExtractorInner<_size, _eop_, P1, P2> Parent;
    static const int size = _size;
    static const bool preventRead = Parent::preventRead;
    static const bool relaxStorage = Parent::relaxStorage;
    static const int endianessConversionType = P3::endianessConversionType;
};

// shift right side _eop_ tokens
template<int _size, typename P1, typename P2>
struct VarArgExtractorInner<_size, P1, P2, _eop_>
{
    typedef VarArgExtractorInner<_size, _eop_, P1, P2> Parent;
    static const int size = _size;
    static const bool preventRead = Parent::preventRead;
    static const bool relaxStorage = Parent::relaxStorage;
    static const int endianessConversionType = Parent::endianessConversionType;
};

// remove PreventRead flag
template<int _size, typename P1, typename P2>
struct VarArgExtractorInner<_size, P1, P2, PreventRead>
{
    typedef VarArgExtractorInner<_size, _eop_, P1, P2> Parent;
    static const int size = _size;
    static const bool preventRead = true;
    static const bool relaxStorage = Parent::relaxStorage;
    static const int endianessConversionType = Parent::endianessConversionType;
};

// remove PreventRead flag
template<int _size, typename P1, typename P2>
struct VarArgExtractorInner<_size, P1, P2, RelaxedStorage>
{
    typedef VarArgExtractorInner<_size, _eop_, P1, P2> Parent;
    static const int size = _size;
    static const bool preventRead = Parent::preventRead;
    static const bool relaxStorage = true;
    static const int endianessConversionType = Parent::endianessConversionType;
};

// abort recursion
template<int _size>
struct VarArgExtractorInner<_size, _eop_, _eop_, _eop_>
{
    static const int size = _size;
    static const bool preventRead = false;
    static const bool relaxStorage = false;
    static const int endianessConversionType = -1; // default
};

// variable argument entry point
template<int _size = -1, typename P1 = _eop_, typename P2 = _eop_, typename P3 = _eop_>
struct VarArgExtractor
{
    typedef VarArgExtractorInner<_size, P1, P2, P3> VT;
    static const int val = VT::size;
    static const bool preventRead = VT::preventRead;
    static const bool relaxStorage = VT::relaxStorage;
    static const int endianessConversionType = VT::endianessConversionType;
};


//-------------------------------------------------------------------------------------------------
// select array address trait from variable argument list
//-------------------------------------------------------------------------------------------------
template<int endianessConversionType, typename Param, bool conversionFlag>
struct SelectEndianessConversionType
{
    static const int val = Param::endianessConversionType;
};

template<int endianessConversionType, typename Param>
struct SelectEndianessConversionType<endianessConversionType, Param, false>
{
    static const int val = endianessConversionType;
};


//-------------------------------------------------------------------------------------------------
// select array index conversion trait from variable argument list
//-------------------------------------------------------------------------------------------------
template<typename _T, typename Param, bool conversionFlag>
struct SelectAddressConversion
{
    typedef Param T;
};

template<typename _T, typename Param>
struct SelectAddressConversion<_T, Param, false>
{
    typedef _T T;
};


//-------------------------------------------------------------------------------------------------
// Assert parameter is insane
//-------------------------------------------------------------------------------------------------
template<bool insane>
struct AssertParameterTypeIsKnown
{
};

template<>
struct AssertParameterTypeIsKnown<true>
{
    struct T {
        static const bool insane = true;
    };
};


//-------------------------------------------------------------------------------------------------
// Variable template argument extractor for optional parameters (member arrays)
//-------------------------------------------------------------------------------------------------
// default recursion template, need to have a endianess conversion or array address conversion
// declaration
template<int _size, typename P1, typename P2, typename P3, typename P4>
struct VarArgArrayExtractorInner
{
    typedef VarArgArrayExtractorInner<_size, _eop_, P1, P2, P3> Parent;
    static const int size = _size;
    static const bool preventRead = Parent::preventRead;
    static const bool relaxStorage = Parent::relaxStorage;

    // declaration type
    static const bool isEndianessConversion = IsBaseClassOf<__EndianessConvTypeFlag, P4>::value;
    static const bool isAddressConversion = IsBaseClassOf<__ArrayAddressConvFlag, P4>::value;

    // check type sanity
private:
    // assert parameter type is known
    static const bool __insane = isEndianessConversion || isAddressConversion;
    static const bool insane = AssertParameterTypeIsKnown<
            isEndianessConversion || isAddressConversion
    >::T::insane;

public:
    // select endian conversion type declaration
    static const int endianessConversionType = SelectEndianessConversionType
        <Parent::endianessConversionType, P4, isEndianessConversion && insane>::val;

    // select array address conversion declaration
    typedef typename SelectAddressConversion<typename Parent::AddressConversion, P4,
        isAddressConversion && insane>::T AddressConversion;

};

// shift right side _eop_ tokens
template<int _size, typename P1, typename P2, typename P3>
struct VarArgArrayExtractorInner<_size, P1, P2, P3, _eop_>
{
    typedef VarArgArrayExtractorInner<_size, _eop_, P1, P2, P3> Parent;
    static const int size = _size;
    static const bool preventRead = Parent::preventRead;
    static const bool relaxStorage = Parent::relaxStorage;
    static const int endianessConversionType = Parent::endianessConversionType;
    typedef typename Parent::AddressConversion AddressConversion;
};

// remove PreventRead flag
template<int _size, typename P1, typename P2, typename P3>
struct VarArgArrayExtractorInner<_size, P1, P2, P3, PreventRead>
{
    typedef VarArgArrayExtractorInner<_size, _eop_, P1, P2, P3> Parent;
    static const int size = _size;
    static const bool preventRead = true;
    static const bool relaxStorage = Parent::relaxStorage;
    static const int endianessConversionType = Parent::endianessConversionType;
    typedef typename Parent::AddressConversion AddressConversion;
};

// remove PreventRead flag
template<int _size, typename P1, typename P2, typename P3>
struct VarArgArrayExtractorInner<_size, P1, P2, P3, RelaxedStorage>
{
    typedef VarArgArrayExtractorInner<_size, _eop_, P1, P2, P3> Parent;
    static const int size = _size;
    static const bool preventRead = Parent::preventRead;
    static const bool relaxStorage = true;
    static const int endianessConversionType = Parent::endianessConversionType;
    typedef typename Parent::AddressConversion AddressConversion;
};

// abort recursion
template<int _size>
struct VarArgArrayExtractorInner<_size, _eop_, _eop_, _eop_, _eop_>
{
    static const int size = _size;
    static const bool preventRead = false;
    static const bool relaxStorage = false;
    static const int endianessConversionType = -1; // default
    typedef DefaultArrayAddressConversion AddressConversion;
};

// variable argument entry point
template<int _size = -1, typename P1 = _eop_, typename P2 = _eop_, typename P3 = _eop_,
    typename P4 = _eop_>
struct VarArgArrayExtractor
{
    typedef VarArgArrayExtractorInner<_size, P1, P2, P3, P4> VT;
    static const int val = VT::size;
    static const bool preventRead = VT::preventRead;
    static const bool relaxStorage = VT::relaxStorage;
    static const int endianessConversionType = VT::endianessConversionType;
    typedef typename VT::AddressConversion AddressConversion;
};


//-------------------------------------------------------------------------------------------------
// Select size is supplied via optional parameter or is derived from member type
//-------------------------------------------------------------------------------------------------
template<typename MT>
struct SelectDefaultSizeInnerType
{
    static const int size = sizeof (MT) * 8;
};

template<>
struct SelectDefaultSizeInnerType<bool>
{
    static const int size = 1;
};

template<typename MT, bool isNested>
struct SelectDefaultSizeInner
{
    static const int size = MT::__Sizes::storageBits;
};

template<typename MT>
struct SelectDefaultSizeInner<MT, false>
{
    static const int size = SelectDefaultSizeInnerType<MT>::size;
};

template<typename MT, int _size = -1>
struct SelectDefaultSize
{
    typedef typename StripQualifiers<MT, true>::T SMT;
    static const int size = _size < 0 ? SelectDefaultSizeInner<SMT,
        IsBitFieldMemberType<MT>::value>::size : _size;
};


//-------------------------------------------------------------------------------------------------
// Determine size of inner declaration in bits
//-------------------------------------------------------------------------------------------------
template<typename Inner, int _storageSize, bool isNested>
struct GetInnerSizeHelper
{
    static const int size = _storageSize;
};

template<typename Inner, int _storageSize>
struct GetInnerSizeHelper<Inner, _storageSize, true>
{
    static const int size = Inner::__Sizes::storageBits;
};

template<typename Inner, int _storageSize>
struct GetInnerSize
{
    static const int size = GetInnerSizeHelper<Inner, _storageSize,
        IsBitFieldMemberType<Inner>::value>::size;
};


//-------------------------------------------------------------------------------------------------
// Select implementation kind, should we nest a union, implement an array
// or implement a leaf structure?
//-------------------------------------------------------------------------------------------------
// reserve storage area for bit-field member (for pre-flight)
template<int bits>
struct MemberImplementationSelectorStorage
{
    char __storage[bits];
};

// default: wrap to bit-field leaf
template<typename _layout, bool isArray, bool isPreflight, bool isNestedType>
struct MemberImplementationSelectorInner
{
    typedef typename MemberWrapperImplementationFactory<_layout>::T T;
};

// pre-flight: determine size of non-nested member
template<typename _layout, bool isArray>
struct MemberImplementationSelectorInner<_layout, isArray, true, false>
{
    typedef MemberImplementationSelectorStorage<_layout::localOffset +
        (_layout::bits * _layout::arraySize)> T;
};

// pre-flight: determine size of nested member
template<typename _layout, bool isArray>
struct MemberImplementationSelectorInner<_layout, isArray, true, true>
{
    typedef typename _layout::MemberType MT;
    typedef MemberImplementationSelectorStorage<_layout::localOffset +
        (MT::__Sizes::storageBits * _layout::arraySize)> T;
};

template<typename I, bool stripVolatile>
struct ConditionallyRemoveVolatileStorageFromBitfield
{
    // apply non volatile qualifier
    typedef typename I::__ForceNonVolatile::T T;
};

template<typename I>
struct ConditionallyRemoveVolatileStorageFromBitfield<I, false>
{
    // return type as it is
    typedef I T;
};

// implementation: wrap to nested bit-field
template<typename _layout>
struct MemberImplementationSelectorInner<_layout, false, false, true>
{
    typedef typename ConditionallyRemoveVolatileStorageFromBitfield
        <typename _layout::MemberType, _layout::forceNonVolatile>::T T;
};

// implementation: wrap to array type (non-nested type)
template<typename _layout>
struct MemberImplementationSelectorInner<_layout, true, false, false>
{
    typedef typename SimpleArrayWrapperImplementationFactory<_layout>::T T;
};

// implementation: wrap to array type (nested type)
template<typename _layout>
struct MemberImplementationSelectorInner<_layout, true, false, true>
{
    typedef typename ComplexArrayWrapperImplementationFactory<_layout>::T T;
};

// top level selector
template<typename _layout, bool isArray, bool isPreflight>
struct MemberImplementationSelector
{
    typedef typename MemberImplementationSelectorInner
        <_layout, isArray, isPreflight, _layout::hostingNested>::T T;
};


//-------------------------------------------------------------------------------------------------
// need to reserve storage space for array reference?
//-------------------------------------------------------------------------------------------------
template<bool isArray> struct SelectArrayReferenceStorageSpace
{
};

template<> struct SelectArrayReferenceStorageSpace<true>
{
    ArrayReferenceBase __arrayRef;
};


//-------------------------------------------------------------------------------------------------
// check if it's an array reference type
//-------------------------------------------------------------------------------------------------
template<typename T> struct IsArrayStorageBackend
{
    static const bool val = IsBaseClassOf <ArrayBackend, T>::value;
};


//-------------------------------------------------------------------------------------------------
// Helper to implement data storage elements when not declared as array reference
//-------------------------------------------------------------------------------------------------
template <typename storageType, int elements, bool implement>
struct ImplementBitfieldStorage
{
    storageType __rawData [elements];
};

template <typename storageType, int elements>
struct ImplementBitfieldStorage<storageType, elements, false>
{
};


//-------------------------------------------------------------------------------------------------
// Get inner member type from view definition
//-------------------------------------------------------------------------------------------------

// non view type
template<typename _T, bool _isView>
struct ViewTypeSelectorInner
{
    struct T
    {
        typedef _T MT;
        typedef _T RT;
        struct VT
        {
            static inline _T get (const _T& t) {return t;}
            static inline _T set (const _T& t) {return t;}
        };
        static const bool isView = false;
        static const bool isConst = TypeInfo<_T>::isConst;
    };
};

// specialize for view types
template<typename _T>
struct ViewTypeSelectorInner<_T, true>
{
    struct T
    {
        typedef typename _T::MemberType MT;
        typedef typename _T::RepresentationType RT;
        typedef typename StripQualifiers<_T, true>::T VT;
        static const bool isView = true;
        static const bool isConst = TypeInfo<_T>::isConst;
    };
};

// top level template to get view type
template<typename _T>
struct ViewTypeSelector
{
    static const bool isView = IsBaseClassOf<BasicViewImplementation,
        typename StripQualifiers<_T, true>::T>::value;
    typedef typename ViewTypeSelectorInner<_T, isView>::T::MT MT;
    typedef typename ViewTypeSelectorInner<_T, isView>::T::RT RT;
    typedef typename ViewTypeSelectorInner<_T, isView>::T T;
};


//-------------------------------------------------------------------------------------------------
// Declaration name information
//-------------------------------------------------------------------------------------------------
struct DeclarationName
{
    static const char* get () {return 0;}
};


//-------------------------------------------------------------------------------------------------
// Bit-field size constraints
//-------------------------------------------------------------------------------------------------
template<unsigned _bitsPerCell, unsigned _storageBits, int _nonVolatileCondition>
struct Sizes
{
    static const unsigned bitsPerCell = _bitsPerCell;
    static const unsigned storageBits = _storageBits;
    static const unsigned storageCells = (storageBits + bitsPerCell - 1) / bitsPerCell;
    static const bool nonVolatile = _nonVolatileCondition;
};


//-------------------------------------------------------------------------------------------------
// Returns an mapped return type for compile time splitting of references or array nodes
//-------------------------------------------------------------------------------------------------
template<bool isArray> struct ArrayDetectionReturnType
{
    typedef char* T;
};


template<> struct ArrayDetectionReturnType<true>
{
    typedef void* T;
};


} // namespace __bitfields


//-------------------------------------------------------------------------------------------------
// convenience declaration types
//-------------------------------------------------------------------------------------------------
#define __bitfields_decl_types \
        typedef ::__bitfields::RelaxedStorage RelaxedStorage; \
        typedef ::__bitfields::TakeOuterStorage TakeOuterStorage; \
        typedef ::__bitfields::TakeOwnStorage TakeOwnStorage; \
        typedef ::__bitfields::PreventRead PreventRead; \
        typedef ::__bitfields::EndianessConversionType<::__bitfields::__kEC_Native> \
            NativeEndian; \
        typedef ::__bitfields::EndianessConversionType<::__bitfields::__kEC_BigEndian> \
            BigEndian; \
        typedef ::__bitfields::EndianessConversionType<::__bitfields::__kEC_LittleEndian> \
            LittleEndian; \
        typedef BigEndian NetworkByteOrder;


//-------------------------------------------------------------------------------------------------
// expand macro token as string for testing
//-------------------------------------------------------------------------------------------------
#define __bitfields_stringify_second(s) #s
#define __bitfields_stringify(s) __bitfields_stringify_second(s)


//-------------------------------------------------------------------------------------------------
// Declaration name inlining (root bitfield type)
//-------------------------------------------------------------------------------------------------
#define __bitfields_decl_name(s, t) \
    struct t {static const char* get () {return __bitfields_stringify(s);}};


//-------------------------------------------------------------------------------------------------
// Declaration name inlining (members)
//-------------------------------------------------------------------------------------------------
#define __bitfields_member_decl_name(s, t) \
    struct t { \
        static const char* getOuter () {return __declName::get ();} \
        static const char* getInner () {return __bitfields_stringify(s);} \
    };


//-------------------------------------------------------------------------------------------------
// begin declaring a bit-field
//-------------------------------------------------------------------------------------------------
#define BITFIELD_DECLARE(__typeName, __storageType, ...) \
    template<typename __nestingInfo, typename __backend, int __aggregateType, bool __derefArray, \
    bool __preflight> union __bitfields_impl_##__typeName; \
    inline ::__bitfields::IsBitfieldType operator ->* \
        (const union __bitfields_impl_##__typeName< ::__bitfields::NoNestingFlag, \
                ::__bitfields::DefaultBackend, false, false, false>*, \
        ::__bitfields::IsBitfieldTypeRequest&) { return ::__bitfields::IsBitfieldType (); } \
    typedef __bitfields_impl_##__typeName< ::__bitfields::NoNestingFlag, \
        ::__bitfields::DefaultBackend, 0, false, false> __typeName;\
    template<typename __nestingInfo, typename __backend, int __aggregateType, bool __derefArray, \
    bool __preflight> union __bitfields_impl_##__typeName { \
        __bitfields_decl_types \
        template<typename BE> struct __WithStorageBackend \
            { typedef __bitfields_impl_##__typeName<__nestingInfo, BE, \
                __aggregateType, __derefArray, __preflight> T; }; \
        typedef typename ::__bitfields::BitFieldAttributes<__VA_ARGS__>::Attrs __Attributes; \
        typedef __nestingInfo __NestingInfo; \
        typedef __backend __Backend; \
        typedef __typeName __TypeName; \
        template<typename NT> struct __SetNesting \
        {typedef __bitfields_impl_##__typeName<NT, __Backend, __aggregateType, __derefArray, \
            false> T;}; \
        struct __DerefArray {typedef __bitfields_impl_##__typeName<__NestingInfo, __Backend, \
            __aggregateType, true, false> T;}; \
        template<typename B> struct __SetBackend \
        {typedef __bitfields_impl_##__typeName<__NestingInfo, B, __aggregateType, __derefArray, \
            false> T;}; \
        template<int aggregateType> struct __ForceNonVolatile \
        {typedef __bitfields_impl_##__typeName<__NestingInfo, __Backend, \
            aggregateType, __derefArray, false> T;}; \
        typedef typename ::__bitfields::StripVolatileCond<__storageType, __aggregateType>::T \
            __StorageType; \
        typedef __NestingInfo __layout; \
        typedef ::__bitfields::Sizes<8 * sizeof (__storageType), \
                ::__bitfields::GetUnionSize <__bitfields_impl_##__typeName< \
                ::__bitfields::NoNestingFlag, ::__bitfields::DefaultBackend, __aggregateType, \
                __derefArray, true>, __preflight>::size, __aggregateType> __Sizes; \
        ::__bitfields::ImplementBitfieldStorage <__storageType ,__Sizes::storageCells, \
        (! __bitfields::IsArrayStorageBackend <__backend>::val) && (!__preflight)> __rawData; \
        ::__bitfields::SelectArrayReferenceStorageSpace< ::__bitfields::IsArrayStorageBackend \
        <__backend>::val> __storageArrayRef; \
        __bitfields_decl_name (__typename, __declName) \
        typename ::__bitfields::ArrayDetectionReturnType< ::__bitfields::IsArrayStorageBackend \
        <__backend>::val>::T __isArrayStorageType () const {return 0;}


//-------------------------------------------------------------------------------------------------
// add member to bit field
//-------------------------------------------------------------------------------------------------
#define BITFIELD_MEMBER(__typeName, __memberName, __offset, ...) \
    __bitfields_member_decl_name(__memberName, __declNames##__memberName) \
    typename ::__bitfields::MemberImplementationSelector< \
        ::__bitfields::BitFieldMemberLayout<__declNames##__memberName, \
        __Backend, __StorageType, __Attributes, \
        typename ::__bitfields::ViewTypeSelector<__typeName>::MT, \
        __offset, ::__bitfields::SelectDefaultSize<typename ::__bitfields::ViewTypeSelector \
        <__typeName>::MT, ::__bitfields::VarArgExtractor<__VA_ARGS__>::val>::size, \
        false, __NestingInfo, typename ::__bitfields::ViewTypeSelector<__typeName>::T, \
        ::__bitfields::GetInnerSize<__typeName, __Sizes::storageBits>::size, 0, \
        ::__bitfields::DefaultArrayAddressConversion, __aggregateType, __derefArray, \
        ::__bitfields::VarArgExtractor<__VA_ARGS__>::relaxStorage, \
        ::__bitfields::VarArgExtractor<__VA_ARGS__>::preventRead, \
        ::__bitfields::VarArgExtractor<__VA_ARGS__>::endianessConversionType> \
    , false, __preflight>::T __memberName;


//-------------------------------------------------------------------------------------------------
// add array member to bit field
//-------------------------------------------------------------------------------------------------
#define BITFIELD_MEMBER_ARRAY(__typeName, __memberName, __offset, __arraySize, ...) \
    __bitfields_member_decl_name(__memberName, __declNames##__memberName) \
    typename ::__bitfields::MemberImplementationSelector< \
         ::__bitfields::BitFieldMemberLayout<__declNames##__memberName, \
          __Backend, __StorageType, __Attributes, \
         typename ::__bitfields::ViewTypeSelector<__typeName>::MT, \
         __offset, ::__bitfields::SelectDefaultSize<typename ::__bitfields::ViewTypeSelector \
         <__typeName>::MT, ::__bitfields::VarArgArrayExtractor<__VA_ARGS__>::val>::size, \
          false, __NestingInfo, typename ::__bitfields::ViewTypeSelector<__typeName>::T, \
         ::__bitfields::GetInnerSize<__typeName, __Sizes::storageBits>::size, __arraySize, \
         typename ::__bitfields::VarArgArrayExtractor<__VA_ARGS__>::AddressConversion, \
         __aggregateType, __derefArray, ::__bitfields::VarArgArrayExtractor<__VA_ARGS__> \
         ::relaxStorage, ::__bitfields::VarArgArrayExtractor<__VA_ARGS__>::preventRead, \
            ::__bitfields::VarArgArrayExtractor<__VA_ARGS__>::endianessConversionType> \
    , true, __preflight>::T __memberName;


//-------------------------------------------------------------------------------------------------
// declare single instance of peripheral
//-------------------------------------------------------------------------------------------------
#define BITFIELD_INSTANCE(baseAddr) \
    inline static __TypeName& instance () \
    { \
        return *reinterpret_cast<__TypeName*> (baseAddr); \
    }


//-------------------------------------------------------------------------------------------------
// end declaring a bit-field
//-------------------------------------------------------------------------------------------------
#define BITFIELD_END \
    };
