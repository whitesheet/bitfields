//=================================================================================================
//
//   Compiler independent safe bit-fields for C++98
//
//   File:          /include/bitfield/tail.h
//
//   Description:   This file implements the tail declarations (implementing a type to layout map)
//
//   License:       Copyright (c) 2018 Julian Dessecker
//
//                  Permission is hereby granted, free of charge, to any person obtaining
//                  a copy of this software and associated documentation files
//                  (the "Software"), to deal in the Software without restriction,
//                  including without limitation the rights to use, copy, modify, merge,
//                  publish, distribute, sublicense, and/or sell copies of the Software,
//                  and to permit persons to whom the Software is furnished to do so,
//                  subject to the following conditions:
//
//                  The above copyright notice and this permission notice shall be
//                  included in all copies or substantial portions of the Software.
//
//                  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
//                  EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
//                  OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
//                  NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS
//                  BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN
//                  ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
//                  CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
//                  SOFTWARE.
//
//=================================================================================================


#pragma once


// check if we are included from top-level header
#ifndef __BITFIELDS__INCLUDE__GUARD__
#   error Do not include this file directly, inlude bitfield.h instead
#endif


namespace __bitfields {


//-------------------------------------------------------------------------------------------------
// get layout if we have an underlying bit-field member (true condition)
//-------------------------------------------------------------------------------------------------
#ifdef __BITFIELDS_TEST_MODE
template<typename T>
struct SelectTestLayoutGetter<T, true>
{
    static inline ::__bitfields::test::MockedLayout* getLayout ()
    {
        return ::__bitfields::test::MockedLayout::create<typename T::__layout> ();
    }
};
#endif


} // namespace __bitfields
