//=================================================================================================
//
//   Compiler independent safe bit-fields for C++98
//
//   File:          /include/bitfield/wrapper.h
//
//   Description:   This file implements wrappers for bit-fields, members of bit-fields and
//                  arrays inside bit-fields. For each declared bit-field member the declaration
//                  mechanism instantiates a wrapper for variable style member access. Wrappers
//                  implement cast, assignment and type specific operators by invoking the storage
//                  backend in a generalized way.
//
//
//   License:       Copyright (c) 2018 Julian Dessecker
//
//                  Permission is hereby granted, free of charge, to any person obtaining
//                  a copy of this software and associated documentation files
//                  (the "Software"), to deal in the Software without restriction,
//                  including without limitation the rights to use, copy, modify, merge,
//                  publish, distribute, sublicense, and/or sell copies of the Software,
//                  and to permit persons to whom the Software is furnished to do so,
//                  subject to the following conditions:
//
//                  The above copyright notice and this permission notice shall be
//                  included in all copies or substantial portions of the Software.
//
//                  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
//                  EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
//                  OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
//                  NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS
//                  BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN
//                  ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
//                  CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
//                  SOFTWARE.
//
//=================================================================================================


#pragma once


// check if we are included from top-level header
#ifndef __BITFIELDS__INCLUDE__GUARD__
#   error Do not include this file directly, inlude bitfield.h instead
#endif


#include "../util/typeinfo.h"
#include "../util/unalignedaccess.h"
#include "backend.h"
#include "testadapter.h"
#include "assertions.h"


namespace __bitfields {


using namespace dstd::meta;


//-------------------------------------------------------------------------------------------------
// flag: use default storage backend, use when backend chaining is not required, i.e. on nodes
// not containing an array declaration parent.
//-------------------------------------------------------------------------------------------------
struct DefaultBackend {};


//-------------------------------------------------------------------------------------------------
// flag: use array storage backend, chaining will be done in members operator []
//-------------------------------------------------------------------------------------------------
struct ArrayBackend {};


//-------------------------------------------------------------------------------------------------
// flag: use array storage backend, chaining will be done in members operator []
// the encapsulated member is guaranteed to fit into a single storage cell
//-------------------------------------------------------------------------------------------------
struct ArrayBackendSingleCell: public ArrayBackend {};


//-------------------------------------------------------------------------------------------------
// flag: use array storage backend, chaining will be done in members operator [].
// note: using this array notation, the backend gets informed that the member will fit into the
//    single storage cell pointed by ArrayRef, the bit index will be in range
//    [0 .. sizeof (StorageType) * 8 - 1]
//-------------------------------------------------------------------------------------------------
struct ArrayBackendSimplified: public ArrayBackend {};


//-------------------------------------------------------------------------------------------------
// Operator implementation detail selector
//-------------------------------------------------------------------------------------------------
struct WrapperOperatorSelector
{
    static const int bad_implementation = 1;
    static const int constant = 2;
    static const int array = 3;
    static const int boolean = 4;
    static const int integral = 5;
    static const int number = 6;
    static const int aggregatedInitializer = 7;
    static const int getSet = 8;
};


//-------------------------------------------------------------------------------------------------
// SFINAE is array check: generate return type for meta function
//-------------------------------------------------------------------------------------------------
template<bool condition>
struct ArraySfinaeReturnType
{
    typedef char* T;
};

template<>
struct ArraySfinaeReturnType<true>
{
    typedef void* T;
};


//-------------------------------------------------------------------------------------------------
// Bad member wrapper (used to generate compile time errors when type can not be deduced)
//-------------------------------------------------------------------------------------------------
template<typename payload, typename layout, int operators>
struct WrapperImplementation: private payload, public ContainsBitfieldLayout
{
    // publish layout information
    typedef layout __layout;

    // is this node storage on an array backend?
    typename ArraySfinaeReturnType<layout::hasArrayBackend>::T __isArrayStorageType () const
    {
        return 0;
    }
};


//-------------------------------------------------------------------------------------------------
// Constant member wrapper (only cast operations)
//-------------------------------------------------------------------------------------------------
template<typename payload, typename layout>
struct WrapperImplementation<payload, layout, WrapperOperatorSelector::constant>:
    private payload, public ContainsBitfieldLayout
{
    // publish layout information
    typedef layout __layout;

    // fetch data from storage backend or view overlay
    typename layout::RepresentationNoQual __get () const
    {
        // get storage backend type
        typedef typename StorageSelectorWithTests<layout>::T BE;

        // get view type
        typedef typename layout::ViewT VT;

        // get item via view implementation
        return VT::get (reinterpret_cast<const BE*> (this)->__get ());
    }

    // cast operator
    operator typename layout::RepresentationNoQual () const
    {
        return __get ();
    }

    // is this node storage on an array backend?
    typename ArraySfinaeReturnType<layout::hasArrayBackend>::T __isArrayStorageType () const
    {
        return 0;
    }
};


//-------------------------------------------------------------------------------------------------
// Member wrapper (only get & set operations)
//-------------------------------------------------------------------------------------------------
template<typename payload, typename layout>
struct WrapperImplementation<payload, layout, WrapperOperatorSelector::getSet>:
    private payload, public ContainsBitfieldLayout
{
    // publish layout information
    typedef layout __layout;

    // declare own type
    typedef WrapperImplementation<payload, layout, WrapperOperatorSelector::getSet> __SelfT;

    // fetch data from storage backend or view overlay
    typename layout::RepresentationNoQual __get () const
    {
        // get storage backend type
        typedef typename StorageSelectorWithTests<layout>::T BE;

        // get view type
        typedef typename layout::ViewT VT;

        // get item via view implementation
        return VT::get (reinterpret_cast<const BE*> (this)->__get ());
    }

    // cast operator
    operator typename layout::RepresentationNoQual () const
    {
        return __get ();
    }

    // write data to storage backened
    void __set (const typename layout::RepresentationNoQual& v)
    {
        // get storage backend type and invoke on this pointer
        typedef typename StorageSelectorWithTests<layout>::T BE;

        // get view type
        typedef typename layout::ViewT VT;

        // set via view
        reinterpret_cast<BE*> (this)->__set (VT::set (v));
    }

    // assignment operator
    __SelfT& operator = (const typename layout::RepresentationNoQual& v)
    {
        __set (v);
        return *this;
    }

    // is this node storage on an array backend?
    typename ArraySfinaeReturnType<layout::hasArrayBackend>::T __isArrayStorageType () const
    {
        return 0;
    }
};


//-------------------------------------------------------------------------------------------------
// Boolean member wrapper (cast operations, boolean related operators)
//-------------------------------------------------------------------------------------------------
template<typename payload, typename layout>
struct WrapperImplementation<payload, layout, WrapperOperatorSelector::boolean>:
    private payload, public ContainsBitfieldLayout
{
    // publish layout information
    typedef layout __layout;

    // own type
    typedef WrapperImplementation<payload, layout, WrapperOperatorSelector::boolean> __SelfT;

    // fetch data from storage backend or view overlay
    typename layout::RepresentationNoQual __get () const
    {
        // get storage backend type
        typedef typename StorageSelectorWithTests<layout>::T BE;

        // get view type
        typedef typename layout::ViewT VT;

        // get item via view implementation
        return VT::get (reinterpret_cast<const BE*> (this)->__get ());
    }

    // write data to storage backened
    void __set (const typename layout::RepresentationNoQual& v)
    {
        // get storage backend type and invoke on this pointer
        typedef typename StorageSelectorWithTests<layout>::T BE;

        // get view type
        typedef typename layout::ViewT VT;

        // set via view
        reinterpret_cast<BE*> (this)->__set (VT::set (v));
    }

    // assignment operator
    __SelfT& operator = (const typename layout::RepresentationNoQual& v)
    {
        __set (v);
        return *this;
    }

    // cast operator
    operator typename layout::RepresentationNoQual () const
    {
        return __get ();
    }

    // other boolean operators
    __SelfT& operator |= (bool v)
    {
        __set (__get () | v);
        return *this;
    }
    __SelfT& operator &= (bool v)
    {
        __set (__get () & v);
        return *this;
    }
    __SelfT& operator ^= (bool v)
    {
        __set (__get () ^ v);
        return *this;
    }

    // is this node storage on an array backend?
    typename ArraySfinaeReturnType<layout::hasArrayBackend>::T __isArrayStorageType () const
    {
        return 0;
    }
};


//-------------------------------------------------------------------------------------------------
// Setter for aggregated initializers
//-------------------------------------------------------------------------------------------------
template<typename payload, typename layout>
struct WrapperImplementation<payload, layout, WrapperOperatorSelector::aggregatedInitializer>:
    private payload, public ContainsBitfieldLayout
{
    // publish layout information
    typedef layout __layout;

    // own type
    typedef WrapperImplementation<payload, layout,
        WrapperOperatorSelector::aggregatedInitializer> __SelfT;

    // write data to storage backened
    void __set (const typename layout::RepresentationNoQual& v)
    {
        // get storage backend type and invoke on this pointer
        typedef typename StorageSelectorWithTests<layout>::T BE;

        // get view type
        typedef typename layout::ViewT VT;

        // set via view
        reinterpret_cast<BE*> (this)->__set (VT::set (v));
    }

    // assignment operator
    __SelfT& operator = (const typename layout::RepresentationNoQual& v)
    {
        __set (v);
        return *this;
    }

    // is this node storage on an array backend?
    typename ArraySfinaeReturnType<layout::hasArrayBackend>::T __isArrayStorageType () const
    {
        return 0;
    }
};


//-------------------------------------------------------------------------------------------------
// Integral number member wrapper (cast operations, integral number related operators)
//-------------------------------------------------------------------------------------------------
template<typename payload, typename layout>
struct WrapperImplementation<payload, layout, WrapperOperatorSelector::integral>:
    private payload, public ContainsBitfieldLayout
{
    // publish layout information
    typedef layout __layout;

    // own type
    typedef WrapperImplementation<payload, layout, WrapperOperatorSelector::integral> __SelfT;

    // fetch data from storage backend or view overlay
    typename layout::RepresentationNoQual __get () const
    {
        // get storage backend type
        typedef typename StorageSelectorWithTests<layout>::T BE;

        // get view type
        typedef typename layout::ViewT VT;

        // get item via view implementation
        return VT::get (reinterpret_cast<const BE*> (this)->__get ());
    }

    // write data to storage backened
    void __set (const typename layout::RepresentationNoQual& v)
    {
        // get storage backend type and invoke on this pointer
        typedef typename StorageSelectorWithTests<layout>::T BE;

        // get view type
        typedef typename layout::ViewT VT;

        // set via view
        reinterpret_cast<BE*> (this)->__set (VT::set (v));
    }

    // assignment operator
    __SelfT& operator = (const typename layout::RepresentationNoQual& v)
    {
        __set (v);
        return *this;
    }

    // cast operator
    operator typename layout::RepresentationNoQual () const
    {
        return __get ();
    }

    // number operators
    __SelfT& operator++ ()
    {
        __set (__get () + 1);
        return *this;
    }

    typename layout::RepresentationNoQual operator++ (int)
    {
        typename layout::RepresentationNoQual t = __get ();
        __set (t + 1);
        return t;
    }

    __SelfT& operator-- ()
    {
        __set (__get () - 1);
        return *this;
    }

    typename layout::RepresentationNoQual operator-- (int)
    {
        typename layout::RepresentationNoQual t = __get ();
        __set (t - 1);
        return t;
    }

    template<typename O> __SelfT& operator += (const O& o)
    {
        __set (__get () + o);
        return *this;
    }

    template<typename O> __SelfT& operator -= (const O& o)
    {
        __set (__get () - o);
        return *this;
    }

    template<typename O> __SelfT& operator *= (const O& o)
    {
        __set (__get () * o);
        return *this;
    }

    template<typename O> __SelfT& operator /= (const O& o)
    {
        __set (__get () / o);
        return *this;
    }

    template<typename O> __SelfT& operator %= (const O& o)
    {
        __set (__get () % o);
        return *this;
    }

    // integral operators

    template<typename O> __SelfT& operator &= (const O& o)
    {
        __set (__get () & o);
        return *this;
    }

    template<typename O> __SelfT& operator |= (const O& o)
    {
        __set (__get () | o);
        return *this;
    }

    template<typename O> __SelfT& operator ^= (const O& o)
    {
        __set (__get () ^ o);
        return *this;
    }

    template<typename O> __SelfT& operator <<= (const O& o)
    {
        __set (__get () << o);
        return *this;
    }

    template<typename O> __SelfT& operator >>= (const O& o)
    {
        __set (__get () >> o);
        return *this;
    }

    // is this node storage on an array backend?
    typename ArraySfinaeReturnType<layout::hasArrayBackend>::T __isArrayStorageType () const
    {
        return 0;
    }
};


//-------------------------------------------------------------------------------------------------
// Number member wrapper (cast operations, number related operators)
//-------------------------------------------------------------------------------------------------
template<typename payload, typename layout>
struct WrapperImplementation<payload, layout, WrapperOperatorSelector::number>:
    private payload, public ContainsBitfieldLayout
{
    // publish layout information
    typedef layout __layout;

    // own type
    typedef WrapperImplementation<payload, layout, WrapperOperatorSelector::number> __SelfT;

    // fetch data from storage backend or view overlay
    typename layout::RepresentationNoQual __get () const
    {
        // get storage backend type
        typedef typename StorageSelectorWithTests<layout>::T BE;

        // get view type
        typedef typename layout::ViewT VT;

        // get item via view implementation
        return VT::get (reinterpret_cast<const BE*> (this)->__get ());
    }

    // write data to storage backened
    void __set (const typename layout::RepresentationNoQual& v)
    {
        // get storage backend type and invoke on this pointer
        typedef typename StorageSelectorWithTests<layout>::T BE;

        // get view type
        typedef typename layout::ViewT VT;

        // set via view
        reinterpret_cast<BE*> (this)->__set (VT::set (v));
    }

    // assignment operator
    __SelfT& operator = (const typename layout::RepresentationNoQual& v)
    {
        __set (v);
        return *this;
    }

    // cast operator
    operator typename layout::RepresentationNoQual () const
    {
        return __get ();
    }

    // number operators
    __SelfT& operator++ ()
    {
        __set (__get () + 1);
        return *this;
    }

    typename layout::RepresentationNoQual operator++ (int)
    {
        typename layout::RepresentationNoQual t = __get ();
        __set (t + 1);
        return t;
    }

    __SelfT& operator-- ()
    {
        __set (__get () - 1);
        return *this;
    }

    typename layout::RepresentationNoQual operator-- (int)
    {
        typename layout::RepresentationNoQual t = __get ();
        __set (t - 1);
        return t;
    }

    template<typename O> __SelfT& operator += (const O& o)
    {
        __set (__get () + o);
        return *this;
    }

    template<typename O> __SelfT& operator -= (const O& o)
    {
        __set (__get () - o);
        return *this;
    }

    template<typename O> __SelfT& operator *= (const O& o)
    {
        __set (__get () * o);
        return *this;
    }

    template<typename O> __SelfT& operator /= (const O& o)
    {
        __set (__get () / o);
        return *this;
    }

    template<typename O> __SelfT& operator %= (const O& o)
    {
        __set (__get () % o);
        return *this;
    }

    // is this node storage on an array backend?
    typename ArraySfinaeReturnType<layout::hasArrayBackend>::T __isArrayStorageType () const
    {
        return 0;
    }
};


//-------------------------------------------------------------------------------------------------
// select operator implementation details based on member layout
//-------------------------------------------------------------------------------------------------
template<typename RT, bool isConst, bool isAggregatedInitializer, bool preventRead>
struct SelectOperatorImplementationDetails
{
    // prepare options
    static const bool isBool = IsBaseClassOf <bool, RT>::value && (!isConst);
    static const bool isIntegral= TypeInfo<RT>::isIntegral && (!isBool) && (!isConst);
    static const bool isNumber =
        TypeInfo<RT>::isNumber && (!isIntegral) && (!isBool) && (!isConst);

    // select implementation details
    static const int details =
        (isAggregatedInitializer || preventRead) ? WrapperOperatorSelector::aggregatedInitializer :
        isBool ? WrapperOperatorSelector::boolean :
        isIntegral ? WrapperOperatorSelector::integral :
        isNumber ? WrapperOperatorSelector::number :
        isConst ? WrapperOperatorSelector::constant :
        WrapperOperatorSelector::getSet;
};


//-------------------------------------------------------------------------------------------------
// Wrapper base without payload
//-------------------------------------------------------------------------------------------------
struct EmptyPayload {};


//-------------------------------------------------------------------------------------------------
// check if M is a multiple of B
//-------------------------------------------------------------------------------------------------
template<unsigned M, unsigned B>
struct IsMultipleOf
{
    static const bool val =  M == (B * (M / B));
};


//-------------------------------------------------------------------------------------------------
// Simple array implementation (create reference and pass back member with array storage backend)
//-------------------------------------------------------------------------------------------------
template<typename layout>
struct SimpleArrayImplementation: public ContainsBitfieldLayout
{
    // export for diagnostics api
    typedef layout __layout;

    // export array size
    static const int size = layout::arraySize;

    // use simplified array notation?
    static const int storageSize = sizeof (typename layout::StorageType);
    static const int storageBits = storageSize * 8;
    static const int innerOffset = (layout::offset % storageBits);
    static const int innerByteOffset = (layout::offset / storageBits) * storageSize;
    static const int lastArrayBitIndex = innerOffset +
        layout::bits * (layout::arraySize < 1 ? 1 : layout::arraySize);
    static const bool useSimplifiedArray =
        (!layout::hasArrayBackend) && (lastArrayBitIndex <= storageBits);

    // is it possible to guarantee, that the member will fit into a single storage cell?
    static const bool isSingleCellAccess = (!layout::hasArrayBackend) &&
        IsMultipleOf<sizeof (typename layout::StorageType) * 8, layout::bits>::val &&
        IsMultipleOf<innerOffset, layout::bits>::val;

    // rewrite layout to use array storage backend, add const qualifier if to member if required
    typedef typename layout::template AsArrayStorage<false, isSingleCellAccess,
        useSimplifiedArray>::T ArrayLayout;
    typedef typename layout::template AsArrayStorage<true, isSingleCellAccess,
        useSimplifiedArray>::T ArrayLayoutConst;

    // working on aggregated initializer
    static const bool isAggregatedInitializer = layout::isAggregatedInitializer;

    // select wrapper operator implementation details
    static const int details = SelectOperatorImplementationDetails
        <typename layout::MemberTypeNoQual, layout::isConst, isAggregatedInitializer,
        layout::preventRead>::details;

    // create constant array reference
    WrapperImplementation<ArrayReferenceBase, ArrayLayoutConst, details>
    operator [] (int index) const
    {
        WrapperImplementation<ArrayReferenceBase, ArrayLayoutConst, details> r;
        ArrayReferenceBase* arr = reinterpret_cast<ArrayReferenceBase*> (&r);

        // use array address conversion
        index = layout::ArrayAddressConversion::convertArrayAddr (index, layout::arraySizeRaw);

        // are we already nested inside an array declaration?
        if (layout::hasArrayBackend)
        {
            const ArrayReferenceBase* oldArr = reinterpret_cast<const ArrayReferenceBase*> (this);
            arr->basePointer = oldArr->basePointer;
            arr->offsetBits = oldArr->offsetBits + layout::offset + (index * layout::bits);
        }
        else
        {
            if (useSimplifiedArray)
            {
                // create new simplified array declaration
                arr->basePointer = const_cast<char*> (reinterpret_cast<const char*> (this))
                    + innerByteOffset;
                arr->offsetBits = innerOffset + layout::bits * index;
            }
            else
            {
                // create new array declaration
                arr->basePointer = const_cast<void*> (reinterpret_cast<const void*> (this));
                arr->offsetBits = layout::offset + (index * layout::bits);
            }
        }

        return r;
    }

    // create non-const array reference
    WrapperImplementation<ArrayReferenceBase, ArrayLayout, details>
    operator [] (int index)
    {
        WrapperImplementation<ArrayReferenceBase, ArrayLayout, details> r;
        ArrayReferenceBase* arr = reinterpret_cast<ArrayReferenceBase*> (&r);

        // use array address conversion
        index = layout::ArrayAddressConversion::convertArrayAddr (index, layout::arraySizeRaw);

        // are we already nested inside an array declaration?
        if (layout::hasArrayBackend)
        {
            ArrayReferenceBase* oldArr = const_cast<ArrayReferenceBase*>
                (reinterpret_cast<const ArrayReferenceBase*> (this));
            arr->basePointer = oldArr->basePointer;
            arr->offsetBits = oldArr->offsetBits + layout::offset + (index * layout::bits);
        }
        else
        {
            if (useSimplifiedArray)
            {
                // create new simplified array declaration
                arr->basePointer = const_cast<char*> (reinterpret_cast<const char*> (this))
                    + innerByteOffset;
                arr->offsetBits = innerOffset + layout::bits * index;
            }
            else
            {
                // create new array declaration
                arr->basePointer = const_cast<void*> (reinterpret_cast<const void*> (this));
                arr->offsetBits = layout::offset + (index * layout::bits);
            }
        }

        return r;
    }
};


//-------------------------------------------------------------------------------------------------
// Complex array implementation (create reference and pass back member with array storage backend)
//-------------------------------------------------------------------------------------------------
template<typename layout, bool requireArrayReference>
struct ComplexArrayImplementationInner: public ContainsBitfieldLayout
{
    // export for diagnostics api
    typedef layout __layout;

    // export array size
    static const int size = layout::arraySize;

    // get member type with rewritten storage backend
    typedef typename layout::MemberType::template __WithStorageBackend<ArrayBackend>::T NMemberT;

    // get complex array implementation (const)
    const NMemberT operator [] (int index) const
    {
        // create array reference storage
        NMemberT mt;
        ArrayReferenceBase* arr = reinterpret_cast<ArrayReferenceBase*> (&mt);

        // use array address conversion
        index = layout::ArrayAddressConversion::convertArrayAddr (index, layout::arraySizeRaw);

        // are we already nested inside an array declaration?
        if (layout::hasArrayBackend)
        {
            const ArrayReferenceBase* oldArr = reinterpret_cast<const ArrayReferenceBase*> (this);
            arr->basePointer = oldArr->basePointer;
            arr->offsetBits += layout::offset + (index * layout::bits);
        }
        else
        {
            // create new array declaration
            arr->basePointer = const_cast<void*> (reinterpret_cast<const void*> (this));
            arr->offsetBits = layout::offset + (index * layout::bits);
        }

        // return new array storage pointer
        return mt;
    }

    // get complex array implementation (const)
    NMemberT operator [] (int index)
    {
        // create array reference storage
        NMemberT mt;
        ArrayReferenceBase* arr = reinterpret_cast<ArrayReferenceBase*> (&mt);

        // use array address conversion
        index = layout::ArrayAddressConversion::convertArrayAddr (index, layout::arraySizeRaw);

        // are we already nested inside an array declaration?
        if (layout::hasArrayBackend)
        {
            const ArrayReferenceBase* oldArr = reinterpret_cast<const ArrayReferenceBase*> (this);
            arr->basePointer = oldArr->basePointer;
            arr->offsetBits += layout::offset + (index * layout::bits);
        }
        else
        {
            // create new array declaration
            arr->basePointer = const_cast<void*> (reinterpret_cast<const void*> (this));
            arr->offsetBits = layout::offset + (index * layout::bits);
        }

        // return new array storage pointer
        return mt;
    }
};


//-------------------------------------------------------------------------------------------------
// Complex array implementation (optimized case: use pointer offset instead of array reference)
//-------------------------------------------------------------------------------------------------
template<typename layout>
struct ComplexArrayImplementationInner<layout, false>: public ContainsBitfieldLayout
{
    // export for diagnostics api
    typedef layout __layout;

    // export array size
    static const int size = layout::arraySize;

    // working on aggregated initializer
    static const bool isAggregatedInitializer =
        layout::isAggregatedInitializer;

    // return type declaration
    typedef typename __layout::MemberType::__DerefArray::T ReturnT;

    // array index operator
    ReturnT& operator [] (int index)
    {
        // use array address conversion
        index = layout::ArrayAddressConversion::convertArrayAddr (index, layout::arraySizeRaw);

        // calculate new pointer and return wrapper to member type directly
        char* t = reinterpret_cast<char*> (this);
        t += (layout::bits * index + layout::offset) / 8;
        return *(reinterpret_cast<ReturnT*> (t));
    }

    // array index operator (const)
    const ReturnT& operator [] (int index) const
    {
        // use array address conversion
        index = layout::ArrayAddressConversion::convertArrayAddr (index, layout::arraySizeRaw);

        // calculate new pointer and return wrapper to member type directly
        const char* t = reinterpret_cast<const char*> (this);
        t += (layout::bits * index + layout::offset) / 8;
        return *(reinterpret_cast<const ReturnT*> (t));
    }
};


//-------------------------------------------------------------------------------------------------
// Selector: use full array notation or base pointer offset
//-------------------------------------------------------------------------------------------------
template<typename layout>
struct ComplexArrayImplementation
{
    static const unsigned requiredBoudaryBits = __UNALIGNED_ACCESS_SUPPORTED ? 8
        : (8 * sizeof (typename layout::StorageType));
    static const bool sizeIsLayedOut = (layout::bits % requiredBoudaryBits) == 0;
    static const bool offsetIsLayedOut = (layout::offset % requiredBoudaryBits) == 0;
    static const bool isLayedOut = sizeIsLayedOut && offsetIsLayedOut &&
            (!layout::hasArrayBackend);
    typedef ComplexArrayImplementationInner<layout, !isLayedOut> T;
};


//-------------------------------------------------------------------------------------------------
// Assert layout is valid
//-------------------------------------------------------------------------------------------------
template<typename layout>
struct LayoutAssertions
{
    // run compile time assertions
    typedef typename CheckAssertion<
        StorageBackendAssertions<layout>::succeeded &&
        MemberLayoutAssertions<layout>::succeeded
    >::Succeeded Succeeded;
};


//-------------------------------------------------------------------------------------------------
// compile time factory for bit-field members
//-------------------------------------------------------------------------------------------------
template<typename layout>
struct MemberWrapperImplementationFactory: public LayoutAssertions<layout>::Succeeded
{
    // working on aggregated initializer?
    static const bool isAggregatedInitializer = layout::isAggregatedInitializer;

    // select wrapper
    static const int details = SelectOperatorImplementationDetails
        <typename layout::MemberTypeNoQual, layout::isConst, isAggregatedInitializer,
        layout::preventRead>::details;
    typedef WrapperImplementation<EmptyPayload, layout, details> T;
};


//-------------------------------------------------------------------------------------------------
// compile time factory for bit-field array members with trivial data type
//-------------------------------------------------------------------------------------------------
template<typename layout>
struct SimpleArrayWrapperImplementationFactory: public LayoutAssertions<layout>::Succeeded
{
    // select wrapper
    typedef SimpleArrayImplementation<layout> T;
};


//-------------------------------------------------------------------------------------------------
// compile time factory for bit-field array members with nesting
//-------------------------------------------------------------------------------------------------
template<typename layout>
struct ComplexArrayWrapperImplementationFactory: public LayoutAssertions<layout>::Succeeded
{
    // select wrapper
    typedef typename ComplexArrayImplementation<layout>::T T;
};


} // namespace __bitfields
