//=================================================================================================
//
//   Compiler independent safe bit-fields for C++98
//
//   File:          /include/bitfield/backend.h
//
//   Description:   Bongo style storage backend implementation to access bit-field members.
//
//
//   License:       Copyright (c) 2018 Julian Dessecker
//
//                  Permission is hereby granted, free of charge, to any person obtaining
//                  a copy of this software and associated documentation files
//                  (the "Software"), to deal in the Software without restriction,
//                  including without limitation the rights to use, copy, modify, merge,
//                  publish, distribute, sublicense, and/or sell copies of the Software,
//                  and to permit persons to whom the Software is furnished to do so,
//                  subject to the following conditions:
//
//                  The above copyright notice and this permission notice shall be
//                  included in all copies or substantial portions of the Software.
//
//                  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
//                  EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
//                  OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
//                  NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS
//                  BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN
//                  ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
//                  CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
//                  SOFTWARE.
//
//=================================================================================================


#pragma once


// check if we are included from top-level header
#ifndef __BITFIELDS__INCLUDE__GUARD__
#   error Do not include this file directly, inlude bitfield.h instead
#endif


#include "stubs.h"
#include "../util/typeinfo.h"


namespace __bitfields {


using namespace dstd::meta;


// TODO: handle multi-cell access


//-------------------------------------------------------------------------------------------------
// Compile time assertions on member layouts
//-------------------------------------------------------------------------------------------------
template<typename layout>
struct StorageBackendAssertions
{
    // test conditions here...
    typedef CheckAssertion<
        true    // assert conditions here...
    > Assertion;

    // propagate compile-time asserts
    static const bool succeeded = Assertion::succeeded;
    typedef typename Assertion::Succeeded Succeeded;
};


//-------------------------------------------------------------------------------------------------
// storage backend toolkit: storage layout & calculations for addresses and bit offsets
//-------------------------------------------------------------------------------------------------
template<typename _ST, typename _STN, typename _MT, typename _MTN, int _bits, int _offset,
    bool _isArrayType, bool _isSimplifiedArray, bool _fitsIntoSingleCell, bool _storageRelaxed,
    bool _preventRead>
struct StorageBackendTookit
{
    // export types and constants
    typedef _ST Storage;
    typedef _STN StorageNoQual;
    typedef _MT MemberType;
    typedef _MTN MemberTypeNoQual;
    static const int bits = _bits;
    static const int offset = _offset;
    static const bool isArrayType = _isArrayType;
    static const bool isSimplifiedArray = _isSimplifiedArray;
    static const bool fitsIntoSingleCell = _fitsIntoSingleCell;
    static const bool storageRelaxed = _storageRelaxed;
    static const bool preventRead = _preventRead;

    // export compile time address constants
    static const int storageBits = sizeof (Storage) * 8;
    static const bool isSigned = TypeInfo<MemberTypeNoQual>::isSigned;

    // export intermediate conversion type
    static const int maxSize = storageBits > (sizeof (MemberType) * 8) ?
            storageBits : sizeof (MemberType) * 8;
    typedef typename CreateNumberType<maxSize, false>::T IntermediateType;

    // is this member a boolean member type?
    static const bool isBool = TypeInfo<MemberTypeNoQual>::isBool;

    // pointer to storage element
    inline Storage* storagePtr () const
    {
        // cast this to StorageType*
        Storage* st = const_cast<Storage*> (reinterpret_cast<const Storage*> (this));

        if (_isSimplifiedArray)
        {
            // access simplified array reference
            ArrayReferenceBase* aRef = const_cast<ArrayReferenceBase*>
                    (reinterpret_cast<const ArrayReferenceBase*> (this));
            return const_cast<Storage*> (reinterpret_cast<const Storage*> (aRef->basePointer));
        }
        else if (_isArrayType)
        {
            // access array reference
            ArrayReferenceBase* aRef = const_cast<ArrayReferenceBase*>
                    (reinterpret_cast<const ArrayReferenceBase*> (this));
            Storage* basePtr = const_cast<Storage*>
                (reinterpret_cast<const Storage*> (aRef->basePointer));
            const int bitOff = offset + aRef->offsetBits;

            // return pointer to first storage element
            return (basePtr + (bitOff / storageBits));
        }
        else
        {
            // direct addressing scheme
            return (st + (offset / storageBits));
        }
    }

    // bit offset inside storage element
    inline int bitOffset () const
    {
        if (_isSimplifiedArray)
        {
            // access simplified array reference
            ArrayReferenceBase* aRef = const_cast<ArrayReferenceBase*>
                (reinterpret_cast<const ArrayReferenceBase*> (this));
            const int bitOff = offset + aRef->offsetBits;

            // return bit offset inside first storage element
            return bitOff;
        }
        else if (_isArrayType)
        {
            // access array reference
            ArrayReferenceBase* aRef = const_cast<ArrayReferenceBase*>
                (reinterpret_cast<const ArrayReferenceBase*> (this));
            const int bitOff = offset + aRef->offsetBits;

            // return bit offset inside first storage element
            return bitOff & (storageBits - 1);
        }
        else
        {
            // direct addressing scheme
            return offset & (storageBits - 1);
        }
    }
};


//-------------------------------------------------------------------------------------------------
// storage backend trait: read / write single bit
//-------------------------------------------------------------------------------------------------
template<typename TK, bool isBool>
struct StorageBackendSelectorImpl
{
    // get data from storage backend
    inline bool __get () const
    {
        // get toolkit instance
        const TK* tk = reinterpret_cast<const TK*> (this);
        if (!TK::preventRead)
        {
            // read data from storage location
            typename TK::StorageNoQual st = *(tk->storagePtr ());

            // return single bit
            const int bitIdx = TK::storageBits - tk->bitOffset () - 1;
            return (st & (1 << bitIdx)) != 0;
        }
        else
        {
            return false;
        }
    }

    // perform read-modify update sequence on storage location
    inline void __set (const bool& v)
    {
        // get toolkit instance
        const TK* tk = reinterpret_cast<const TK*> (this);

        // read data from storage location
        typename TK::StorageNoQual st = TK::preventRead ? 0 : *(tk->storagePtr ());

        // modify single bit
        const int bitIdx = TK::storageBits - tk->bitOffset () - 1;
        const typename TK::StorageNoQual setMask = 1 << bitIdx;
        const typename TK::StorageNoQual clrMask = ~setMask;

        if (v)
        {
            st |= setMask;
        }
        else
        {
            st &= clrMask;
        }

        // write back
        *(tk->storagePtr ()) = st;
    }
};


//-------------------------------------------------------------------------------------------------
// storage backend trait: read / write integer (non-overlapping)
//-------------------------------------------------------------------------------------------------
template<typename TK, bool fitsIntoSingleCell>
struct StorageBackendIntegerTrait
{
    // type and layout shortcuts
    typedef typename TK::MemberTypeNoQual MTN;
    typedef typename TK::Storage ST;
    typedef typename TK::StorageNoQual STN;
    typedef typename TK::IntermediateType IT;

    // layout dependent sign extension constants
    static const int signExtendShift = TK::bits >= (sizeof (IT) * 8) ? -1 : TK::bits;

    // layout dependent mask constants
    static const IT valueMask = (TK::bits == TK::storageBits) ? ~0 : (1 << TK::bits) - 1;

    // get data from storage backend
    inline MTN __get () const
    {
        if (!TK::preventRead)
        {
            // get toolkit instance
            const TK* tk = reinterpret_cast<const TK*> (this);

            // read data from storage location
            const int ITSize = sizeof (IT);
            STN st = *(tk->storagePtr ());

            // move bits, so that we are left aligned inside the intermediate type
            IT t = st;
            if (sizeof (ST) < sizeof (IT))
            {
                t <<= (sizeof (IT) - sizeof (ST)) * 8;
            }

            // apply member bit offset
            t <<= tk->bitOffset ();

            // move down bits to right boundary
            t >>= ((ITSize * 8) - TK::bits);

            // need to sign extend?
            if (TK::isSigned)
            {
                if ((t & (1 << (TK::bits - 1))) != 0)
                {
                    t |= static_cast<IT> (signExtendShift > -1 ? ((~0ULL) << signExtendShift) : 0);
                }
            }

            // return casted member type
            MTN rt = static_cast <MTN> (t);
            return rt;
        }
        else
        {
            return static_cast<MTN> (0);
        }
    }

    // perform read-modify update sequence on storage location
    inline void __set (const MTN& v)
    {
        // get toolkit instance
        const TK* tk = reinterpret_cast<const TK*> (this);

        // calculate left shift amount
        const int lShift = TK::storageBits - tk->bitOffset () - TK::bits;

        // read data from storage location
        STN st = TK::preventRead ? 0 : *(tk->storagePtr ());

        // shift storage mask and mask storage value
        STN storageMask = ~(valueMask << lShift);
        st &= storageMask;

        // inject value into temporary storage
        STN tv = static_cast<STN> (v);
        tv &= valueMask;
        st |= (tv << lShift);

        // write modified data
        *(tk->storagePtr ()) = st;
    }
};


//-------------------------------------------------------------------------------------------------
// storage backend trait: read / write integer (overlapping)
//-------------------------------------------------------------------------------------------------
template<typename TK>
struct StorageBackendIntegerTrait<TK, false>
{
    // TODO: overlapping read / write trait not implemented yet
};


//-------------------------------------------------------------------------------------------------
// storage backend trait: read / write integer (non-overlapping)
//-------------------------------------------------------------------------------------------------
template<typename TK>
struct StorageBackendSelectorImpl<TK, false>
{
    typedef typename TK::MemberTypeNoQual MTN;

    // get data from storage backend
    inline MTN __get () const
    {
        return reinterpret_cast<const StorageBackendIntegerTrait<TK, TK::fitsIntoSingleCell>*>
            (this)->__get ();
    }

    // perform read-modify update sequence on storage location
    inline void __set (const MTN& v)
    {
        reinterpret_cast<StorageBackendIntegerTrait<TK, TK::fitsIntoSingleCell>*>
            (this)->__set (v);
    }
};


//-------------------------------------------------------------------------------------------------
// select backend type fitting to layout
//-------------------------------------------------------------------------------------------------
template <typename layout>
struct StorageBackendSelector
{
    static const int storageBits = sizeof (typename layout::StorageType) * 8;

    // is this member a boolean type?
    static const bool isBool = TypeInfo<typename layout::MemberTypeNoQual>::isBool;

    // is this member represented as an array reference?
    static const bool isArray = layout::hasArrayBackend;

    // is this member represented as simplified array reference?
    static const bool isSimplifiedArray = layout::isSimplifiedArray && isArray;

    // constant addressing scheme?
    static const bool isConstantLayout = !isArray;

    // constant addressed, fitting into single storage cell?
    static const bool constLayoutFitsIntoSingleCell =
        ((layout::offset % storageBits) + layout::bits) <= storageBits;

    // check if array fits into single storage cell
    static const bool arrayFitsIntoSingleCell = isSimplifiedArray || layout::isSingleCellAccess;

    // guaranteed to fit into a single storage cell
    static const bool fitsIntoSingleCell = isBool || arrayFitsIntoSingleCell
        || (isConstantLayout && constLayoutFitsIntoSingleCell);

    // storage backend toolkit instance
    typedef StorageBackendTookit<
        typename layout::StorageType,
        typename layout::StorageTypeNoQual,
        typename layout::MemberType,
        typename layout::MemberTypeNoQual,
        layout::bits,
        layout::offset,
        isArray,
        isSimplifiedArray,
        fitsIntoSingleCell,
        layout::storageRelaxed,
        layout::preventRead
    > Toolkit;

    // instantiate backend
    typedef StorageBackendSelectorImpl<Toolkit, isBool> T;
};


} // namespace __bitfields
