//=================================================================================================
//
//   Compiler independent safe bit-fields for C++98
//
//   File:          /include/bitfield/assertions.h
//
//   Description:   Compile time asserts for bit-field members
//
//
//   License:       Copyright (c) 2018 Julian Dessecker
//
//                  Permission is hereby granted, free of charge, to any person obtaining
//                  a copy of this software and associated documentation files
//                  (the "Software"), to deal in the Software without restriction,
//                  including without limitation the rights to use, copy, modify, merge,
//                  publish, distribute, sublicense, and/or sell copies of the Software,
//                  and to permit persons to whom the Software is furnished to do so,
//                  subject to the following conditions:
//
//                  The above copyright notice and this permission notice shall be
//                  included in all copies or substantial portions of the Software.
//
//                  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
//                  EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
//                  OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
//                  NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS
//                  BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN
//                  ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
//                  CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
//                  SOFTWARE.
//
//=================================================================================================


#pragma once


// check if we are included from top-level header
#ifndef __BITFIELDS__INCLUDE__GUARD__
#   error Do not include this file directly, inlude bitfield.h instead
#endif


#include "stubs.h"
#include "../util/typeinfo.h"


namespace __bitfields {


//-------------------------------------------------------------------------------------------------
// check for non matching inner / outer type
//-------------------------------------------------------------------------------------------------
template<typename layout>
struct MemberLayoutAssert_InnerOuterMismatch
{
    static const bool succeeded = !layout::innerOuterTypeMismatch;
};

//-------------------------------------------------------------------------------------------------
// Compile time assertions on member layouts
//-------------------------------------------------------------------------------------------------
template<typename layout>
struct MemberLayoutAssertions
{
    // test conditions here...
    typedef CheckAssertion<
            MemberLayoutAssert_InnerOuterMismatch<layout>::succeeded
    > AssertionInnerOuterMismatch;

    // propagate compile-time asserts
    static const bool succeeded = AssertionInnerOuterMismatch::succeeded;
};


} // namespace __bitfields
