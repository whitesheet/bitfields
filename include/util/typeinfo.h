//=================================================================================================
//
//   Dessecker standard framework - meta template type informations
//
//   File:          /include/util/typeinfo.h
//
//   Description:   Basic informations on build-in data types, pointers, references and qualifiers
//
//   License:       Copyright (c) 2015 Julian Dessecker
//
//                  Permission is hereby granted, free of charge, to any person obtaining
//                  a copy of this software and associated documentation files
//                  (the "Software"), to deal in the Software without restriction,
//                  including without limitation the rights to use, copy, modify, merge,
//                  publish, distribute, sublicense, and/or sell copies of the Software,
//                  and to permit persons to whom the Software is furnished to do so,
//                  subject to the following conditions:
//
//                  The above copyright notice and this permission notice shall be
//                  included in all copies or substantial portions of the Software.
//
//                  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
//                  EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
//                  OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
//                  NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS
//                  BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN
//                  ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
//                  CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
//                  SOFTWARE.
//
//=================================================================================================


#pragma once


#include <stdint.h>


namespace dstd {
namespace meta {


//-------------------------------------------------------------------------------------------------
// Strip first qualifier
//-------------------------------------------------------------------------------------------------
template<typename _T>
struct StripQualifier
{
    typedef _T T;
};

template<typename _T>
struct StripQualifier<const _T>
{
    typedef typename StripQualifier<_T>::T T;
};

template<typename _T>
struct StripQualifier<volatile _T>
{
    typedef typename StripQualifier<_T>::T T;
};

template<typename _T>
struct StripQualifier<const volatile _T>
{
    typedef typename StripQualifier<_T>::T T;
};

template<typename _T>
struct StripQualifier<_T*>
{
    typedef typename StripQualifier<_T>::T T;
};

template<typename _T>
struct StripQualifier<_T&>
{
    typedef typename StripQualifier<_T>::T T;
};

template<typename _T>
struct StripCQualifier
{
    typedef _T T;
};

template<typename _T>
struct StripCQualifier<const _T>
{
    typedef _T T;
};

template<typename _T>
struct StripCQualifier<const volatile _T>
{
    typedef volatile _T T;
};

template<typename _T>
struct StripVQualifier
{
    typedef _T T;
};

template<typename _T>
struct StripVQualifier<volatile _T>
{
    typedef _T T;
};

template<typename _T>
struct StripVQualifier<const volatile _T>
{
    typedef const _T T;
};

template<typename _T>
struct StripCVQualifier
{
    typedef _T T;
};

template<typename _T>
struct StripCVQualifier<const volatile _T>
{
    typedef _T T;
};


//-------------------------------------------------------------------------------------------------
// Strip qualifiers (recursive)
//-------------------------------------------------------------------------------------------------
template<typename _T, bool stopOnReferenceOrPointer = false>
struct StripQualifiers
{
    typedef _T T;
};

template<typename _T>
struct StripQualifiers<const _T, false>
{
    typedef typename StripQualifiers<_T, false>::T T;
};

template<typename _T>
struct StripQualifiers<volatile _T, false>
{
    typedef typename StripQualifiers<_T, false>::T T;
};

template<typename _T>
struct StripQualifiers<const volatile _T, false>
{
    typedef typename StripQualifiers<_T, false>::T T;
};

template<typename _T>
struct StripQualifiers<_T*, false>
{
    typedef typename StripQualifiers<_T, false>::T T;
};

template<typename _T>
struct StripQualifiers<_T&, false>
{
    typedef typename StripQualifiers<_T, false>::T T;
};

template<typename _T>
struct StripQualifiers<const _T, true>
{
    typedef typename StripQualifiers<_T, true>::T T;
};

template<typename _T>
struct StripQualifiers<volatile _T, true>
{
    typedef typename StripQualifiers<_T, true>::T T;
};

template<typename _T>
struct StripQualifiers<const volatile _T, true>
{
    typedef typename StripQualifiers<_T, true>::T T;
};

template<typename _T>
struct StripQualifiers<_T*, true>
{
    typedef  _T* T;
};

template<typename _T>
struct StripQualifiers<_T&, true>
{
    typedef _T& T;
};


//-------------------------------------------------------------------------------------------------
// check inheritance, B must be a base of D
//-------------------------------------------------------------------------------------------------
template <typename B, typename D>
struct IsBaseClassOf
{
    typedef char yes[1];
    typedef char no[2];
    static yes& test (const B*);
    static no& test (...);
    static const D* get (void);
    static const bool value = sizeof (test (get ())) == sizeof (yes);
};


//-------------------------------------------------------------------------------------------------
// Is a signed number?
//-------------------------------------------------------------------------------------------------
template<typename T, bool isNumber>
struct IsSigned
{
    static const bool value = false;
};

template<typename T>
struct IsSigned<T, true>
{
    static const bool value = ((T) - 1) < 0;
};


//-------------------------------------------------------------------------------------------------
// Get informations on number types
//-------------------------------------------------------------------------------------------------
template<typename T>
struct TypeInfoNumber
{
    static const bool isIntegral =
        IsBaseClassOf <char, T>::value ||
        IsBaseClassOf <unsigned char, T>::value ||
        IsBaseClassOf <signed char, T>::value ||
        IsBaseClassOf <unsigned short, T>::value ||
        IsBaseClassOf <signed short, T>::value ||
        IsBaseClassOf <unsigned int, T>::value ||
        IsBaseClassOf <signed int, T>::value ||
        IsBaseClassOf <unsigned long, T>::value ||
        IsBaseClassOf <signed long, T>::value ||
        IsBaseClassOf <unsigned long long, T>::value ||
        IsBaseClassOf <signed long long, T>::value;

    static const bool isNumber = isIntegral ||
        IsBaseClassOf <float, T>::value ||
        IsBaseClassOf <double, T>::value ||
        IsBaseClassOf <long double, T>::value;

    static const bool isSigned = IsSigned<T, isNumber>::value;
};


//-------------------------------------------------------------------------------------------------
// Create number type
//-------------------------------------------------------------------------------------------------
template<unsigned size, bool isSigned>
struct CreateNumberType
{
};

template<>
struct CreateNumberType<8, true>
{
    typedef int8_t T;
};

template<>
struct CreateNumberType<8, false>
{
    typedef uint8_t T;
};

template<>
struct CreateNumberType<16, true>
{
    typedef int16_t T;
};

template<>
struct CreateNumberType<16, false>
{
    typedef uint16_t T;
};

template<>
struct CreateNumberType<32, true>
{
    typedef int32_t T;
};

template<>
struct CreateNumberType<32, false>
{
    typedef uint32_t T;
};

template<>
struct CreateNumberType<64, true>
{
    typedef int64_t T;
};

template<>
struct CreateNumberType<64, false>
{
    typedef uint64_t T;
};


//-------------------------------------------------------------------------------------------------
// Is boolean type?
//-------------------------------------------------------------------------------------------------
template<typename T> struct TypeInfoIsBool
{
    static const bool isBool = false;
};

template<> struct TypeInfoIsBool<bool>
{
    static const bool isBool = true;
};


//-------------------------------------------------------------------------------------------------
// Type informations
//-------------------------------------------------------------------------------------------------
template<typename T>
struct TypeInfo
{
    typedef T UT;
    static const int size = sizeof (T);
    static const bool isConst = false;
    static const bool isVolatile = false;
    static const bool isPointer = false;
    static const bool isReference = false;
    static const bool isBool = TypeInfoIsBool<T>::isBool;
    static const bool isIntegral = TypeInfoNumber<typename StripQualifiers<T>::T>::isIntegral;
    static const bool isNumber = TypeInfoNumber<typename StripQualifiers<T>::T>::isNumber;
    static const bool isSigned = TypeInfoNumber<typename StripQualifiers<T>::T>::isSigned;
};

// is constant type
template <typename T>
struct TypeInfo<const T>
{
    typedef typename TypeInfo<T>::UT UT;
    static const int size = sizeof (typename TypeInfo<T>::UT);
    static const bool isConst = true;
    static const bool isPointer = TypeInfo<T>::isPointer;
    static const bool isReference = TypeInfo<T>::isReference;
    static const bool isVolatile = TypeInfo<T>::isVolatile;
    static const bool isBool = TypeInfo<T>::isBool;
    static const bool isIntegral = TypeInfo<T>::isIntegral;
    static const bool isNumber = TypeInfo<T>::isNumber;
    static const bool isSigned = TypeInfo<T>::isSigned;
};

// is volatile type
template <typename T>
struct TypeInfo<volatile T>
{
    typedef typename TypeInfo<T>::UT UT;
    static const int size = sizeof (typename TypeInfo<T>::UT);
    static const bool isConst = TypeInfo<T>::isConst;
    static const bool isVolatile = true;
    static const bool isPointer = TypeInfo<T>::isPointer;
    static const bool isReference = TypeInfo<T>::isReference;
    static const bool isBool = TypeInfo<T>::isBool;
    static const bool isIntegral = TypeInfo<T>::isIntegral;
    static const bool isNumber = TypeInfo<T>::isNumber;
    static const bool isSigned = TypeInfo<T>::isSigned;
};

// is const volatile type
template <typename T>
struct TypeInfo<const volatile T>
{
    typedef typename TypeInfo<T>::UT UT;
    static const int size = sizeof (UT);
    static const bool isConst = true;
    static const bool isVolatile = true;
    static const bool isPointer = TypeInfo<T>::isPointer;
    static const bool isReference = TypeInfo<T>::isReference;
    static const bool isBool = TypeInfo<T>::isBool;
    static const bool isIntegral = TypeInfo<T>::isIntegral;
    static const bool isNumber = TypeInfo<T>::isNumber;
    static const bool isSigned = TypeInfo<T>::isSigned;
};

// is pointer type
template <typename T>
struct TypeInfo<T*>
{
    typedef typename TypeInfo<T>::UT UT;
    static const int size = sizeof (void*);
    static const bool isConst = false;
    static const bool isVolatile = false;
    static const bool isPointer = true;
    static const bool isReference = false;
    static const bool isBool = TypeInfo<T>::isBool;
    static const bool isIntegral = false;
    static const bool isNumber = false;
    static const bool isSigned = false;
};

// do not allow to use references
template <typename T>
struct TypeInfo<T&>
{
    typedef typename TypeInfo<T>::UT UT;
    static const int size = sizeof (void*);
    static const bool isConst = false;
    static const bool isVolatile = false;
    static const bool isPointer = false;
    static const bool isReference = true;
    static const bool isBool = TypeInfo<T>::isBool;
    static const bool isIntegral = false;
    static const bool isNumber = false;
    static const bool isSigned = false;
};


//-------------------------------------------------------------------------------------------------
// Create constant type
//-------------------------------------------------------------------------------------------------
template<typename _T>
struct MakeConstType
{
    typedef const _T T;
};


template<typename _T, bool condition>
struct MakeConstTypeCond
{
    typedef const _T T;
};


template<typename _T>
struct MakeConstTypeCond<_T, false>
{
    typedef _T T;
};


//-------------------------------------------------------------------------------------------------
// Create volatile type
//-------------------------------------------------------------------------------------------------
template<typename _T>
struct MakeVolatileType
{
    typedef volatile _T T;
};


template<typename _T, bool condition>
struct MakeVolatileTypeCond
{
    typedef volatile _T T;
};


template<typename _T>
struct MakeVolatileTypeCond<_T, false>
{
    typedef _T T;
};


} // namespace meta
} // namespace dstd
