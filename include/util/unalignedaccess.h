//=================================================================================================
//
//   Dessecker standard framework - compile time flag: architecture supports unaligned access
//
//   File:          /include/util/unalignedaccess.h
//
//   Description:   Compile time flag: architecture supports unaligned access.
//                  The flag is set for all known architectures which support unaligned access.
//                  It will not be defined for architectures like M4 which support unaligned access
//                  depending on the core's configuration. In such cases you should define the
//                  macro manually if your configuration supports unaligned access. This flag
//                  helps to perform some optimizations if possible.
//
//   License:       Copyright (c) 2015 Julian Dessecker
//
//                  Permission is hereby granted, free of charge, to any person obtaining
//                  a copy of this software and associated documentation files
//                  (the "Software"), to deal in the Software without restriction,
//                  including without limitation the rights to use, copy, modify, merge,
//                  publish, distribute, sublicense, and/or sell copies of the Software,
//                  and to permit persons to whom the Software is furnished to do so,
//                  subject to the following conditions:
//
//                  The above copyright notice and this permission notice shall be
//                  included in all copies or substantial portions of the Software.
//
//                  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
//                  EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
//                  OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
//                  NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS
//                  BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN
//                  ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
//                  CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
//                  SOFTWARE.
//
//=================================================================================================


#pragma once


// amd x86_64 architecture
#ifdef __amd64__
#   define __UNALIGNED_ACCESS_SUPPORTED 1
#endif
#ifdef __amd64
#   define __UNALIGNED_ACCESS_SUPPORTED 1
#endif
#ifdef __x86_64__
#   define __UNALIGNED_ACCESS_SUPPORTED 1
#endif
#ifdef __x86_64
#   define __UNALIGNED_ACCESS_SUPPORTED 1
#endif
#ifdef _M_X64
#   define __UNALIGNED_ACCESS_SUPPORTED 1
#endif
#ifdef _M_AMD64
#   define __UNALIGNED_ACCESS_SUPPORTED 1
#endif

// intel i386 architecture
#ifdef i386
#   define __UNALIGNED_ACCESS_SUPPORTED 1
#endif
#ifdef __i386
#   define __UNALIGNED_ACCESS_SUPPORTED 1
#endif
#ifdef __i386__
#   define __UNALIGNED_ACCESS_SUPPORTED 1
#endif
#ifdef __IA32__
#   define __UNALIGNED_ACCESS_SUPPORTED 1
#endif
#ifdef _M_I86
#   define __UNALIGNED_ACCESS_SUPPORTED 1
#endif
#ifdef _M_IX86
#   define __UNALIGNED_ACCESS_SUPPORTED 1
#endif
#ifdef __X86__
#   define __UNALIGNED_ACCESS_SUPPORTED 1
#endif
#ifdef __THW_INTEL__
#   define __UNALIGNED_ACCESS_SUPPORTED 1
#endif
#ifdef __I86__
#   define __UNALIGNED_ACCESS_SUPPORTED 1
#endif
#ifdef __INTEL__
#   define __UNALIGNED_ACCESS_SUPPORTED 1
#endif
#ifdef __386
#   define __UNALIGNED_ACCESS_SUPPORTED 1
#endif

// intel itanium ia64
#ifdef __ia64__
#   define __UNALIGNED_ACCESS_SUPPORTED 1
#endif
#ifdef _IA64
#   define __UNALIGNED_ACCESS_SUPPORTED 1
#endif
#ifdef __IA64__
#   define __UNALIGNED_ACCESS_SUPPORTED 1
#endif
#ifdef __ia64
#   define __UNALIGNED_ACCESS_SUPPORTED 1
#endif
#ifdef _M_IA64
#   define __UNALIGNED_ACCESS_SUPPORTED 1
#endif
#ifdef _M_IA64
#   define __UNALIGNED_ACCESS_SUPPORTED 1
#endif
#ifdef __itanium__
#   define __UNALIGNED_ACCESS_SUPPORTED 1
#endif

// power pc
#ifdef __powerpc
#   define __UNALIGNED_ACCESS_SUPPORTED 1
#endif
#ifdef __powerpc__
#   define __UNALIGNED_ACCESS_SUPPORTED 1
#endif
#ifdef __powerpc64__
#   define __UNALIGNED_ACCESS_SUPPORTED 1
#endif
#ifdef __POWERPC__
#   define __UNALIGNED_ACCESS_SUPPORTED 1
#endif
#ifdef __ppc__
#   define __UNALIGNED_ACCESS_SUPPORTED 1
#endif
#ifdef __ppc64__
#   define __UNALIGNED_ACCESS_SUPPORTED 1
#endif
#ifdef __PPC__
#   define __UNALIGNED_ACCESS_SUPPORTED 1
#endif
#ifdef __PPC64__
#   define __UNALIGNED_ACCESS_SUPPORTED 1
#endif
#ifdef _ARCH_PPC
#   define __UNALIGNED_ACCESS_SUPPORTED 1
#endif
#ifdef _ARCH_PPC64
#   define __UNALIGNED_ACCESS_SUPPORTED 1
#endif
#ifdef _M_PPC
#   define __UNALIGNED_ACCESS_SUPPORTED 1
#endif
#ifdef _ARCH_PPC
#   define __UNALIGNED_ACCESS_SUPPORTED 1
#endif
#ifdef _ARCH_PPC64
#   define __UNALIGNED_ACCESS_SUPPORTED 1
#endif
#ifdef __ppc
#   define __UNALIGNED_ACCESS_SUPPORTED 1
#endif

// undefine if zero
#ifndef __UNALIGNED_ACCESS_SUPPORTED
#   define __UNALIGNED_ACCESS_SUPPORTED 0
#endif
