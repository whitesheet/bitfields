//=================================================================================================
//
//   Dessecker standard framework - compile time endianness informations
//
//   File:          /include/util/endianness.h
//
//   Description:   Compile time informations about system endianness.
//
//   License:       Copyright (c) 2015 Julian Dessecker
//
//                  Permission is hereby granted, free of charge, to any person obtaining
//                  a copy of this software and associated documentation files
//                  (the "Software"), to deal in the Software without restriction,
//                  including without limitation the rights to use, copy, modify, merge,
//                  publish, distribute, sublicense, and/or sell copies of the Software,
//                  and to permit persons to whom the Software is furnished to do so,
//                  subject to the following conditions:
//
//                  The above copyright notice and this permission notice shall be
//                  included in all copies or substantial portions of the Software.
//
//                  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
//                  EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
//                  OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
//                  NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS
//                  BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN
//                  ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
//                  CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
//                  SOFTWARE.
//
//=================================================================================================


#pragma once


//-------------------------------------------------------------------------------------------------
// detect endianness
//-------------------------------------------------------------------------------------------------

#ifndef __ENDIANNESS

// using gnu c library?
#   if defined (__GNUC__)
#       include <endian.h>
#       if (__BYTE_ORDER == __BIG_ENDIAN)
#           define __ENDIANNESS 0
#       elif (__BYTE_ORDER == __LITTLE_ENDIAN)
#           define __ENDIANESS 1
#       else
#           error Can not detect endianness via gnu c lib. Please define __ENDIANNESS macro.
#       endif

// detect big endian platforms
#   elif defined (__BIG_ENDIAN__)
#       define __ENDIANNESS 0
#   elif defined (__ARMEB__)
#       define __ENDIANNESS 0
#   elif defined (__THUMBEB__)
#       define __ENDIANNESS 0
#   elif defined (__AARCH64EB__)
#       define __ENDIANNESS 0
#   elif defined (_MIPSEB)
#       define __ENDIANNESS 0
#   elif defined (__MIPSEB)
#       define __ENDIANNESS 0
#   elif defined (__MIPSEB__)
#       define __ENDIANNESS 0

// detect little endian platforms
#   elif defined (__LITTLE_ENDIAN__)
#       define __ENDIANNESS 1
#   elif defined (__ARMEL__)
#       define __ENDIANNESS 1
#   elif defined (__THUMBEL__)
#       define __ENDIANNESS 1
#   elif defined (__AARCH64EL__)
#       define __ENDIANNESS 1
#   elif defined (_MIPSEL)
#       define __ENDIANNESS 1
#   elif defined (__MIPSEL)
#       define __ENDIANNESS 1
#   elif defined (__MIPSEL__)
#       define __ENDIANNESS 1

// detect via byte order
#   elif defined (__BYTE_ORDER__) && (__BYTE_ORDER__ == __ORDER_BIG_ENDIAN__)
#       define __ENDIANNESS 0
#   elif defined (__BYTE_ORDER__) && (__BYTE_ORDER__ == __ORDER_LITTLE_ENDIAN__)
#       define __ENDIANNESS 1

// win32 platform? (assuming intel cpu with little endian)
#   elif defined (_WIN32)
#       define __ENDIANNESS 1

// not detected on compile time
#   else
#       error Can not detect endianness. Please define the __ENDIANNESS macro.
#   endif
#endif


//-------------------------------------------------------------------------------------------------
// endianness constants
//-------------------------------------------------------------------------------------------------

#ifdef __BIG_ENDIAN
#   undef __BIG_ENDIAN
#endif
#define __BIG_ENDIAN 0
#ifdef __LITTLE_ENDIAN
#   undef __LITTLE_ENDIAN
#endif
#define __LITTLE_ENDIAN 1


namespace util {


//-------------------------------------------------------------------------------------------------
// endianness conversion functions - default implementation, returns origial value
//-------------------------------------------------------------------------------------------------

// by returns the same value as argument *t*
template<typename T, unsigned cellSize, unsigned typeSize>
struct __SwapByteOrder
{
    static T conv (const T& t) {return t;}
};


template<typename T>
struct __SwapByteOrder<T, 1, 2>
{
    static T conv (const T& t)
    {
        return
            ((0xFF00 & t) >> 8) |
            ((0x00FF & t) << 8);
    }
};


template<typename T>
struct __SwapByteOrder<T, 1, 4>
{
    static T conv (const T& t)
    {
        return
            ((0xFF000000 & t) >> 24) |
            ((0x00FF0000 & t) >> 8) |
            ((0x0000FF00 & t) << 8) |
            ((0x000000FF & t) << 24);
    }
};


template<typename T>
struct __SwapByteOrder<T, 1, 8>
{
    static T conv (const T& t)
    {
        return
            ((0xFF00000000000000ULL & t) >> 56) |
            ((0x00FF000000000000ULL & t) >> 40) |
            ((0x0000FF0000000000ULL & t) >> 24) |
            ((0x000000FF00000000ULL & t) >> 8)  |
            ((0x00000000FF000000ULL & t) << 8)  |
            ((0x0000000000FF0000ULL & t) << 24) |
            ((0x000000000000FF00ULL & t) << 40) |
            ((0x00000000000000FFULL & t) << 56);

    }
};


template<typename T>
struct __SwapByteOrder<T, 2, 4>
{
    static T conv (const T& t)
    {
        return
            ((0xFFFF0000 & t) >> 16) |
            ((0x0000FFFF & t) << 16);
    }
};


template<typename T>
struct __SwapByteOrder<T, 2, 8>
{
    static T conv (const T& t)
    {
        return
            ((0xFFFF000000000000ULL & t) >> 48) |
            ((0x0000FFFF00000000ULL & t) >> 16) |
            ((0x00000000FFFF0000ULL & t) << 16) |
            ((0x000000000000FFFFULL & t) << 48);
    }
};


template<typename T>
struct __SwapByteOrder<T, 4, 8>
{
    static T conv (const T& t)
    {
        return
            ((0xFFFFFFFF00000000ULL & t) >> 32) |
            ((0x00000000FFFFFFFFULL & t) << 32);
    }
};


//-------------------------------------------------------------------------------------------------
// endianness swap function implementation
//-------------------------------------------------------------------------------------------------

template<typename T, unsigned cellSize>
inline T swapOrder (const T& t)
{
    return __SwapByteOrder <T, cellSize, sizeof (T)>::conv (t);
};


} // namespace util
