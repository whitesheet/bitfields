# Safe bit-fields for C++ #
Traditional bit-fields in C and C++ are compiler dependent.
Bit-fields for C++ provide a convenient and optimized way to implement compiler independent bit-fields.

## Benefits ##
* Members behave like a member of a struct
* Can be used to access hardware registers by maintaining read and write order
* Build-in endianness convertion can be used to interprete network packets
* Lots of compile time checks help to improve code quality
* Unleashes mostly all compiler optimizations to provide small and efficient code

## Documentation is coming soon... ##
