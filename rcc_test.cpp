//=================================================================================================
//
//   Compiler independent safe bit-fields for C++98
//
//   File:          example.cpp
//
//   Description:   Brief example how to use bit-fields.
//
//   License:       Copyright (c) 2018 Julian Dessecker
//
//                  Permission is hereby granted, free of charge, to any person obtaining
//                  a copy of this software and associated documentation files
//                  (the "Software"), to deal in the Software without restriction,
//                  including without limitation the rights to use, copy, modify, merge,
//                  publish, distribute, sublicense, and/or sell copies of the Software,
//                  and to permit persons to whom the Software is furnished to do so,
//                  subject to the following conditions:
//
//                  The above copyright notice and this permission notice shall be
//                  included in all copies or substantial portions of the Software.
//
//                  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
//                  EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
//                  OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
//                  NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS
//                  BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN
//                  ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
//                  CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
//                  SOFTWARE.
//
//=================================================================================================


#include "api/bitfield.h"


namespace STM32F4 {


//-------------------------------------------------------------------------------------------------
// STM32 register bit offset: calculate from byte-offset and bit-offset, bit 0 is the LSB
//-------------------------------------------------------------------------------------------------
#define STM_REG_ADDR(byteOffset, bitOffset) ((byteOffset * 8) + (31 - bitOffset))
#define STM_REG_OFFSET(byteOffset) (byteOffset * 8)


//-------------------------------------------------------------------------------------------------
// RCC clock control register
//-------------------------------------------------------------------------------------------------

    //            /---------------------------------------- type name of declared bit-field
    //            |       /-------------------------------- inner storage type
    //            v       v
BITFIELD_DECLARE (RCC_CR, volatile uint32_t)

    //               /------------------------------------- field representation data type
    //               |
    //               |                  /------------------ name of field declaration
    //               |                  |
    //               |                  |                   / bit address (0 represents the MSB at
    //               |                  |                   |             the base address of this
    //               |                  |                   |             bit-field.
    //               |                  |                   |
    //               |                  |                   |             /---- byte offset
    //               |                  |                   |             |
    //               |                  |                   |             |  /- bit offset,
    //               |                  |                   |             |  |  0 is LSB
    //               |                  |                   |             |  |
    //               |                  |                   |             |  |      / optional:
    //               |                  |                   |             |  |      | size in bits
    //               v                  v                   v             v  v      v
    BITFIELD_MEMBER (const bool,        pll_sai_ready,      STM_REG_ADDR (0, 29)       )
    BITFIELD_MEMBER (bool,              pll_sai_on,         STM_REG_ADDR (0, 28)       )
    BITFIELD_MEMBER (const bool,        pll_i2s_ready,      STM_REG_ADDR (0, 27)       )
    BITFIELD_MEMBER (bool,              pll_i2s_on,         STM_REG_ADDR (0, 26)       )
    BITFIELD_MEMBER (const bool,        pll_ready,          STM_REG_ADDR (0, 25)       )
    BITFIELD_MEMBER (bool,              pll_on,             STM_REG_ADDR (0, 24)       )
    BITFIELD_MEMBER (bool,              css_on,             STM_REG_ADDR (0, 19)       )
    BITFIELD_MEMBER (bool,              hse_bypass,         STM_REG_ADDR (0, 18)       )
    BITFIELD_MEMBER (const bool,        hse_ready,          STM_REG_ADDR (0, 17)       )
    BITFIELD_MEMBER (bool,              hse_on,             STM_REG_ADDR (0, 16)       )
    BITFIELD_MEMBER (const uint8_t,     hsi_cal,            STM_REG_ADDR (0, 15),   8  )
    BITFIELD_MEMBER (uint8_t,           hsi_trim,           STM_REG_ADDR (0, 7),    5  )
    BITFIELD_MEMBER (const bool,        hsi_ready,          STM_REG_ADDR (0, 1)        )
    BITFIELD_MEMBER (bool,              hsi_on,             STM_REG_ADDR (0, 0)        )

    // reset function, write key and reset flag
    void reset ()
    {
        __reg_reset = 0x10032421; // dummy value
    }

private:
    BITFIELD_MEMBER (uint32_t, __reg_reset, STM_REG_OFFSET (0), 32, PreventRead)

BITFIELD_END


//-------------------------------------------------------------------------------------------------
// RCC peripheral declaration
//-------------------------------------------------------------------------------------------------
BITFIELD_DECLARE (RCC, volatile uint32_t)

    // nesting bit-fields ...
    BITFIELD_MEMBER (RCC_CR,            cr,                 STM_REG_OFFSET (0),     32 )

    // other members go here ...

    // create single instance of RCC at memory address 0x12345678
    BITFIELD_INSTANCE (0x12345678)

BITFIELD_END


//-------------------------------------------------------------------------------------------------
// Example bitfield containing an array member
//-------------------------------------------------------------------------------------------------
BITFIELD_DECLARE (ExampleA, volatile uint8_t, TakeOuterStorage)

    //                     /------------------------------------- field representation data type
    //                     |
    //                     |                /-------------------- name of field declaration
    //                     |                |
    //                     |                |                   /- start bit address
    //                     |                |                   |  (0 represents the MSB
    //                     |                |                   |  at the base address
    //                     |                |                   |  of this bit-field.
    //                     |                |                   |
    //                     |                |                   |       /---- array element count
    //                     |                |                   |       |            /- optional:
    //                     |                |                   |       |            | size in bits
    //                     |                |                   |       |            | of a member
    //                     v                v                   v       v            v
    BITFIELD_MEMBER_ARRAY (uint8_t,         b,                  0,      8,           4  )
    BITFIELD_MEMBER (bool,              bv,                 0                           )

BITFIELD_END


//-------------------------------------------------------------------------------------------------
// Example bitfield containing a nested array
//-------------------------------------------------------------------------------------------------
BITFIELD_DECLARE (ExampleArray, volatile uint32_t)

    // define nested bitfield as array
    BITFIELD_MEMBER_ARRAY (ExampleA,        a,                  0,      16,          32  )

BITFIELD_END


} // namespace STM32F4


#if 1


//-------------------------------------------------------------------------------------------------
// test volatile access to initialize rcc.cr register
//-------------------------------------------------------------------------------------------------
void volatileTest ()
{
    // get single instance of RCC peripheral
    STM32F4::RCC& rcc = STM32F4::RCC::instance ();

    rcc.cr.pll_i2s_on = true;
    rcc.cr.pll_i2s_on = false;
    rcc.cr.hse_on = true;
    rcc.cr.hse_on = false;
}


//-------------------------------------------------------------------------------------------------
// initialize rcc.cr register
//-------------------------------------------------------------------------------------------------
void initTest ()
{
    // get single instance of RCC peripheral
    STM32F4::RCC& rcc = STM32F4::RCC::instance ();

    // use "initializeBitfield" to implement an optimized initializer for bit-field members
    initializeBitfield (
        rcc.cr,                 // bit-field to operate on

        pll_sai_on = false,     // evaluates to "rcc.cr.pll_sai_on = false;"
        pll_i2s_on = true,      // evaluates to "rcc.cr.pll_i2s_on = true;"
        pll_on = true,          // ...
        css_on = true,          // ...
        hse_bypass = true,      // ...
        hse_on = false,         // ...
        hsi_on = true,          // ...
        hsi_trim = 3,           // ...
        pll_sai_on = false      // ...
    );

    // wait until clocks are ready
    while (
        // use "testBitfield" to check for multiple conditions, this is faster than using
        // (rcc.cr.hsi_ready && rcc.cr.pll_ready) since the rcc.cr peripheral register will
        // only be read once allowing better compile-time optimizations.
        ! testBitfield (
            rcc.cr,             // bit-field to operate on

            hsi_ready == true,  // expands to "rcc.cr.hsi_ready == true &&"
            pll_ready,          // expands to "rcc.cr.pll_ready"
            pll_sai_ready       // expands to "rcc.cr.pll_sai_ready"
        )
    );
}


//-------------------------------------------------------------------------------------------------
// get hsi trim value
//-------------------------------------------------------------------------------------------------
uint8_t getHsiTrim ()
{
    STM32F4::RCC& rcc = STM32F4::RCC::instance ();

    // bit-field members can be explicitly casted to their representation type
    return rcc.cr.hsi_trim;
}


//-------------------------------------------------------------------------------------------------
// set hsi trim value
//-------------------------------------------------------------------------------------------------
void setHsiTrim (uint8_t trim)
{
    STM32F4::RCC& rcc = STM32F4::RCC::instance ();

    // bit-field members can be assigned from their representation type
    rcc.cr.hsi_trim = trim;
}


//-------------------------------------------------------------------------------------------------
// increment hsi trim value when smaller than 15
//-------------------------------------------------------------------------------------------------
void incrementHsiTrim ()
{
    STM32F4::RCC& rcc = STM32F4::RCC::instance ();

    // operators are available like on normal variables
    if (rcc.cr.hsi_trim < 15)
    {
        rcc.cr.hsi_trim++;
    }
}


//-------------------------------------------------------------------------------------------------
// access nested array (constant index)
//-------------------------------------------------------------------------------------------------
bool getFromConstExampleArray (const STM32F4::ExampleA& array)
{
    return array.b[2];
}


//-------------------------------------------------------------------------------------------------
// access nested array (constant index)
//-------------------------------------------------------------------------------------------------
bool getFromConstExampleArray (const STM32F4::ExampleArray& array)
{
    // use [] operators to access array members
    return array.a[42].b[1];
}

#endif


//-------------------------------------------------------------------------------------------------
// init bifield array
//-------------------------------------------------------------------------------------------------
void initArray (STM32F4::ExampleArray& array, unsigned idx)
{
    // init bitfield from reference
    initializeBitfield (
        array.a[idx],

        b[3] = false,
        b[2] = true,
        b[1] = true,
        b[0] = true
    );

    // spin loop until values are read back
    while (
        !testBitfieldAll
        (
            array.a[idx],
            b[2],
            b[1]
        )
    );
}


//-------------------------------------------------------------------------------------------------
// test array reference
//-------------------------------------------------------------------------------------------------
void xxx (STM32F4::ExampleArray& array, int a, int b)
{
    array.a[15].b[a]++;
}
