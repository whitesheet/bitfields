# this module has no own build step
all: clean

# use clean build step only to update indexer directory
clean:
	@echo "#!/bin/bash" > clean_cmd
	@echo "" >> clean_cmd
	@echo "mkdir -p .indexer_include; export TARGET=$(TARGET); export TOOLCHAIN=$(TOOLCHAIN); export MODULES=$(MODULES); export CONFIGURATION=$(CONFIGURATION); export MODULE_PATH=$(MODULE_PATH); source "minibuild"; minibuild.update_indexer .indexer_include" >> clean_cmd
	@chmod +x clean_cmd
	@./clean_cmd
	@rm clean_cmd
	